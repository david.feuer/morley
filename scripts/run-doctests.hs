#!/usr/bin/env stack
{- stack script
  --resolver lts-18.10
  --package bytestring
  --package Cabal
  --package directory
  --package filepath
  --package process
  --package universum
-}

-- SPDX-FileCopyrightText: 2022 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE OverloadedStrings, LambdaCase, TupleSections, BlockArguments, NoImplicitPrelude
  , ImportQualifiedPost #-}

import Data.ByteString qualified as BS
import Distribution.Fields (showPError)
import Distribution.PackageDescription
  (Library, condLibrary, condTreeData, defaultExtensions, libBuildInfo)
import Distribution.PackageDescription.Parsec (parseGenericPackageDescription, runParseResult)
import Distribution.Pretty (pretty)
import Distribution.Simple (Extension(..), KnownExtension(..))
import System.Directory (listDirectory)
import System.Environment (getArgs)
import System.Exit (ExitCode(..))
import System.FilePath ((</>))
import System.Process (spawnProcess, waitForProcess)
import Universum

{-|

  Launches doctests for specified packages.

  This script leverages "cabal-docspec" library's
  executable. Please find the installation instructions
  at https://github.com/phadej/cabal-extras. You can
  also download the binaries at https://github.com/phadej/cabal-extras/releases.

  Finally, __make sure "cabal-docspec" is available from your $PATH.__

  To launch doctests locally, specify one or more packages or
  use "all" keyword as a shortcut for all available packages:

  > ./run-doctests.hs morley lorentz

  > ./run-doctests.hs all

  You can also pass some additional "cabal-docspec" arguments to
  the script:

  > ./run-doctests.hs morley lorentz -- --verbose

  To launch doctests in CI, use "--ci" flag first:

  > ./run-doctests.hs --ci morley lorentz

-}
main = do

  args <- getArgs
  exitCodes <- case split (== "--") args of
    (["all"],        rest) -> mapM (runLocally rest) =<< allComponents
    ("--ci" : comps, rest) -> mapM (runOnCI rest) comps
    (comps@(_ : _),  rest) -> mapM (runLocally rest) comps
    _                      -> putStrLn usageInfo >> exitFailure
  checkExitCodes exitCodes

  where
    split cond = fmap (drop 1) . break cond

usageInfo =
  unlines [ "Usage: run-doctests.hs TARGET [TARGET] [TARGET] ... [-- [CABAL-DOCSPEC_ARGUMENTS]]"
          , "\tor run-doctests.hs all [-- [CABAL-DOCSPEC_ARGUMENTS]]"
          , "Hint: remember to rebuild packages with cabal-install before running doctests:"
          , "\tmake MORLEY_USE_CABAL=1 all"
          ]

checkExitCodes :: [(String, ExitCode)] -> IO ()
checkExitCodes codes = do
  let failures = filter isFailure codes
  unless (null failures) $ do
    putStrLn $ "Some doctests failed for packages: " <> intercalate ", " (map fst failures)
    exitFailure
  where
    isFailure = \case
      (_, ExitSuccess) -> False
      _                -> True

-- | Runs doctests in CI for a single package.
--
-- Passes additional "--no-cabal-plan" option
-- to "cabal-docspec" executable.
runOnCI
  :: [String] -- ^ additional "cabal-docspec" arguments
  -> String   -- ^ component/package name
  -> IO (String, ExitCode)
runOnCI args comp = do
  putStrLn $ "Running doctests for package " <> comp
  let cabalfile = componentToCabalfile comp
  exts <- extensionsFromCabalfile cabalfile
  (comp,) <$> waitCallProcess "cabal-docspec" ("--no-cabal-plan" : cabalfile : exts <> args)

-- | Runs doctests locally for a single package.
runLocally
  :: [String] -- ^ additional "cabal-docspec" arguments
  -> String   -- ^ component/package name
  -> IO (String, ExitCode)
runLocally args comp = do
  putStrLn $ "Running doctests for package " <> comp
  exts <- extensionsFromCabalfile $ componentToCabalfile comp
  (comp,) <$> waitCallProcess "cabal-docspec" (comp : exts <> args)

waitCallProcess :: FilePath -> [String] -> IO ExitCode
waitCallProcess command = spawnProcess command >=> waitForProcess

componentToCabalfile :: String -> FilePath
componentToCabalfile comp = "code" </> comp </> (comp <> ".cabal")

allComponents :: IO [String]
allComponents = filterM hasCabalFile =<< listDirectory "code"
  where
    hasCabalFile dir = do
      lst <- listDirectory $ "code" </> dir
      pure $ (dir <> ".cabal") `elem` lst

extensionsFromCabalfile :: FilePath -> IO [String]
extensionsFromCabalfile cabalfile = do
  lib <- getCabalLibComponent cabalfile
  case lib of
    Just lib' -> pure $ ("-X" <>) <$> getEnabledExtensions lib'
    Nothing   -> pure []

-- | Parse a cabal file and extract info about its library component.
getCabalLibComponent :: FilePath -> IO (Maybe Library)
getCabalLibComponent cabalFilePath = do
  contents <- BS.readFile cabalFilePath
  case snd <$> runParseResult $ parseGenericPackageDescription contents of
    Left (version, err :| errs) -> do
      fail $ toString $ unlines $
        [ "Failed to parse .cabal file: " <> toText cabalFilePath
        , "Version: " <> show version
        , "Errors:"
        ]
        <> (toText . showPError cabalFilePath <$> (err : errs))
    Right cabal -> pure $ condTreeData <$> condLibrary cabal

-- | Extracts a list of enabled language extensions from a library component.
--
-- "NoImplicitPrelude" and "RebindableSyntax" extensions are filtered out due
-- to incompatibility with the "cabal-docspec" executable. For more details, see
-- <https://gitlab.com/morley-framework/morley/-/merge_requests/1020>
getEnabledExtensions :: Library -> [String]
getEnabledExtensions lib =
  libBuildInfo lib & defaultExtensions & mapMaybe \case
     EnableExtension extension
         | extension /= RebindableSyntax
         -> Just $ show extension
     ext@(DisableExtension extension)
         | extension /= ImplicitPrelude
         -> Just . show . pretty $ ext
     _ -> Nothing
