BEGIN {
  err=0;
  errs="";
  outp="";
  library="";
  hint="";
  indent = "    ";
  indent2 = indent indent;
  indent3 = indent2 indent;
}

/^Running Haddock on library for/ {
  gsub(/^Running Haddock on library for /,"");
  gsub(/\.*$/,"");
  library=$0;
}

/^Warning:/ ||
/Missing documentation for:/ ||
/^$/ ||
/^ *[0-9]+% *\( *[0-9]+ *\/ *[0-9]+\) in '.+'$/ {
  if (err=1 && hint!="") {
    errs= errs "\n" hint;
  }
  err=0;
  hint="";
}

/^Warning: '.+' is out of scope\./ ||
/^Warning: '.+' is ambiguous\. It is defined/ {
  err=1;
  ident=$0;
  gsub(/^[^']*'/,"",ident);
  gsub(/'[^']*$/,"",ident);
  hint=indent2 "Hint: Haddock doesn't differentiate between ` and ' in most cases, \n"\
    indent3 "so check if the file also contains \n"\
    indent3 "`" ident "`, `" ident "' or '" ident "`, \n"\
    indent3 "those will also produce this warning.";
}

/^Warning: '.+' is out of scope\./ {
  hint=hint "\n" \
    indent2 "Hint: Qualifying identifiers in Haddock is generally discouraged,\n"\
    indent3 "due to the silently broken links it can create.\n"\
    indent3 "Consider using @"ident"@ instead";
}

/^Warning: '.+' is ambiguous\. It is defined/ {
  hint=hint "\n"\
    indent2 "Hint: Use t'" ident "' or v'" ident "' to specify\n"\
    indent3 "whether it is a type or a value, respectively.";
}

/^ *[0-9]+% *\( *[0-9]+ *\/ *[0-9]+\) in '.+'$/ {
  gsub(/^ *[0-9]+% *\( *[0-9]+ *\/ *[0-9]+\) in '/, "");
  gsub(/'$/, "");
  if (errs != "") {
    if (outp != "")
      outp=outp "\n\n"
    outp=outp "In package " library " module " $0 ":" errs;
  }
  errs="";
}

{
  if (err)
    errs=errs "\n" indent $0;
}

END {
  if (outp != "") {
    print "Fixable Haddock warnings found:\n" outp;
    exit 1;
  }
}
