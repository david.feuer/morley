#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

@test "Morley test verbose typechecking" {
    # If the test fails on '<CONTRACT>.tz' check the '<CONTRACT>.temp'
    # for output. If you find it satisfactory, just rename
    # '<CONTRACT>.temp' to '<CONTRACT>.gold'.

    # In order to regenerate all gold files, use
    # 'scripts/regenerate-gold-files-for-verbose-typechecking'. Be
    # careful.
    tests_dir="contracts/verbose-typecheck"
    for test_file in $tests_dir/*.{tz,mtz}; do
        printf "checking $test_file\n"
        base_file="${test_file%.*}"
        temp_file="${base_file}.temp"
        gold_file="${base_file}.gold"
        morley typecheck \
          --contract $test_file \
          --verbose \
          $(
          case "$test_file" in
            *".mtz") echo "--deprecated-morley-extensions";;
          esac ) &> "$temp_file" || true
        diff "$temp_file" "$gold_file" \
             --ignore-matching-lines="#.*" \
             --ignore-trailing-space \
             --ignore-blank-lines \
             --new-file # treat absent files as empty
        rm "$temp_file"
    done
}
