#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ
#
# Test whether morley utilities work correctly independent of a system
# locale. For more detailed description of the problem (and also
# chosen approach to solve it) see
# https://serokell.io/blog/haskell-with-utf8.
#
# Tests expect morley executables and tezos-client to be in PATH.

export LC_ALL=C # to discover encoding issues

contract="contracts/tezos_examples/attic/add1.tz"
contract_with_ep="contracts/entrypoints/contract1.mtz"
db="bats_db.json"
genesisAddress="tz1f1S7V2hZJ3mhj47djb5j1saek8c2yB2Cx"

@test "invoking morley emulate run" {
    morley emulate run --contract "$contract" \
           --db "$db" --storage 1 --parameter 1 --amount 1 --level 1 --verbose \
           --now 0 --max-steps 1000 --balance 100 --write
    morley emulate run --contract "$contract_with_ep" \
           --db "$db" --storage 1 --parameter 1 --amount 1 --level 1 --entrypoint a \
           --verbose --now 0 --max-steps 1000 --balance 100 --write \
           --deprecated-morley-extensions
    rm "$db"
}

@test "invoking morley emulate originate" {
    morley emulate originate --contract "$contract" \
           --db "$db" \
           --originator $genesisAddress \
           --delegate $genesisAddress \
           --storage 1 --balance 1 --verbose
    rm "$db"
}

@test "invoking morley emulate transfer" {
    morley emulate transfer --db "$db" \
           --to $genesisAddress \
           --sender $genesisAddress \
           --parameter Unit --amount 1 --now 0 --max-steps 1000 \
           --verbose --dry-run
}

@test "invoking morley to typecheck contract with cyrillic comments from stdin" {
    cat contracts/add1_with_cyrillic_comments.tz | morley typecheck
}

@test "invoking morley to print contract with cyrillic comments" {
    morley print --contract contracts/add1_with_cyrillic_comments.tz
}

@test "invoking morley expecting it to fail to parse an invalid contract with non-ascii characters in error message" {
    morley typecheck --contract contracts/unparsable/non_ascii_error.mtz 2>&1 | grep "expecting '%' or type"
}

@test "originating a contract from cyrillic alias with morley client" {
    tempdir="$(mktemp -d --tmpdir="$PWD")"
    tezos_client_args=(-E "$TASTY_CLEVELAND_NODE_ENDPOINT" -d "$tempdir")

    alias="кириллический-псевдоним"
    tezos-client "${tezos_client_args[@]}" \
                 import secret key $alias \
                 "$TASTY_CLEVELAND_MONEYBAG_SECRET_KEY" --force

    tezos-client "${tezos_client_args[@]}" list known addresses

    morley-client "${tezos_client_args[@]}" get-balance --addr $alias

    morley-client "${tezos_client_args[@]}" originate \
                  --contract ./contracts/add1_with_cyrillic_comments.tz \
                  --initial-storage 1 \
                  --from $alias -VV # double verbose to print alias

    rm -r "$tempdir"
}
