# SPDX-FileCopyrightText: 2021 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

{ system ? builtins.currentSystem }:
let
  sources = import ./sources.nix;
  haskellNix = import sources."haskell.nix" {
    sourcesOverride = { hackage = sources."hackage.nix"; stackage = sources."stackage.nix"; };
  };
  nixpkgs = import sources.nixpkgs;

in
  nixpkgs (haskellNix.nixpkgsArgs // {
    localSystem.system = system;
  })
