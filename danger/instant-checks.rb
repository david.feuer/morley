# SPDX-FileCopyrightText: 2021 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

# Checks that, when hit, should be fixed as soon as possible.

require_relative 'helpers'
require_relative 'trailing-whitespaces'
require_relative 'changelogs'

check_trailing_whitespaces()

check_changelogs()

# Clean commits history
if git.commits.any? { |c| c.subject =~ /^Merge branch/ }
  fail 'Please, no merge commits. Rebase for the win.'
end

# Proper commit style
# Note: we do not use commit_lint plugin because it triggers on fixup commits
git.commits.each { |commit|
  if commit.fixup? || commit.wip?
    next
  end

  subject = commit.subject
  subject_payload = subject.sub(issue_tags_pattern, "")
  subject_ticked = commit.subject_ticked

  unless has_valid_issue_tags(subject)
    # If any of these substrings is included into commit message,
    # we are fine with issue tag absence.
    exclusions = [
      # In lower-case
      "changelog"
    ]
    if exclusions.none? { |exc| subject.downcase.include?(exc) }
      warn("In #{commit.sha} message lacks issue id: #{subject_ticked}.")
    end
  end

  if subject_payload.start_with?(" ")
    warn("Extra space in commit #{commit.sha} subject after the issue tags: #{subject_ticked}.")
  elsif !subject_payload.start_with?(/[A-Z]/)
    warn("In #{commit.sha} subject does not begin from uppercase letter: #{subject_ticked}.")
  end

  if subject[-1..-1] == '.'
    warn("In #{commit.sha} message ends with a dot: #{subject_ticked} :fire_engine:")
  end

  if subject.length > 90
    fail("Nooo, such long commit message names do not work (`#{commit.sha}`).")
  elsif subject.length > 72
    warn("In commit #{commit.sha} message is too long (#{subject.length} chars), keep it within 72 characters length.")
  end

  if commit.message_body.empty?
    # If any of these substrings is included into commit message,
    # we are fine with commit description absence.
    exclusions = [
      # In lower-case
      "changelog"
    ]
    unless commit.chore? || exclusions.any? { |exc| subject.downcase.include?(exc) }
      fail("Commit #{commit.sha} lacks description :unamused:")
    end
  else
    # Checks on description

    if !commit.blank_line_after_subject?
      warn("In #{commit.sha} blank line is missing after the commit's subject.")
    end

    if !commit.chore?
      description_patterns = [
        /^Problem:[ \n].*^Solution:[ \n]/m,
        /And yes, I don't care about templates/
      ]
      unless description_patterns.any? { |pattern| pattern.match?(commit.description) }
        warn(
          "Description of #{commit.sha} does not follow the template.\n"\
          "Try `Problem:`/`Solution:` structure."
        )
      end
    end
  end

}

# Proper MR content
mr_title_payload = githost.mr_title_payload

unless has_valid_issue_tags(mr_title_payload)
  warn(
    "Inappropriate title for PR.\n"\
    "Should start from issue ID (e.g. `[#123]`) or `[Chore]` tag.\n"\
    "Note: please use `[Chore]` also for tickets tracked internally on YouTrack."
  )
end

## Supplying a link to an YT ticket
all_YT_tickets = githost.mr_body.scan(/\b[A-Z]+-\d+\b/).uniq.map do |ticket_id|
  link = "https://issues.serokell.io/issue/#{ticket_id}"
  "[#{ticket_id}](#{link})"
end.join(', ')
unless all_YT_tickets.empty?
  message("Mentioned YT tickets: #{all_YT_tickets}.")
end

# Proper branch name
if branch_match = githost.branch_for_head.match(/([^\/]+)\/([^\-]+)-(.+)/)
  nick, issue_id, desc = branch_match.captures

  # We've decided not to put any restrictions on nickname for now

  unless /^(#\d+|[a-z]{2,}\d+|chore)$/.match?(issue_id)
    warn(
      "Bad issue ID in branch name.\n"\
      "Valid format for issue IDs: `#123`, `tm123` (for YT tickets) or `chore`."
    )
  end

  weird_chars = desc.scan(/[^a-z\-\d]/)
  unless weird_chars.empty?
    warn(
      "Please, only use lowercase letters, digits and dashes in the branch name.
      Found: #{weird_chars}"
    )
  end
elsif
  warn(
    "Please use `<nickname>/<issue-id>-<brief-description>`` format for branch names.`\n"\
    "Example: `lazyman/#123-my-commit`"
  )
end

# Licenses
# Check that the REUSE license header contains the current year.
cur_year = Time.new.year
expected_holder = "Oxhead Alpha"
# Only go over new files; see https://gitlab.com/morley-framework/morley/-/merge_requests/1091
# for the discussion and rationale for this.
git.added_files.each do |file|
  File.foreach(file).with_index(1).find do |line, line_num|
    if year_match = line.match(/(^.*SPDX-FileCopyrightText:)\s+(\w+-)?(\w+)\s+(.*)$/)
      head, start, year, holder = year_match.captures
      message = ""
      unless (year == cur_year.to_s)
        message += ":warning: The year in this license header is outdated, time to update!\n\n"
      end
      unless (holder.strip == expected_holder)
        message += ":warning: The copyright holder likely should be #{expected_holder}\n\n"
      end
      unless (message == "")
        suggested = "#{head} #{start.nil? ? "" : start}#{cur_year} #{expected_holder}"
        markdown(
          "#{message}"\
          "```suggestion:-0+0\n"\
          "#{suggested}\n"\
          "```", file: file, line: line_num
        )
      end
      # either way, we return 'true' to stop looking after the first 'match'
      true
    end
  end
end
