# SPDX-FileCopyrightText: 2021 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

rec {
  sources = import ./nix/sources.nix;
  # pkgs for builtins.currentSystem
  pkgs = import ./nix/nixpkgs-with-haskell-nix.nix {};
  # TODO: drop this when `serokell/nixpkgs` acquires stylish-haskell >= 0.13.0.0.
  pkgs-stylish = import sources.nixpkgs-stylish {};
  pkgsStatic = pkgs.pkgsCross.musl64;
  xrefcheck = import sources.xrefcheck;
  weeder-hacks = import sources.haskell-nix-weeder { inherit pkgs; };
  tezos-client = (import "${sources.tezos-packaging}/nix/build/pkgs.nix" {}).ocamlPackages.tezos-client;

  # all local packages and their subdirectories
  # we need to know subdirectories for weeder and for cabal check
  local-packages = [
    { name = "lorentz"; subdirectory = "code/lorentz"; }
    { name = "morley"; subdirectory = "code/morley"; }
    { name = "morley-client"; subdirectory = "code/morley-client"; }
    { name = "morley-large-originator"; subdirectory = "code/morley-large-originator"; }
    { name = "cleveland"; subdirectory = "code/cleveland"; }
    { name = "morley-prelude"; subdirectory = "code/morley-prelude"; }
    { name = "autodoc-sandbox"; subdirectory = "code/autodoc-sandbox"; }
  ];

  # names of all local packages
  local-packages-names = map (p: p.name) local-packages;

  # source with gitignored files filtered out
  projectSrc = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "morley";
    src = ./.;
  };

  # GHC has issues with compiling statically linked executables when Template Haskell
  # is used, this wrapper for ld fixes it. Another solution we could use is to link
  # GHC itself statically, but it seems to break haddock.
  # See https://github.com/input-output-hk/haskell.nix/issues/914#issuecomment-897424135
  linker-workaround = pkgs.writeShellScript "linker-workaround" ''
    # put all flags into 'params' array
    source ${pkgsStatic.stdenv.cc}/nix-support/utils.bash
    expandResponseParams "$@"

    # check if '-shared' flag is present
    hasShared=0
    for param in "''${params[@]}"; do
      if [[ "$param" == "-shared" ]]; then
        hasShared=1
      fi
    done

    if [[ "$hasShared" -eq 0 ]]; then
      # if '-shared' is not set, don't modify the params
      newParams=( "''${params[@]}" )
    else
      # if '-shared' is present, remove '-static' flag
      newParams=()
      for param in "''${params[@]}"; do
        if [[ ("$param" != "-static") ]]; then
          newParams+=( "$param" )
        fi
      done
    fi

    # invoke the actual linker with the new params
    exec x86_64-unknown-linux-musl-cc @<(printf "%q\n" "''${newParams[@]}")
  '';

  # haskell.nix package set
  # parameters:
  # - release -- 'true' for master and producion branches builds, 'false' for all other builds.
  #   This flag basically disables weeder related files production, haddock and enables stripping
  # - static -- build statically linked executables
  # - commitSha, commitDate -- git revision info used during compilation of packages with autodoc
  # - optimize -- 'true' to enable '-O1' ghc flag, we use it for publishing docker image for morley
  hs-pkgs = { release, static ? true, optimize ? false, commitSha ? null, commitDate ? null}:
    let
      haskell-nix = if static then pkgsStatic.haskell-nix else pkgs.haskell-nix;
    in haskell-nix.stackProject {
      src = projectSrc;

      # use .cabal files for building because:
      # 1. haskell.nix fails to work for package.yaml with includes from the parent directory
      # 2. with .cabal files haskell.nix filters out source files for each component, so only the changed components will rebuild
      ignorePackageYaml = true;

      modules = [
        # common options for all local packages:
        {
          packages = pkgs.lib.genAttrs local-packages-names (packageName: {
            ghcOptions = with pkgs.lib;
              # we use O1 for production binaries in order to improve their performance
              # for end-users
              [ (if optimize then "-O1" else "-O0") "-Werror"]
              # override linker to work around issues with Template Haskell
              ++ optionals static [ "-pgml=${linker-workaround}" ]
              # produce *.dump-hi files, required for weeder:
              ++ optionals (!release) ["-ddump-to-file" "-ddump-hi"]
              # do not erase any 'assert' calls
              ++ optionals (!release) ["-fno-ignore-asserts"];
            dontStrip = !release;  # strip in release mode, reduces closure size
            doHaddock = !release;  # don't haddock in release mode

            # in non-release mode collect all *.dump-hi files (required for weeder)
            postInstall = if release then null else weeder-hacks.collect-dump-hi-files;
          });
        }

        {
          # don't haddock dependencies
          doHaddock = false;

          packages.autodoc-sandbox = {
            preBuild = ''
              export MORLEY_DOC_GIT_COMMIT_SHA=${if release then pkgs.lib.escapeShellArg commitSha else "UNSPECIFIED"}
              export MORLEY_DOC_GIT_COMMIT_DATE=${if release then pkgs.lib.escapeShellArg commitDate else "UNSPECIFIED"}
            '';
          };
          # in order to make contracts from ./contracts accessible during 'nix-build'
          packages.cleveland = {
            preBuild = ''
              cp -rT ${projectSrc}/contracts ../../contracts
            '';
          };
        }
      ];
  };

  hs-pkgs-development = hs-pkgs { release = false; };

  # component set for all local packages like this:
  # { morley = { library = ...; exes = {...}; tests = {...}; ... };
  #   morley-prelude = { ... };
  #   ...
  # }
  packages = pkgs.lib.genAttrs local-packages-names (packageName: hs-pkgs-development."${packageName}".components);

  # returns a list of all components (library + exes + tests + benchmarks) for a package
  get-package-components = pkg: with pkgs.lib;
    optional (pkg ? library) pkg.library
    ++ attrValues pkg.exes
    ++ attrValues pkg.tests
    ++ attrValues pkg.benchmarks;

  # per-package list of components
  components = pkgs.lib.mapAttrs (pkgName: pkg: get-package-components pkg) packages;

  # a list of all components from all packages in the project
  all-components = with pkgs.lib; flatten (attrValues components);

  haddock = with pkgs.lib; flatten (attrValues
    (mapAttrs (pkgName: pkg: optional (pkg ? library) pkg.library.haddock) packages));

  # TODO [#694]: consider building 'cabal-docspec' from source
  cabal-docspec = pkgs.stdenv.mkDerivation rec {
    name = "cabal-docspec";
    src = builtins.fetchurl {
      url = "https://github.com/phadej/cabal-extras/releases/download/cabal-docspec-0.0.0.20211114/cabal-docspec-0.0.0.20211114.xz";
    };
    phases = [ "installPhase" ];
    installPhase = ''
      mkdir -p $out/bin
      xz -d < ${src} > $out/bin/cabal-docspec
      chmod +x $out/bin/cabal-docspec
    '';
  };

  run-doctests = { target }:
    let
      env = hs-pkgs-development.shellFor {
        withHoogle = false;
        additional = _: [ packages."${target}".library ];
      };
    in with pkgs; writeShellScript "run-doctests" ''
      export PATH=${lib.makeBinPath [ cabal-docspec env.ghc ]}
      ${env.ghc.targetPrefix}runghc \
        ${projectSrc}/scripts/run-doctests.hs --ci ${target} -- -w ${env.ghc.targetPrefix}ghc
    '';

  # run some executables to produce contract documents
  contracts-doc = { release, commitSha ? null, commitDate ? null }@releaseArgs: pkgs.runCommand "contracts-doc" {
    buildInputs = [
      (hs-pkgs releaseArgs).autodoc-sandbox.components.exes.autodoc-sandbox-registry
    ];
  } ''
    mkdir $out
    cd $out
    mkdir autodoc
    autodoc-sandbox-registry document --name AutodocSandbox --output \
      autodoc/AutodocSandboxContract.md
  '';

  # Script that runs given network scenario with a dedicated moneybag in isolated tezos-client
  # environment. Scenario to run should be defined by the 'scenario' argument.
  # 'TASTY_CLEVELAND_MONEYBAG_SECRET_KEY' address will be refilled before the
  # scenario is run (to have a balance defined by the 'refill-balance' argument)
  # and emptied after it is completed (to avoid getting too many funds stuck).
  # Usage example:
  # nix-build ci.nix -A packages.cleveland.tests.cleveland-test
  # $(nix-build ci.nix -A run-chain-tests
  #   --argstr refill-balance 10
  #   --argstr node-endpoint "$TASTY_CLEVELAND_NODE_ENDPOINT"
  #   --argstr step-moneybag "$STEP_MONEYBAG"
  #   --argstr scenario 'cd code/cleveland && ../../result/bin/cleveland-test --color=always --cleveland-mode=only-network')
  run-chain-tests = { refill-balance ? "10", node-endpoint, step-moneybag, scenario }: let
    tezos-client-bin="${tezos-client}/bin/tezos-client -d \"$TASTY_CLEVELAND_DATA_DIR\" -E ${node-endpoint}";
  in pkgs.writeShellScript "run-chain-tests.sh" ''
    min_block_delay="$(${pkgs.curl}/bin/curl -s "${node-endpoint}/chains/main/blocks/head/context/constants" | ${pkgs.jq}/bin/jq -r .minimal_block_delay)"
    get_curr_level="${pkgs.curl}/bin/curl -s "${node-endpoint}/chains/main/blocks/head/metadata" | ${pkgs.jq}/bin/jq -r .level_info.level"
    wait_next_block() {
      start_level="$(eval $get_curr_level)"
      while [[ "$start_level" == "$(eval $get_curr_level)" ]]; do
        sleep "$min_block_delay"
      done
    }

    TASTY_CLEVELAND_DATA_DIR="$(mktemp -d --tmpdir="$PWD")"
    export TASTY_CLEVELAND_DATA_DIR
    export PATH=${tezos-client}/bin:$PATH

    ${tezos-client-bin} import secret key step-moneybag ${step-moneybag}
    ${tezos-client-bin} gen keys moneybag --force
    TASTY_CLEVELAND_MONEYBAG_SECRET_KEY="$(${tezos-client-bin} show address moneybag -S | grep "Secret Key:" | cut -d' ' -f3)"
    export TASTY_CLEVELAND_MONEYBAG_SECRET_KEY
    # Transfer may fail due to a race condition in case multiple CI steps are running simultaneously,
    # so we retry it until it succeeds.
    until ${tezos-client-bin} transfer ${refill-balance} from step-moneybag to moneybag --burn-cap 0.1; do
      wait_next_block
    done
    ${scenario}
    # Wait one block to avoid race conditions with the scenario itself, in the
    # unlikely case that the moneybag just did a manager operation there.
    wait_next_block
    # Transfer all remaining balance (minus 0.1 to account for fees) back, to
    # avoid too many funds getting stuck.
    # Note: no retry loop here because no other job should be using this moneybag
    remaining_amount="$(${tezos-client-bin} get balance for moneybag | cut -d' ' -f 1)"
    transfer_amount="$(echo "$remaining_amount-0.1" | ${pkgs.bc}/bin/bc -l)"
    ${tezos-client-bin} transfer "$transfer_amount" from moneybag to step-moneybag
  '';

  # release version of morley executable
  morley-release = { optimize ? false }:
    (hs-pkgs { release = true; optimize = optimize; }).morley.components.exes.morley;

  # docker image for morley executable
  # 'creationDate' -- creation date to put into image metadata
  # 'optimize' -- compile with optimizations enabled
  morleyDockerImage = { creationDate, tag ? null, optimize ? false }:
    pkgs.dockerTools.buildImage {
    name = "morley";
    contents = morley-release { inherit optimize; };
    created = creationDate;
    inherit tag;
  };

  # nixpkgs has weeder 2, but we use weeder 1
  weeder-legacy = pkgs.haskellPackages.callHackageDirect {
    pkg = "weeder";
    ver = "1.0.9";
    sha256 = "0gfvhw7n8g2274k74g8gnv1y19alr1yig618capiyaix6i9wnmpa";
  } {};

  # a derivation which generates a script for running weeder
  weeder-script = weeder-hacks.weeder-script {
    hs-pkgs = hs-pkgs-development;
    local-packages = local-packages;
    weeder = weeder-legacy;
  };

  # checks if all packages are appropriate for uploading to hackage
  run-cabal-check = pkgs.runCommand "cabal-check" { buildInputs = [ pkgs.cabal-install ]; } ''
    ${pkgs.lib.concatMapStringsSep "\n" ({ name, subdirectory }: ''
      echo 'Running `cabal check` for ${name}'
      cd ${projectSrc}/${subdirectory}
      cabal check
    '') local-packages}

    touch $out
  '';

  # stack2cabal is broken because of strict constraints, set 'jailbreak' to ignore them
  stack2cabal = pkgs.haskell.lib.overrideCabal pkgs.haskellPackages.stack2cabal (drv: {
    jailbreak = true;
    broken = false;
  });
}
