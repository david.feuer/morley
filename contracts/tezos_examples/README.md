<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Tezos example contracts

The contracts in this directory are taken from the [tezos repository][tz:contracts].

Note: Legacy examples were removed from this repository

## Update instructions

These contracts are occasionally updated in the [tezos repository][tz:contracts] and we synchronize them from time to time.
Here are some things you should keep in mind:
1. Update the link to the contracts in this file.
2. Do not accidentally add the `legacy` folder because it was removed as stated above.
3. Carefully check the [Contracts.hs](/code/cleveland/test-common/Test/Util/Contracts.hs) module:
  + `wellTypedContractDirs` lists all folders with well-typed contracts, you may need to add new folders there.
  + We have to distinguish ill-typed and unparseable contracts, and our parser is not the same as in Tezos, so there is an ill-typed example contract that is not parseable by our code.
  It's handled in that module.

## License

[MIT][tz:license] © 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>

[tz:contracts]: https://gitlab.com/tezos/tezos/-/tree/98cbf36bf8bbe2e853510f0eea8f4ddff12dc568/tests_python/contracts
[tz:license]: ../../LICENSES/LicenseRef-MIT-Tezos.txt
