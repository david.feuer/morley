# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# This contract's behavior heavily depends on environment in which it's executed.
# 1. It fails if its balance is greater than 1000.
# 2. It fails if the address passed to it is a contract with parameter `address`.
# 3. It fails if the amount transferred to it is less than 15.
parameter address;
storage unit;
code {
       # Check balance and possibly fail
       PUSH mutez 1000;
       BALANCE;
       IFCMPGT { BALANCE; FAILWITH; } { };

       # Check address passed as parameter
       CAR;
       CONTRACT address;
       IF_SOME { ADDRESS; FAILWITH; } { };

       # Check amount transferred to this contract
       PUSH mutez 15;
       AMOUNT;
       IFCMPLT { AMOUNT; FAILWITH; } { };

       # Finish
       UNIT;
       NIL operation;
       PAIR; };
