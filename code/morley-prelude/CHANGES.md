<!-- Unreleased: append new entries here -->


0.5.1
=====
* [!1080](https://gitlab.com/morley-framework/morley/-/merge_requests/1080)
  Add more `PrettyShow` type instances
  + For lists
  + Add instances that forbid defining `PrettyShow` for `String`, `Text`, and `ByteString`
* [!1075](https://gitlab.com/morley-framework/morley/-/merge_requests/1075)
  Add suitable types for oddly-sized unsigned integers
  + Add a dependency on `OddWord`.
  + Re-export `Word62`, `Word63` types representing unsigned integer word types with odd sizes in `Prelude`.
* [!1025](https://gitlab.com/morley-framework/morley/-/merge_requests/1025)
  Reduce the number of unsafe functions at the call site
  + Add `unsafe`, which is primarily needed for making unsafe counter-parts of safe functions, to `Unsafe`.
  + Add `unsafeM`, which is similar to `unsafe`, but throws monadic exceptions.
* [!978](https://gitlab.com/morley-framework/morley/-/merge_requests/978)
  Make it difficult to misuse 'Show'
  + `show` exported from `morley-prelude` now requires the type to be an instance of an open type family `PrettyShow` of kind `Constraint`. This is intended to discourage the use of `show` for user-facing output (use `Buildable` instead).
  + The original `show` is exported from `Debug` module, which is intended to be imported qualified.
* [!1035](https://gitlab.com/morley-framework/morley/-/merge_requests/1035)
  Better type errors on invalid int casts

0.5.0
=====
* [!1001](https://gitlab.com/morley-framework/morley/-/merge_requests/1001)
  Make fromIntegralNoOverflow safe(r)
  + `fromIntegralNoOverflow` now doesn't throw `ArithException` `Underflow`
  + Moved `fromIntegralNoOverflow` from `Unsafe` to `Prelude`
* [!936](https://gitlab.com/morley-framework/morley/-/merge_requests/936)
  + Hide `Universum`'s `fromIntegral` and redefine it as `intCast` in `Prelude`.
  + Re-export `intCastMaybe` as `fromIntegralMaybe` in `Prelude`.
  + Add `fromIntegralToRealFrac` to `Prelude`.
  + Add `fromIntegralOverflowing` to `Prelude`.
  + Add `fromIntegralNoOverflow` to `Unsafe`.
  + Add `Unsafe.fromIntegral` which is like `Universum`'s `fromIntegral` but raises exception on overflow/underflow.
* [!945](https://gitlab.com/morley-framework/morley/-/merge_requests/945)
  + Bump Stackage LTS version from 17.9 to 18.10.

0.4.2
=====
* [!867](https://gitlab.com/morley-framework/morley/-/merge_requests/867)
  + Hid Prelude's `&&` and `||` operators
  + Exported `Boolean` typeclass and polymorphic `&&` and `||` operators.

0.4.1
=====
* [!861](https://gitlab.com/morley-framework/morley/-/merge_requests/861)
  + Removed the re-export of some basic `microlens` operators from `universum`
    in favor of the ones from `lens` with the same name.
* [!779](https://gitlab.com/morley-framework/morley/-/merge_requests/779)
  + Export `for`.

0.4.0
=====

* [!814](https://gitlab.com/morley-framework/morley/-/merge_requests/814)
  + Reverted hiding `Type` from export list.
* [!781](https://gitlab.com/morley-framework/morley/-/merge_requests/781)
  + Replaced mixins and dependency on `base` with `base-noprelude`.

* Change the license to MIT.

0.3.0
=====

* Hide `Nat` export.

0.2.0.1
=======

* Update maintainer.

0.2.0
======

* Hide `readFile` and `writeFile`.
* Add `Unsafe` module which re-exports `Universum.Unsafe`.

0.1.0.4
=====

Initial release.
Re-exports `universum`.
