#!/usr/bin/env stack
{- stack
  script
  --resolver snapshot.yaml
  --package base-noprelude
  --package tasty
  --package morley
  --package cleveland
  --package morley-prelude
  --ghc-options "-hide-package base"
-}
-- Note that stack shebang and its arguments list cannot be separated with a newline
-- because otherwise arguments won't be used.

-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE OverloadedStrings, QuasiQuotes, TypeApplications #-}

module HelloTezosSpec
  ( test_hello
  ) where

import Test.Tasty (TestTree)

import Morley.Michelson.Text (MText)
import Test.Cleveland
import Test.Cleveland.Tasty

main :: IO ()
main = clevelandMain test_hello

test_hello :: TestTree
test_hello = testScenario "contracts/hello_tezos.tz puts 'Hello Tezos!' to its storage" $ scenario $ do
  contract <- importContract @() @MText "contracts/hello_tezos.tz"
  contractHandle <- originateSimple "hello" "" contract
  call contractHandle CallDefault ()
  storage <- getStorage contractHandle
  storage @== "Hello Tezos!"
