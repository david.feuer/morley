#!/usr/bin/env stack
{- stack
  script
  --resolver snapshot.yaml
  --package base-noprelude
  --package tasty
  --package tasty-hedgehog
  --package hedgehog
  --package cleveland
  --package morley
  --package morley-prelude
  --ghc-options "-hide-package base"
-}
-- Note that stack shebang and its arguments list cannot be separated with a newline
-- because otherwise arguments won't be used.

-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE ImportQualifiedPost, OverloadedStrings, TypeApplications, RecordWildCards #-}

module EnvironmentSpec
  ( hprop_environment
  ) where

import Hedgehog (MonadGen, Property, forAll, property)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Test.Tasty.Hedgehog (testProperty)

import Hedgehog.Gen.Tezos.Core (genMutez')
import Lorentz.Address (toAddress)
import Morley.Michelson.Typed
import Morley.Tezos.Address (Address)
import Morley.Tezos.Core (Mutez, Timestamp, timestampFromSeconds)
import Test.Cleveland
import Test.Cleveland.Tasty

main :: IO ()
main = clevelandMain $ testProperty desc hprop_environment
  where
  desc =
    "This contract fails under conditions described in a comment at the \
    \beginning of this contract."

hprop_environment :: Property
hprop_environment = property $ do
    fixture@Fixture{..} <- forAll genFixture
    testScenarioProps $ withInitialNow fNow $ myScenario fixture

data Fixture = Fixture
  { fNow :: !Timestamp
  , fPassOriginatedAddress :: !Bool
  , fBalance :: !Mutez
  , fAmount :: !Mutez
  } deriving (Show)

genFixture :: MonadGen m => m Fixture
genFixture = do
  fNow <- timestampFromSeconds <$> (Gen.integral $ Range.linear 100000 111111)
  fPassOriginatedAddress <- Gen.bool
  fBalance <- genMutez' $ Range.linear 1 1234
  fAmount <- genMutez' $ Range.linear 1 42
  return Fixture {..}

shouldExpectFailed :: Fixture -> Bool
shouldExpectFailed fixture =
  or
    [ fBalance fixture + fAmount fixture > 1000
    , fNow fixture < timestampFromSeconds 100500
    , fPassOriginatedAddress fixture
    , fAmount fixture < 15
    ]

myScenario :: Monad m => Fixture -> Scenario m
myScenario fixture@Fixture{..} = scenario $ do
  contract <- importContract @Address "contracts/environment.tz"

  -- Then let's originate the 'environment.tz' contract
  environmentAddress <- originate OriginateData
    { odContract = contract
    , odName = "environment"
    , odStorage = ()
    , odBalance = fBalance
    }

  -- And transfer mutez to it
  param <-
    if fPassOriginatedAddress
    then pure $ toAddress environmentAddress
    else newFreshAddress auto
  let
    txData = TransferData
      { tdTo = environmentAddress
      , tdParameter = param
      , tdAmount = fAmount
      , tdEntrypoint = DefEpName
      }
  if (shouldExpectFailed fixture)
  then do
    expectTransferFailure (addressIs environmentAddress)
      $ transfer txData
  else do
    transfer txData
