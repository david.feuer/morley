#!/usr/bin/env stack
{- stack
  script
  --resolver snapshot.yaml
  --package base-noprelude
  --package tasty
  --package tasty-hedgehog
  --package hedgehog
  --package morley
  --package cleveland
  --package morley-prelude
  --ghc-options "-hide-package base"
-}
-- Note that stack shebang and its arguments list cannot be separated with a newline
-- because otherwise arguments won't be used.

-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE OverloadedStrings, TypeApplications #-}

module CompareSpec
  ( hprop_compare
  ) where

import Hedgehog (Gen, Property, forAll, property)
import Test.Tasty.Hedgehog (testProperty)

import Hedgehog.Gen.Tezos.Core (genMutez)
import Morley.Tezos.Core (Mutez)
import Test.Cleveland
import Test.Cleveland.Tasty

main :: IO ()
main = clevelandMain $
  testProperty "compare" hprop_compare

genParameter :: Gen (Mutez, Mutez)
genParameter = (,) <$> genMutez <*> genMutez

hprop_compare :: Property
hprop_compare = property $ do
    input <- forAll genParameter
    testScenarioProps $ scenario $ do
      contract <- importContract @(Mutez, Mutez) @[Bool] "contracts/compare.tz"
      handle <- originateSimple "compare" [] contract
      call handle CallDefault input
      getStorage handle @@== mkExpected input

mkExpected :: (Mutez, Mutez) -> [Bool]
mkExpected (a, b) = [a == b, a > b, a < b, a >= b, a <= b]
