-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Main (main) where

import Test.Cleveland.Tasty

import Tree (tests)

main :: IO ()
main = tests >>= clevelandMain
