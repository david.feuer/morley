-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests on importing functionality.
module TestSuite.Cleveland.Import
  ( test_Import
  , test_Embed
  , test_ImportInt
  , test_EmbedInt
  , test_ImportAddress
  , test_EmbedAddress
  , test_ImportMutez
  , test_EmbedMutez
  ) where

import Test.Tasty (TestTree)

import Lorentz.Value (Address, Mutez)
import Morley.Tezos.Address (ta)
import Test.Cleveland
import Test.Cleveland.Lorentz (embedContract, embedValue, importValue)

test_Import :: [TestTree]
test_Import =
  [ testScenario "Can embed a contract" $ scenario do
      contract <- originateSimple "basic1" []
        $$(embedContract @() @[Integer] @() "../../contracts/basic1.tz")

      call contract CallDefault ()
  ]

test_Embed :: [TestTree]
test_Embed =
  [ testScenario "Can embed a contract" $ scenario do
      contractCode <- importContract @() @[Integer] @() "../../contracts/basic1.tz"
      contract <- originateSimple "basic1" [] contractCode

      call contract CallDefault ()
  ]

test_ImportInt :: [TestTree]
test_ImportInt =
  [ testScenarioOnEmulator "Can import an int value" $ scenario do
      value <- runIO $ importValue @Integer "./test/fixtures/intValue.txt"

      value @== 42
  ]

test_EmbedInt :: [TestTree]
test_EmbedInt =
  [ testScenarioOnEmulator "Can embed an int value" $ scenario do
      let value = $$(embedValue @Integer "./test/fixtures/intValue.txt")

      value @== 42
  ]

test_ImportAddress :: [TestTree]
test_ImportAddress =
  [ testScenarioOnEmulator "Can import an address value" $ scenario do
      value <- runIO $ importValue @Address "./test/fixtures/addrValue.txt"

      value @== [ta|tz1Se5MhFNJdPrur2iNwC3nCLvYzr1y9TsZp|]
  ]

test_EmbedAddress :: [TestTree]
test_EmbedAddress =
  [ testScenarioOnEmulator "Can embed an address value" $ scenario do
      let value = $$(embedValue @Address "./test/fixtures/addrValue.txt")

      value @== [ta|tz1Se5MhFNJdPrur2iNwC3nCLvYzr1y9TsZp|]
  ]

test_ImportMutez :: [TestTree]
test_ImportMutez =
  [ testScenarioOnEmulator "Can import a mutez value" $ scenario do
      value <- runIO $ importValue @Mutez "./test/fixtures/intValue.txt"

      value @== 42
  ]

test_EmbedMutez :: [TestTree]
test_EmbedMutez =
  [ testScenarioOnEmulator "Can embed a mutez value" $ scenario do
      let value = $$(embedValue @Mutez "./test/fixtures/intValue.txt")

      value @== 42
  ]
