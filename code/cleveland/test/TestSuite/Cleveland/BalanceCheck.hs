-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.BalanceCheck
  ( test_BalanceCheck
  , test_EmptyBalanceCheck
  ) where

import Test.Tasty (TestTree)

import Morley.Tezos.Address (Address)
import Morley.Util.SizedList qualified as SL
import Morley.Util.SizedList.Types
import Test.Cleveland

test_BalanceCheck :: TestTree
test_BalanceCheck =
  testScenario "An address's balance can be checked" $ scenario do
    test :: Address <- newFreshAddress auto

    comment "balance is updated after transfer"
    transferMoney test 100
    getBalance test @@== 100

test_EmptyBalanceCheck :: TestTree
test_EmptyBalanceCheck =
  testScenario "An empty address' balance can be checked" $ scenario do
    test ::< dummy ::< Nil' <- traverse newFreshAddress $ SL.replicateT auto

    -- Doing something in order not to get "validating empty scenario" error
    transferMoney dummy 1
    getBalance test @@== 0
