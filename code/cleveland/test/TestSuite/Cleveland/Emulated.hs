-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.Emulated
  ( test_Emulated
  ) where

import Test.Tasty (TestTree, testGroup)

import Morley.Tezos.Address
import Test.Cleveland

import TestSuite.Util (shouldFailWithMessage)

test_Emulated :: [TestTree]
test_Emulated =
  [ testGroup "branchout"
    [ testScenarioOnEmulator "passes if all branches pass" $ scenarioEmulated $
        branchout
          [ "a" ?- pass
          , "b" ?- pass
          ]

    , testScenarioOnEmulator "fails if any branch fails" $ scenarioEmulated do
        addr <- newAddress auto
        let branchA = "a" ?- transferMoney addr 0
        let branchB = "b" ?- pass
        branchout [branchA, branchB] & expectTransferFailure emptyTransaction
        branchout [branchB, branchA] & expectTransferFailure emptyTransaction

    , testScenarioOnEmulator "a branch's effects do not leak into another branch" $ scenarioEmulated do
        test <- newAddress auto
        initialBalance <- getBalance test
        branchout
          [ "a" ?- do
              transferMoney test 2
              getBalance test @@== initialBalance + 2
          , "b" ?- do
              transferMoney test 3
              getBalance test @@== initialBalance + 3
          ]

    , testScenarioOnEmulator "branchout's effects are discarded" $ scenarioEmulated do
        test <- newAddress auto
        initialBalance <- getBalance test
        branchout
          [ "a" ?- transferMoney test 3 ]
        getBalance test @@== initialBalance

    , testScenarioOnEmulator "adds branch name to error" $ scenarioEmulated do
        branchout
          [ "<branch name>" ?- void $
              signBytes "" (unsafe $ parseAddress "tz1Zpj6cNhkVkvksGtRTwTrgfCG3WP9eA5BM")
          ]
          & shouldFailWithMessage "<branch name>"
    ]
  , testGroup "offshoot"
    [ testScenarioOnEmulator "fails if inner scenario fails" $ scenarioEmulated do
        addr <- newAddress auto
        offshoot "a" (transferMoney addr 0)
          & expectTransferFailure emptyTransaction

    , testScenarioOnEmulator "offshoot's effects are discarded" $ scenarioEmulated do
        test <- newAddress auto
        initialBalance <- getBalance test
        offshoot "a" $ transferMoney test 1
        getBalance test @@== initialBalance
    ]
  ]
