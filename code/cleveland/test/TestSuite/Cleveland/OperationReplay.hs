-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.OperationReplay
  ( test_replayOrigination_fails
  , test_replayTransfer_fails
  ) where

import Test.Tasty (TestTree)

import Morley.Tezos.Address (Address)
import Test.Cleveland
import Test.Util.Contracts
import TestSuite.Util (idContract, shouldFailWithMessage)

test_replayTransfer_fails :: TestTree
test_replayTransfer_fails =
  testScenarioOnEmulator "Transfer operation replay is prohibited" $ scenarioEmulated do
    dummyAddress <- chAddress <$> originateSimple @() @() "dummy" () idContract
    contract <- importContract @Address @() @() (contractsDir </> "replay_transfer.tz")
    replayTransfer <- originateSimple "replayTransfer" () contract
    call replayTransfer CallDefault dummyAddress
      & shouldFailWithMessage "Operation replay attempt"

test_replayOrigination_fails :: TestTree
test_replayOrigination_fails =
  testScenarioOnEmulator "Origination operation replay is prohibited" $ scenarioEmulated do
    contract <- importContract @() @() @() (contractsDir </> "replay_origination.tz")
    replayOrigination <- originateSimple "replayOrigination" () contract
    call replayOrigination CallDefault ()
      & shouldFailWithMessage "Operation replay attempt"
