-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.PrefixNetworkScenario
  ( test_PrefixNetworkScenario
  ) where

import Data.Fixed (Nano)
import Data.Text (takeEnd)
import Data.Time.Clock.POSIX (getPOSIXTime)
import System.IO.Temp (withSystemTempDirectory)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase, (@=?))

import Morley.Client as Client
import Morley.Client.TezosClient.Types (tceAliasPrefixL, tceMbTezosClientDataDirL)
import Morley.Tezos.Crypto (SecretKey, detSecretKey)
import Test.Cleveland
import Test.Cleveland.Internal.Abstract (Moneybag(Moneybag), moneybagL)
import Test.Cleveland.Internal.Client
  (neMoneybagAliasL, neMorleyClientEnvL, neSecretKeyL, runClevelandT)
import Test.Cleveland.Tasty (modifyNetworkEnv, whenNetworkEnabled)

aliasName :: Alias
aliasName = mkAlias "testMoneybagAlias"

aliasPrefix :: Text
aliasPrefix = "testPrefix"

mangleNetworkEnv :: SecretKey -> TestTree -> TestTree
mangleNetworkEnv sk = modifyNetworkEnv $
    (neMorleyClientEnvL . mceTezosClientL . tceAliasPrefixL .~ Just aliasPrefix)
  . (neSecretKeyL .~ Just sk)
  . (neMoneybagAliasL .~ aliasName)

setTmpDir :: FilePath -> NetworkEnv -> NetworkEnv
setTmpDir dir = neMorleyClientEnvL . mceTezosClientL . tceMbTezosClientDataDirL .~ Just dir

test_PrefixNetworkScenario :: IO TestTree
test_PrefixNetworkScenario = do
  -- use 7 digits of the current POSIX time in nanoseconds to generate seed
  -- this ensures we test for a fresh key more often than not
  -- taking 7 digits ensures that we're generating Ed25519 key.
  seed <- encodeUtf8 . takeEnd 7 . show @Text . realToFrac @_ @Nano <$> getPOSIXTime
  let key = detSecretKey seed
  pure $ mangleNetworkEnv key $ whenNetworkEnabled $ \withEnv ->
    testCase "Moneybag alias is not affected by alias prefix" $ withEnv $ \env' ->
      -- NB: we can't have 'withSystemTempDirectory' outside the test because
      -- the outer IO action finishes before the test itself is run.
      -- hence we have to change the tmpdir in env inside the test too
      withSystemTempDirectory "tezos-client" \tmpdir -> do
        let env = setTmpDir tmpdir env'
        Moneybag moneybagAddress <- runClevelandT env $ view moneybagL

        actualAlias <- runMorleyClientM (neMorleyClientEnv env) $
          Client.getAlias $ AddressResolved moneybagAddress
        actualAlias @=? aliasName
