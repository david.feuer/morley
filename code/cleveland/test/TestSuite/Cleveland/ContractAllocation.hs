-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.ContractAllocation
  ( test_ContractAllocation
  ) where

import Test.Tasty (TestTree)

import Morley.Tezos.Address (Address)
import Morley.Util.Named
import Test.Cleveland

import TestSuite.Cleveland.Lorentz.Contracts.ContractAllocator

test_ContractAllocation :: TestTree
test_ContractAllocation =
  testScenario "Contracts can be allocated and called" $ scenario do
    addresses :: [Address] <- replicateM 10 $ newFreshAddress auto
    let
      od = OriginateData
        { odName = "allocator"
        , odStorage = #storage :! (take 570 $ cycle addresses)
        , odBalance = 25
        , odContract = allocatorContract
        }

    allocatorAddr <- originate od

    call allocatorAddr CallDefault (take 25 $ cycle addresses)
