-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.ChainIdGet
  ( test_ChainIdGet
  ) where

import Test.Tasty (TestTree)

import Lorentz ((#))
import Lorentz qualified as L
import Morley.Tezos.Core (ChainId, dummyChainId)
import Morley.Util.Named
import Test.Cleveland

type Storage = ("storage" :! ChainId)

checkChainIdContract :: L.Contract () Storage ()
checkChainIdContract = L.defaultContract $
  L.drop # L.chainId # L.toNamed #storage # L.nil @L.Operation # L.pair

test_ChainIdGet :: TestTree
test_ChainIdGet =
  testScenario "ChainId is retrievable" $ scenario do
    helperAddr <- originateSimple "helper" (#storage :! dummyChainId) checkChainIdContract

    chainId <- getChainId
    call helperAddr CallDefault ()
    getStorage helperAddr @@== #storage :! chainId
