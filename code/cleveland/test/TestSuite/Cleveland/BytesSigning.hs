-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.BytesSigning
  ( test_BytesSigning
  ) where

import Test.Tasty (TestTree)

import Lorentz ((#))
import Lorentz qualified as L
import Morley.Tezos.Crypto
import Morley.Util.SizedList.Types
import Test.Cleveland

checkSignatureContract :: L.Contract (PublicKey, (L.TSignature ByteString, ByteString)) () ()
checkSignatureContract = L.defaultContract $
  L.car #
  L.unpair # L.dip L.unpair #
  L.checkSignature # L.assert [L.mt|Invalid signature|] #
  L.unit # L.nil @L.Operation # L.pair

test_BytesSigning :: TestTree
test_BytesSigning =
  testScenario "Bytestrings can be signed" $ scenario do
    dummy :< signer :< Nil <- traverse newFreshAddress $ "user" :< "signer" :< Nil

    helper <- originateSimple "helper" () checkSignatureContract

    let bytes = "some bytestring"

    sig <- signBinary bytes signer
    signerPK <- getPublicKey signer
    dummyPK <- getPublicKey dummy

    expectFailedWith [L.mt|Invalid signature|] $
      call helper CallDefault (dummyPK, (sig, bytes))
    call helper CallDefault (signerPK, (sig, bytes))
