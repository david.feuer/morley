<!-- Unreleased: append new entries here -->


0.1.1
=====
* [!1113](https://gitlab.com/morley-framework/morley/-/merge_requests/1113)
  Remove uses of `Parser LetEnv` due to redundant constraint.
* [!1094](https://gitlab.com/morley-framework/morley/-/merge_requests/1094)
  Deprecate morley language extensions
  + Functions for importing and testing Morley contracts are added: `Test.Cleveland.Michelson.Import.testTreesWithUntypedContractExt`, `Test.Cleveland.Michelson.Import.testTreesWithTypedContractExt`, `Test.Cleveland.Lorentz.Import.importContractExt`. All these functions are considered deprecated.
* [!965](https://gitlab.com/morley-framework/morley/-/merge_requests/965)
  Use Morley's fixed-size lists
  + Add Hedgehog generators for sized lists.
  + Introduce `enumAliasHints` for generating a sized list of similarly named alias hints.
* [!1072](https://gitlab.com/morley-framework/morley/-/merge_requests/1072)
  Add `runCode` to Cleveland
* [!1070](https://gitlab.com/morley-framework/morley/-/merge_requests/1070)
  Simplify cleveland's internals & public api
  + Relaced the `NiceUnpackedValue (AsRPC st)` constraint in `getStorage`
  + Removed the `NicePackedValue k` constraint from `getAllBigMapValuesMaybe`,
    `getAllBigMapValues`, `getBigMapSizeMaybe`, and `getBigMapSize`.
* [!1025](https://gitlab.com/morley-framework/morley/-/merge_requests/1025)
  Reduce the number of unsafe functions at the call site
  + Remove `unsafeFromHex` converter that is used as counter-part of safe `fromHex` and replaced with `unsafe . fromHex`.
  + Refactor `fromHex` to return `Either` instead of `Maybe`.
  + Add `parseAddressFromHex` helper to get rid of duplicated usages of `parseAddressRaw . unsafe . fromHex`.
* [!978](https://gitlab.com/morley-framework/morley/-/merge_requests/978)
  Make it difficult to misuse 'Show'
  + Use `Buildable` and `pretty` preferrentially.
  + Add `Buildable` instances for `FaluireReason`, `Either a b`.
  + Add `PrettyShow` instances for `TypeSpec` (in lorentz-test) and `Time _` (in cleveland itself)./
  + Avoid using `(+||)` and `(||+)`.
  + Use `Debug.show` when defining `Show` instances.
* [!1058](https://gitlab.com/morley-framework/morley/-/merge_requests/1058)
  Add `Buildable` instances for Unit, tuples up to 8 elements & bytestrings
* [!1057](https://gitlab.com/morley-framework/morley/-/merge_requests/1057)
  Test `Show` and `RenderDoc` instances of `Annotation t`, `AnnotationSet`, `Notes t`, `InstrAbstract op`
* [!997](https://gitlab.com/morley-framework/morley/-/merge_requests/997)
  Check necessity in initial `cast` automatically in Lorentz contracts
* [!1044](https://gitlab.com/morley-framework/morley/-/merge_requests/1044)
  Fix `getRunMode` and `whenEmulated`
  + `getRunMode` is replaced by `ifEmulation` helper.
  + A new helper `whenNetwork` is added.
  + One caveat is that functions passed to `ifEmulation` are universally quantified over the outer monad, so if additional constraints are required beyond `MonadCleveland` or `MonadEmulated`, those constraints have to go on the base monad, e.g. `(MonadCleveland caps m, MonadFail (ClevelandBaseMonad caps)) => m ()`.
ifEmulation.
* [!1043](https://gitlab.com/morley-framework/morley/-/merge_requests/1043)
  Extract actual + expected pattern into a datatype and show diff in type mismatch
* [!1037](https://gitlab.com/morley-framework/morley/-/merge_requests/1037)
  Add synonyms for `SomeConstrainedValue`
  + Reimplement `SomeConstant`, `SomeValue`, `SomeStorage` via `SomeConstrainedValue`.

0.1.0
=====
Initial release extracted from the [morley](https://hackage.haskell.org/package/morley)
and [lorentz](https://hackage.haskell.org/package/lorentz) packages.
Tezos testing framework for contract developed with Michelson, Morley or Lorentz.
