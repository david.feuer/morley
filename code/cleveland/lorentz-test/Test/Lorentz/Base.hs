-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for basic Lorentz functionality.

module Test.Lorentz.Base
  ( unit_parseLorentzValue
  ) where

import Test.HUnit (Assertion, (@?=))

import Lorentz.Base (parseLorentzValue)
import Morley.Michelson.Parser (codeSrc)
import Morley.Michelson.Typed (IsoValue)

-- Sum type.
data S
  = S1 Bool
  | S2
  | S3 Integer
  deriving stock (Generic, Eq, Show)
  deriving anyclass (IsoValue)

-- Product type.
type P = (Bool, Integer, S, S)

unit_parseLorentzValue :: Assertion
unit_parseLorentzValue = do
  parseLorentzValue codeSrc "Left True" @?= Right (S1 True)
  parseLorentzValue @P codeSrc "Pair (Pair False 20) (Pair (Right (Left Unit)) (Right (Right 15)))" @?=
    Right (False, 20, S2, S3 15)
