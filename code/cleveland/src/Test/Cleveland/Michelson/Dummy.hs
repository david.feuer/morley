-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Dummy data to be used in tests where it's not essential.

module Test.Cleveland.Michelson.Dummy
  {-# DEPRECATED "Use Morley.Michelson.Runtime.Dummy or Morley.Tezos.Core directly instead" #-}
  ( dummyNow
  , dummyLevel
  , dummyMaxSteps
  , dummyBigMapCounter
  , dummyContractEnv
  , dummyGlobalCounter
  , dummyOrigination
  , dummyChainId
  ) where

import Morley.Michelson.Runtime.Dummy
import Morley.Tezos.Core
