-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Utilities for testing documentations sanity.
--
-- These tests serve to ensure that documentation generation is not broken and
-- that user follows sane documentation structure (e.g. contract should be
-- named, some entities require description, e.t.c).
--
-- This module only re-exports modules "Test.Cleveland.Doc.Michelson" and
-- "Test.Cleveland.Doc.Lorentz"
module Test.Cleveland.Doc
  ( module Test.Cleveland.Doc.Michelson
  , module Test.Cleveland.Doc.Lorentz
  ) where

import Test.Cleveland.Doc.Lorentz
import Test.Cleveland.Doc.Michelson
