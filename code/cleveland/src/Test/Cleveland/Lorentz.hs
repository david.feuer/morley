-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-deprecations #-}

module Test.Cleveland.Lorentz
  ( -- * Importing a contract
    specWithContract
  , specWithTypedContract
  , specWithUntypedContract
  , importContract
  , embedContract
  , embedContractM

  -- * Importing a value
  , importValue
  , embedValue
  , embedValueM

  -- * Unit testing
  , Michelson.ContractReturn
  , Michelson.ContractPropValidator
  , Michelson.contractProp
  , Michelson.contractPropVal
  , testContractCoversEntrypointsT
  , testContractMatchesEntrypointsT
  , testContractCoversEntrypoints
  , testContractMatchesEntrypoints

  -- * Basic types
  , ContractHandle (..)
  , chNiceParameterEvi
  , chNiceStorageEvi

  , ToStorageType (..)

  -- * Conversions
  , toAddress

  -- * Autodoc testing
  , runDocTests
  , testLorentzDoc
  , excludeDocTests

  -- * General utilities
  , failedTest
  , succeededTest
  , eitherIsLeft
  , eitherIsRight
  , meanTimeUpperBoundProp
  , meanTimeUpperBoundPropNF

  -- * Re-exports
  --
  -- | These functions from @Time@ are re-exported here to make it convenient to call
  -- 'meanTimeUpperBoundProp' and 'meanTimeUpperBoundPropNF'.
  , mcs, ms, sec, minute

  -- * Dummy values
  , dummyContractEnv

  -- * Special contracts for testing
  , contractConsumer
  ) where

import Test.Cleveland.Doc.Lorentz
import Test.Cleveland.Lorentz.Consumer
import Test.Cleveland.Lorentz.Entrypoints
import Test.Cleveland.Lorentz.Import
import Test.Cleveland.Lorentz.Types
import Test.Cleveland.Michelson.Dummy
import Test.Cleveland.Michelson.Import
  (specWithContract, specWithTypedContract, specWithUntypedContract)
import Test.Cleveland.Michelson.Unit qualified as Michelson
import Test.Cleveland.Util
