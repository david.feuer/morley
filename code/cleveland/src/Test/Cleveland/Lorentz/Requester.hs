-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-redundant-constraints #-}

-- | Contract that call on-chain views and keep the result in storage.
--
-- This is like 'Test.Clevelad.Lorentz.Consumer.contractConsumer', but it is
-- _active_ - you call it, not the contract containing the view, in order to
-- access the view.
--
-- TODO [#708]: mention that calling views directly should be preferred.
module Test.Cleveland.Lorentz.Requester
  ( contractRequester
  ) where

import Lorentz
import Prelude hiding (swap, view)

import Morley.Util.TypeLits
import Test.Cleveland.Lorentz.Types

-- | Allows calling a view multiple times, remembering results in
-- last-goes-first order.
--
-- > contractRequester @"viewName" contractWithView
contractRequester
  :: forall name arg ret vd cp st.
     ( NiceParameterFull cp, NiceStorage cp
     , NiceParameterFull arg, NiceStorageFull [ret]
     , NiceViewable arg, NiceViewable ret, HasView vd name arg ret
     , Typeable vd, KnownSymbol name
     )
  => ContractHandle cp st vd -> Contract arg [ret] ()
contractRequester target = defaultContract $
  unpair #
  push (toTAddress target) # swap # view @name # cons #
  nil # pair
