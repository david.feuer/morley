-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Michelson contracts testing on a real Tezos network.
--
-- See the [documentation](https://gitlab.com/morley-framework/morley/-/blob/master/code/cleveland/testingEDSL.md)
-- for usage instructions.
module Test.Cleveland
  ( AliasHint
  , ContractHandle (..)
  , OriginateData (..)
  , UntypedOriginateData (..)
  , TransferData (..)
  , Scenario
  , PureM

  -- * Validation
  , TransferFailure

  -- * Real network implementation based on @tezos-client@ and RPC.
  , testScenario
  , testScenarioOnNetwork
  , scenario

  -- * Emulation tests
  , testScenarioOnEmulator
  , scenarioEmulated
  , withInitialNow
  , withInitialLevel
  , collectLogs
  , logsForAddress

  -- * Cleveland actions
  , MonadCleveland
  , MonadOps
  , MonadEmulated
  , ClevelandT
  , EmulatedT
  , withSender
  , withMoneybag
  , runIO
  , resolveAddress
  , refillable
  , newAddress
  , newFreshAddress
  , enumAliasHints
  , signBytes
  , signBinary
  , originate
  , originateSimple
  , originateUntyped
  , originateUntypedSimple
  , originateTypedSimple
  , originateLarge
  , originateLargeSimple
  , originateLargeUntyped
  , originateLargeUntypedSimple
  , transfer
  , transferMoney
  , call
  , importUntypedContract
  , importContract
  , noViews
  , comment
  , getBalance
  , getDelegate
  , registerDelegate
  , getMorleyLogs
  , getMorleyLogs_
  , getStorage
  , getFullStorage
  , getSomeStorage
  , getAllBigMapValues
  , getAllBigMapValuesMaybe
  , getBigMapSize
  , getBigMapSizeMaybe
  , getBigMapValueMaybe
  , getBigMapValue
  , getPublicKey
  , getChainId
  , advanceTime
  , advanceLevel
  , advanceToLevel
  , getNow
  , getLevel
  , getApproximateBlockInterval
  , RunCode(..)
  , AsRPC.MaybeRPC(..)
  , runCode
  , branchout
  , offshoot
  , setVotingPowers
  , whenEmulation
  , whenNetwork
  , ifEmulation
  , inBatch

  -- * Primitives re-exports
  , EntrypointRef (..)
  , VotingPowers
  , mkVotingPowers
  , mkVotingPowersFromMap

  -- * Assertions
  , failure
  , assert
  , (@==)
  , (@/=)
  , (@@==)
  , (@@/=)
  , checkCompares
  , checkComparesWith
  , evalJust
  , evalRight
  , Showing(..)

    -- * Exception handling
  , attempt
  , catchTransferFailure
  , checkTransferFailure
  , expectTransferFailure
  , expectFailedWith
  , expectError
  , expectCustomError
  , expectCustomError_
  , expectCustomErrorNoArg
  , expectNumericError
  , clarifyErrors

  -- ** Exception predicates
  , TransferFailurePredicate
  , shiftOverflow
  , emptyTransaction
  , badParameter
  , gasExhaustion
  , failedWith
  , addressIs
  -- ** @FAILWITH@ errors
  -- | Convert the many error formats available in @morley@ and @lorentz@ to t'Morley.Michelson.Typed.Existential.SomeConstant'.
  , constant
  , lerror
  , customError
  , customError_
  , customErrorNoArg
  , numericError

  -- * Helpers
  , auto
  , pattern DefEpName
  , ep
  , (?-)

  -- * Integration with @hedgehog@
  , testScenarioProps

  -- * Config (reexports)
  , TezosClientEnv(..)
  , MorleyClientEnv
  , MorleyClientEnv'(..)
  , NetworkEnv (..)
  , MorleyLogs
  -- ** Config utilities
  , mkMorleyOnlyRpcEnvNetwork

  -- * AsRPC
  , AsRPC.HasRPCRepr(AsRPC)
  , AsRPC.deriveRPC
  , AsRPC.deriveRPCWithStrategy
  , AsRPC.deriveManyRPC
  , AsRPC.deriveManyRPCWithStrategy

  -- * Utilities
  , mapEach
  , forEach
  ) where

import Lorentz (EntrypointRef(..), pattern DefEpName)
import Morley.AsRPC qualified as AsRPC
import Morley.Client (MorleyClientEnv, MorleyClientEnv'(..))
import Morley.Client.TezosClient.Types (TezosClientEnv(..))
import Morley.Michelson.Interpret (MorleyLogs)
import Morley.Michelson.Runtime (VotingPowers, mkVotingPowers, mkVotingPowersFromMap)
import Test.Cleveland.Instances ()
import Test.Cleveland.Internal.Abstract
import Test.Cleveland.Internal.Actions
import Test.Cleveland.Internal.Client
import Test.Cleveland.Internal.Hedgehog
import Test.Cleveland.Internal.Pure
import Test.Cleveland.Internal.Scenario
import Test.Cleveland.Tasty.Internal
import Test.Cleveland.Util
