-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.Tezos.Crypto
  ( genPublicKey
  , genSecretKey
  , genSignature
  , genKeyHashTag
  , genKeyHash
  ) where

import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Morley.Tezos.Crypto
  (KeyHash, KeyHashTag, PublicKey, SecretKey(..), Signature(..), hashKey, signatureLengthBytes,
  toPublic)

import Hedgehog.Gen.Tezos.Crypto.Ed25519 qualified as Ed25519
import Hedgehog.Gen.Tezos.Crypto.P256 qualified as P256
import Hedgehog.Gen.Tezos.Crypto.Secp256k1 qualified as Secp256k1

genPublicKey :: MonadGen m => m PublicKey
genPublicKey = toPublic <$> genSecretKey

genSecretKey :: MonadGen m => m SecretKey
genSecretKey = Gen.choice
  [ SecretKeyEd25519 <$> Ed25519.genSecretKey
  , SecretKeySecp256k1 <$> Secp256k1.genSecretKey
  , SecretKeyP256 <$> P256.genSecretKey
  ]

genSignature :: MonadGen m => m Signature
genSignature = Gen.choice
  [ SignatureEd25519 <$> Ed25519.genSignature
  , SignatureSecp256k1 <$> Secp256k1.genSignature
  , SignatureP256 <$> P256.genSignature
  , SignatureGeneric <$> Gen.bytes (Range.singleton signatureLengthBytes)
  ]

genKeyHashTag :: MonadGen m => m KeyHashTag
genKeyHashTag = Gen.enumBounded

genKeyHash :: MonadGen m => m KeyHash
genKeyHash = hashKey <$> genPublicKey
