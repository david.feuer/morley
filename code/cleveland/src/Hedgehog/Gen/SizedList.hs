-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.SizedList
  ( genSizedList
  , genSomeSizedList
  ) where


import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Morley.Util.SizedList qualified as SL
import Morley.Util.SizedList.Types

genSizedList
  :: forall n m a n'. (SingIPeano n, IsoNatPeano n n', MonadGen m)
  => m a -> m (SizedList' n' a)
genSizedList el = sequence $ pure el

genSomeSizedList
  :: forall m a. (MonadGen m)
  => Range.Range Int -> m a -> m (SomeSizedList a)
genSomeSizedList len el = SL.fromList <$> Gen.list len el
