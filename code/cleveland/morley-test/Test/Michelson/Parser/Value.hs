-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Michelson.Parser.Value
  ( test_exponentialBacktracking
  ) where

import Test.Tasty (TestTree, localOption, mkTimeout)
import Test.Tasty.HUnit (testCase, (@?=))

import Morley.Michelson.Parser as P
import Morley.Michelson.Untyped.Value

millisec :: Integer
millisec = 1000

test_exponentialBacktracking :: TestTree
test_exponentialBacktracking = localOption (mkTimeout $ 500 * millisec) $
  testCase "Parser is able to parse nested pairs in reasonable time" do
    let parsedValue = P.parseNoEnv @() P.value "" "Pair 0 (Pair 1 (Pair 2 (Pair 3 \
          \(Pair 4 (Pair 5 (Pair 6 (Pair 7 (Pair 8 (Pair 9 (Pair 10 (Pair 11 \
          \(Pair 12 (Pair 13 (Pair 14 (Pair 15 16)))))))))))))))"
        expected = foldr1 ValuePair $ ValueInt <$> 0 :| [1..16]

    parsedValue @?= Right expected
