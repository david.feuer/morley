-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for preprocessing.

module Test.Preprocess
  ( unit_Sample_preprocess
  ) where

import Prelude hiding (EQ)

import Test.HUnit (Assertion, (@?=))

import Morley.Michelson.Preprocess (transformBytes, transformStrings)
import Morley.Michelson.Text
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.Instr
import Morley.Michelson.Untyped.Annotation (noAnn)

unit_Sample_preprocess :: Assertion
unit_Sample_preprocess = do
  transformStrings False (f (unsafe . mkMText . toText)) (sample (T.VString . unsafe . mkMText))
    @?= (expected (T.VString . unsafe . mkMText))
  transformBytes False (f fromString) (sample T.VBytes) @?= (expected T.VBytes)
  where
    f :: (Eq a) => (String -> a) -> a -> a
    f fn s
      | s == (fn str1) = (fn str5)
      | s == (fn str2) = (fn str4)
      | s == (fn str4) = (fn str2)
      | s == (fn str5) = (fn str1)
      | otherwise = s

sample :: (IsString i, T.ConstantScope o) => (i -> T.Value o) -> T.ContractCode o o
sample fn =
  CAR `Seq`
  PUSH (fn str1) `Seq`
  DIP (PUSH (fn str2)) `Seq`
  (AnnLEFT @('T.TKey) noAnn noAnn noAnn) `Seq`
    IF_LEFT Nop (PUSH (fn str4) `Seq` FAILWITH) `Seq`
  DIP (DIP (PUSH (fn str5))) `Seq`
  PUSH (fn str3) `Seq`
  DROP `Seq` DROP `Seq` DROP `Seq` DROP `Seq`
  NIL `Seq` PAIR

expected :: (IsString i, T.ConstantScope o) => (i -> T.Value o) -> T.ContractCode o o
expected fn =
  CAR `Seq`
  PUSH (fn str5) `Seq`
  DIP (PUSH (fn str4)) `Seq`
  (AnnLEFT @('T.TKey) noAnn noAnn noAnn) `Seq`
    IF_LEFT Nop (PUSH (fn str2) `Seq` FAILWITH) `Seq`
  DIP (DIP (PUSH (fn str1))) `Seq`
  PUSH (fn str3) `Seq`
  DROP `Seq` DROP `Seq` DROP `Seq` DROP `Seq`
  NIL `Seq` PAIR

str1, str2, str3, str4, str5 :: (IsString a) => a
str1 = "aa"
str2 = "ls"
str3 = "gulya"
str4 = "naiks"
str5 = "eek"
