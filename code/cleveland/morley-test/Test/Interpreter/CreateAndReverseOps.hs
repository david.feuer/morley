-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for 'create_2_n_reverse_ops.tz' contract.

-- Here we check that by storing 'GlobalCounter' in 'OriginationOpertion'
-- contract addresses aren't confused with each other even if we reverse the
-- list of operations manually in the contract.

module Test.Interpreter.CreateAndReverseOps
  ( test_createAndReverseOps
  ) where

import Test.Tasty (TestTree)

import Morley.Michelson.Text (MText)
import Morley.Tezos.Address
import Test.Cleveland
import Test.Cleveland.Instances ()
import Test.Util.Contracts

test_createAndReverseOps :: IO TestTree
test_createAndReverseOps =
  pure $ testScenario "Resuling addresses match each other" $ scenario do
    contract <- importContract @() @(Address, Address) @()
      (contractsDir </> "create_2_n_reverse_ops.tz")
    createAndReverseOps <- originateSimple "createAndReverseOps" (someAddr, someAddr) contract
    call createAndReverseOps CallDefault ()
    (addrWithUnitStorage, addrWithStringStorage) <-
      getStorage @(Address, Address) createAndReverseOps
    () <- getStorage @() addrWithUnitStorage
    stString <- getStorage @MText addrWithStringStorage
    stString @== "kek"

someAddr :: Address
someAddr = unsafe $ parseAddress "tz1NJRjyBXqAmBf94FLTTuQWZGHpmGG4CWKe"
