-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for 'transfer_n_create.tz' contract. See [#643]
module Test.Interpreter.TransferAndCreate
  ( test_transferAndCreate
  ) where

import Test.Tasty (TestTree)

import Morley.Tezos.Address
import Test.Cleveland
import Test.Cleveland.Instances ()
import Test.Util.Contracts

test_transferAndCreate :: IO TestTree
test_transferAndCreate =
  pure $ testScenario "'transfer_n_create.tz' performs origination after transfer" $ scenario do
    contract <- importContract @() @Address @() (contractsDir  </> "transfer_n_create.tz")
    transferAndCreate <- originateSimple "transferAndCreate" constAddr contract
    oldBalance <- getBalance constAddr
    transferMoney transferAndCreate 10
    newBalance <- getBalance constAddr
    newBalance - oldBalance @== 1
    newAddr <- getStorage @Address transferAndCreate
    -- Here we check that origination was performed
    () <- getStorage @() newAddr
    pure ()

-- Address hardcoded in 'transfer_n_create.tz'.
constAddr :: Address
constAddr = unsafe $ parseAddress "tz1NJRjyBXqAmBf94FLTTuQWZGHpmGG4CWKe"
