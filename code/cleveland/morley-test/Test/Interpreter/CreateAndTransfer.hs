-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for 'create_n_transfer.tz' contract. See [#643]
module Test.Interpreter.CreateAndTransfer
  ( test_createAndTranfser
  ) where

import Test.Tasty (TestTree)

import Morley.Tezos.Address
import Test.Cleveland
import Test.Cleveland.Instances ()
import Test.Util.Contracts

test_createAndTranfser :: IO TestTree
test_createAndTranfser =
  pure $ testScenario "'create_n_transfer.tz' performs transfer after origination" $ scenario do
    contract <- importContract @() @() @() (contractsDir </> "create_n_transfer.tz")
    createAndTransfer <- originateSimple "createAndTransfer" () contract
    oldBalance <- getBalance constAddr
    transferMoney createAndTransfer 10
    newBalance <- getBalance constAddr
    newBalance - oldBalance @== 1

-- Address hardcoded in 'create_n_transfer.tz'.
constAddr :: Address
constAddr = unsafe $ parseAddress "tz1NJRjyBXqAmBf94FLTTuQWZGHpmGG4CWKe"
