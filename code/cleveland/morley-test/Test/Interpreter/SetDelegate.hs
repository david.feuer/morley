-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for 'create_n_transfer.tz' contract. See [#643]
module Test.Interpreter.SetDelegate
  ( test_setDelegate
  ) where

import Test.Tasty (TestTree)

import Morley.Michelson.Typed
import Morley.Tezos.Crypto
import Test.Cleveland

contract :: Contract ('TOption 'TKeyHash) 'TUnit
contract = defaultContract $
  CAR `Seq` SET_DELEGATE `Seq` DIP NIL `Seq` CONS `Seq` DIP UNIT `Seq` PAIR

test_setDelegate :: IO TestTree
test_setDelegate =
  pure $ testScenario "setDelegate works" $ scenario do
    handle <- originateTypedSimple @(Maybe KeyHash) @_ @() "set_delegate" () contract
    getDelegate handle @@== Nothing
    addr <- newAddress "delegate"
    registerDelegate addr
    pk <- getPublicKey addr
    let kh = hashKey pk
    call handle CallDefault (Just kh)
    getDelegate handle @@== Just kh
    call handle CallDefault Nothing
    getDelegate handle @@== Nothing
