<!--
SPDX-FileCopyrightText: 2021 Oxhead Alpha
SPDX-License-Identifier: LicenseRef-MIT-OA
-->

# Cleveland: EDSL for Testing Michelson Contracts in Haskell

One possible way to test your Michelson contracts is to write tests in Haskell using Cleveland Testing EDSL.
This document explains how to do it.
The advantages of this approach are the following:
1. You can use the full power of Haskell libraries, such as [`hedgehog`](https://hackage.haskell.org/package/hedgehog), [`tasty`](https://hackage.haskell.org/package/tasty), etc.
2. Haskell has a lot of tools built around it: code formatters, linters, editor plugins, etc.
It makes writing code in Haskell quite pleasant and convenient.
3. Haskell syntax is quite concise and expressive, so tests written in Haskell are easy to read even for a person with little knowledge of the language.

## Table of contents

- [Prerequisites](#prerequisites)
- [Overview](#overview)
- [Write Your First Tests](#write-your-first-tests)
  - [Hello Tezos](#hello-tezos)
  - [Property Testing](#property-testing)
- [More complex scenarios](#more-complex-scenarios)
  - [Manipulating emulator state](#manipulating-emulator-state)
- [Managing Multiple Tests](#managing-multiple-tests)
- [Summary](#summary)

## Prerequisites

1. The reader should be familiar with the basic features of Haskell. Be able to define functions and data types, use pattern matching, guards, case expressions, do-notation, etc. Know fundamental types and functions from `base`.
2. Familiarity with testing libraries like `hedgehog` and `tasty` is also desirable.
3. In this document, we will use [`stack`](https://docs.haskellstack.org/en/stable/README/) to build Haskell code.

## Overview

Morley is essentially a set of developer tools for the Michelson Language written in Haskell.
In particular, it provides a testing framework, defining an EDSL for testing Michelson contracts.
There are two types of tests one can write:

* Property-based tests.

   These tests check that a contract (or indeed contracts) satisfy some
   predicate (property) for a semi-arbitrary randomly generated set of
   data.

   These tests run on the Morley interpreter, basically emulating a real
   network.

* Example-based tests.

   These tests check that after running some sequence of operations (like calling
   contracts), some predicate holds.

   Unlike property-based tests, these can be run either using the Morley
   interpreter, or on a real network (likely a testnet or a
   [local chain](https://gitlab.com/morley-framework/local-chain), as running
   these on the mainnet would be expensive).

## Write Your First Tests

*Disclaimer: the following examples correspond to a currently unreleased version of Cleveland (db64dee8375f92b4a1668b0a82a2f690c392329f) and may be incompatible with the stable morley release.*

This chapter provides a step-by-step guide to writing tests using the Cleveland EDSL.
We start with a very simple contract and test case and then proceed to more advanced features.
All files mentioned in this chapter can be found in the [`examples`](./examples/) directory.
You can use [Haddock documentation on Hackage](https://hackage.haskell.org/package/morley) to read more information about functions and data types used here.

### Hello Tezos

We start with a contract that unconditionally puts the "Hello Tezos!" string into its storage:

```
# hello_tezos.tz
parameter unit;
storage string;
code {DROP;
      PUSH string "Hello Tezos!";
      NIL operation; PAIR;};
```

Let's create a directory called `contracts/` and save this contract as `contracts/hello_tezos.tz`.
In this example, we will not create a complete Stack project but instead will use the ["script interpreter"](https://docs.haskellstack.org/en/stable/GUIDE/#script-interpreter) feature of Stack.

Let's create a file called `HelloTezosSpec.hs` with an example-based test for our `hello_tezos.tz` contract.
Here is the full test suite:

```haskell
#!/usr/bin/env stack
{- stack
  script
  --resolver snapshot.yaml
  --package base-noprelude
  --package tasty
  --package morley
  --package cleveland
  --package morley-prelude
  --ghc-options "-hide-package base"
-}

{-# LANGUAGE OverloadedStrings, QuasiQuotes, TypeApplications #-}

module HelloTezosSpec
  ( test_hello
  ) where

import Test.Tasty

import Morley.Michelson.Text (MText)
import Test.Cleveland
import Test.Cleveland.Tasty

main :: IO ()
main = clevelandMain test_hello

test_hello :: TestTree
test_hello = testScenario "contracts/hello_tezos.tz puts 'Hello Tezos!' in its storage" $ scenario $ do
  contract <- importContract @() @MText "contracts/hello_tezos.tz"
  contractHandle <- originateSimple "hello" "" contract
  call contractHandle CallDefault ()
  storage <- getStorage contractHandle
  storage @== "Hello Tezos!"
```

It starts with a shebang line to execute `stack` and arguments that will be passed to the `stack` executable.
We are using the following packages:
* `base-noprelude`, `text` are the most basic Haskell packages which are used almost everywhere.
* `tasty` is a generic Haskell testing framework we use to set up
the testing infrastructure.
* `morley` is the Morley library itself.
* `cleveland` is Morley testing framework.
* `morley-prelude` is the custom prelude made for the Morley project.

Then we enable `OverloadedStrings` to be able to write `Text` constants, and `TypeApplications` to be able to conveniently specify types where we need to.

Then we import some modules.
All the test logic is in `test_hello` of type `TestTree`.
You can treat it as the specification of the contract we want to test.
The test itself defines a single Cleveland scenario using the monadic interface. We first wrap the monadic scenario itself by calling `scenario`, then we use `testScenario` to turn a `Scenario` into a Tasty `TestTree`.

`scenario` defines a scenario that can be run on both the emulator (Morley interpreter) and a real network, and `testScenario` will attempt to run it on
both (given the corresponding settings, see the end of this section).

The scenario itself does the following:

1. It imports the contract from `contracts/hello_tezos.tz` using `importContract`. The result is a _typed_ Haskell representation of a contract.

    Note that we specify the types of the contract argument `()` -- i.e. the unit type in Haskell -- and the type of the storage `MText` using visible type application syntax.

    The reason we're using `MText` is that Tezos strings have some limitations compared to Haskell's `Text` or `String`. Other types have more straightforward mappings.

2. Then it originates the contract.

    By default, originations and transfers are done from what we call the "moneybag" account, i.e. an implicit account that has plenty of XTZ to pay the required fees. On a network, you need to provide such an account by running the test with command-line arguments (see below), while on the emulator this is taken care of automatically.

    The result of the origination is the originated contract's "handle". The handle contains the contract's address, type signature and alias.

3. The scenario now calls the contract's default entrypoint.

    This is signified by `CallDefault`, with `()` (i.e. unit) argument.

4. Then it gets the contract's current storage with `getStorage`.

    We don't need to specify the type of storage here, because `contractHandle` remembers the type of contract.

5. Finally, it compares the storage against the literal `"Hello Tezos!"`.

    Cleveland provides a few assertion helpers, notably `@==`, `@/=`, `@@==` and `@@/=`.

    The former two are "equal" and "not equal" assertions for plain values, and the latter two are convenience wrappers, where the first argument can be monadic, i.e. we could've written the last two lines as `getStorage contractHandle @@== "Hello Tezos!"`.

Now we can launch our test:

```
stack HelloTezosSpec.hs
```

Alternatively you can do `chmod +x HelloTezosSpec.hs` and run `./HelloTezosSpec.hs`.

It may take a while to download all the dependencies and compile them.
In the end, you should see the following output:

```
contracts/hello_tezos.tz puts 'Hello Tezos!' to its storage
  On emulator: OK

All 1 tests passed (0.00s)
```

By default, the test will only run on the emulator. To run on a real network, you'll need the `tezos-client` binary available on your system. Additionally, you need to specify a Tezos node you will be using and the moneybag secret key.

Run

```
stack HelloTezosSpec.hs --help
```

to get a full description of options, but in short, you can run

```
./HelloTezosSpec.hs \
  --cleveland-data-dir /some/temp/dir \
  --cleveland-moneybag-secret-key  'unencrypted:edsk...'  \
  --cleveland-node-endpoint https://some.testnet.node.endpoint \
  --cleveland-mode all
```

(of course, you'll need to specify a real existing directory instead of `/some/temp/dir`, a full unencrypted secret key and a real testnet endpoint)

You should see roughly the following result:

```
contracts/hello_tezos.tz puts 'Hello Tezos!' to its storage
  On emulator: OK (0.01s)
  On network:  Originated smart contract 'hello' with address KT...
OK (43.67s)

All 2 tests passed (43.68s)
```

Notice that tests on a real network can take a while, since most operations need to wait for inclusion into the blockchain, and depending on the current network load this can take some time.

### Property Testing

Now let's write a more advanced test.
This time we'll write a property-based test using `Hedgehog`.

Let's test the following contract:
```
# compare.tz
# Accepts a pair of values of type Mutez (Pair a b).
# Returns a bool list:
# [ a == b?
# , a >  b?
# , a <  b?
# , a >= b?
# , a <= b?
# ]

parameter (pair mutez mutez);
storage (list bool);
code {CAR; DUP; DUP; DUP; DUP; DIIIIIP {NIL bool};
      DIIIIP {DUP; CAR; DIP {CDR}; COMPARE; LE; CONS};
      DIIIP {DUP; CAR; DIP {CDR}; COMPARE; GE; CONS};
      DIIP{DUP; CAR; DIP {CDR}; COMPARE; LT; CONS};
      DIP {DUP; CAR; DIP {CDR}; COMPARE; GT; CONS};
      DUP; CAR; DIP {CDR}; COMPARE; EQ; CONS;
      NIL operation; PAIR};
```

Let's put it into `contracts/compare.tz`.

As you can see, it takes a pair of `Mutez` values and puts a `list` of `bool`s into its storage.
There are 5 values as described in the comment.

Again let's see our test straight away:

```haskell
#!/usr/bin/env stack
{- stack
  script
  --resolver snapshot.yaml
  --package base-noprelude
  --package tasty
  --package tasty-hedgehog
  --package hedgehog
  --package morley
  --package cleveland
  --package morley-prelude
  --ghc-options "-hide-package base"
-}

{-# LANGUAGE OverloadedStrings, TypeApplications #-}

module CompareSpec
  ( hprop_compare
  ) where

import Hedgehog (Gen, Property, forAll, property)
import Test.Tasty
import Test.Tasty.Hedgehog (testProperty)

import Hedgehog.Gen.Tezos.Core (genMutez)
import Morley.Tezos.Core (Mutez)
import Test.Cleveland
import Test.Cleveland.Tasty

main :: IO ()
main = clevelandMain $
  testProperty "compare" hprop_compare

genParameter :: Gen (Mutez, Mutez)
genParameter = (,) <$> genMutez <*> genMutez

hprop_compare :: Property
hprop_compare = property $ do
    input <- forAll genParameter
    testScenarioProps $ scenario $ do
      contract <- importContract @(Mutez, Mutez) @[Bool] "contracts/compare.tz"
      handle <- originateSimple "compare" [] contract
      call handle CallDefault input
      getStorage handle @@== mkExpected input

mkExpected :: (Mutez, Mutez) -> [Bool]
mkExpected (a, b) = [a == b, a > b, a < b, a >= b, a <= b]
```

It starts with the same lines, except that the module name is different and now we use the
`hedgehog` and `tasty-hedgehog` libraries in addition to the rest.

The scenario is pretty straightforward: it imports the contract, originates it, calls it with a supplied pair of `Mutez` values and checks that the storage has the expected value.

The interesting part here is in the `hprop_compare` function, which implements
a Hedgehog property we want to test. Note we're turning the `Property` into a `TestTree` with the `testProperty` function (imported from `Test.Tasty.Hedgehog`)

* We define a property with `property`
* We generate arbitrary `input` value via `forAll` function.
This `input` will be generated using the `genParameter` generator to
produce `(Mutez, Mutez)`.
* `genParameter` uses the `genMutez` generator which is located in the
`Hedgehog.Gen.Tezos.Core` module (part of Cleveland)
* Finally we run the monadic Cleveland scenario `ClevelandT m ()` using `scenario` and `testScenarioProps`, passing the generated input value to the scenario. Specifically, `testScenarioProps` is what allows us to run a scenario inside a Hedgehog property.

You can run this test the same way as `HelloTezos.hs` and you should see the following output:

```
Randomized property test: OK (0.22s)
    ✓ Randomized property test passed 100 tests.

All 1 tests passed (0.22s)
```

Note that property tests are never run on a network. Indeed, since we've just
ran the scenario 100 times, running this on a network would create an undue
load and simply take too long.

## More complex scenarios

Now let's try writing some more complex testing scenarios.

In our first example we will use two contracts – `string_caller.tz` and `fail_or_store_and_transfer.tz`:

```
# string_caller.tz
# This contract takes a string as parameter and an address as storage.
# It transfers 300 mutez to the given address and passes its parameter as
# parameter for this transfer.
# It fails if current timestamp is greater than 500.

parameter string;
storage address;
code {
       # Check current timestamp
       PUSH timestamp 500;
       NOW;
       IFCMPGT { FAIL; } { };
       # Construct operations
       DUP;
       DUP;
       CDR;
       CONTRACT string;
       IF_NONE {DROP; NIL operation }
               {SWAP;
                CAR;
                DIP {PUSH mutez 300};
                TRANSFER_TOKENS;
                DIP {NIL operation;};
                CONS;
               };
       DIP { CDR };
       PAIR;
     };

```

```
# fail_or_store_and_transfer.tz
# This contract takes a string as parameter and updates its storage to
# this string.
# However, it fails if its balance is greater than 1300.
# Also it transfers 5 mutez to a fixed address (tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU).

parameter string;
storage string;
code { CAR; # ignore storage
       # Check balance and possibly fail
       PUSH mutez 1300;
       BALANCE;
       IFCMPGT { FAIL; } { };
       # Construct transfer operation
       NIL operation;
       PUSH (contract unit) "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU";
       PUSH mutez 5;
       UNIT;
       TRANSFER_TOKENS;
       CONS;
       PAIR;};
```

Their behavior is pretty well described in their comments.
The test is located in `StringCallerSpec.hs`.

Let's skip some boilerplate from the beginning of this file and look at `test_string_caller`:

```haskell
test_string_caller :: [TestTree]
test_string_caller =
  [ testScenario (prefix <> "a constant" <> suffix) $ myScenario constStr
  , testProperty (prefix <> "an arbitrary value" <> suffix) $
      -- the test is somewhat redundant, so we limit it to two runs here
      withTests 2 $ property $ do
        str <- forAll genMText
        testScenarioProps $ myScenario str
  ]
  where
  constStr = "caller"
  prefix =
    "stringCaller calls failOrStoreAndTransfer and updates its storage with "
  suffix =
    " and properly updates balances. But fails if failOrStoreAndTransfer's\
    \ balance is ≥ 1300 and NOW is > 500"
```

Here our testing function returns a list of `TestTree`s, and we're testing both an example-based and a property-based scenario.

Notice we've moved the scenario itself to a separate function `myScenario`.

Nothing we didn't see before, but as a refresher:
- we're using `testScenario` to turn a scenario into a named Tasty test.
- we're using `testScenarioProps` to run a scenario inside a Hedgehog property.
- `property` and `testProperty` are used respectively to define a Hedgehog property and turn it into a Tasty test.

Here we also use `genMText` generator (imported from `Hedgehog.Gen.Michelson`, part of Cleveland) to generate an arbitrary `MText` in the property test.

The most interesting part happens in `myScenario`:

```haskell
myScenario :: Monad m => MText -> Scenario m
myScenario str = scenario $ do
  let
    initFailOrStoreBalance = 900
    initStringCallerBalance = 500

  failOrStoreAndTransfer
    <- importContract @MText @MText "contracts/fail_or_store_and_transfer.tz"
  stringCaller <- importContract @MText @Address "contracts/string_caller.tz"

  -- Originate both contracts
  failOrStoreAndTransferHandle <- originate OriginateData
    { odName = "failOrStoreAndTransfer"
    , odStorage = "hello"
    , odBalance = initFailOrStoreBalance
    , odContract = failOrStoreAndTransfer
    }
  stringCallerHandle <- originate OriginateData
    { odContract = stringCaller
    , odName = "stringCaller"
    , odStorage = toAddress failOrStoreAndTransferHandle
    , odBalance = initStringCallerBalance
    }
```

First, we import and originate both contracts.
Since our contracts will be making payments, we need to supply both the initial balance and the initial storage value for each contract, hence instead of `originateSimple` we use the more general `originate`, which accepts an `OriginateData` argument.

We pass the second contract's address to `stringCaller` so that `stringCaller` will call `failOrStoreAndTransfer` every time it's called.

Now let's transfer 100 mutez to `stringCaller`:

```haskell
  -- Transfer 100 mutez to stringCaller, it should transfer 300 mutez
  -- to failOrStoreAndTransfer
  let transferToStringCaller =
        transfer TransferData
          { tdTo = stringCallerHandle
          , tdParameter = str
          , tdAmount = 100
          , tdEntrypoint = DefEpName
          }
  transferToStringCaller
```

This transfer should succeed.
* `stringCaller` should receive 100 mutez and send 300 mutez.
* `failOrStoreAndTransfer` should receive 300 mutez and spend 5 mutez.
* `tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU"` should receive 5 mutez.
* Storage of `failOrStoreAndTransferAddress` should be updated to `str`.

Let us check that this all holds:

```haskell
  -- Check balances and storage of 'failOrStoreAndTransfer'
  let
    -- `string_caller.tz` transfers 300 mutez.
    -- 'fail_or_store_and_transfer.tz' transfers 5 mutez.
    -- Also 100 mutez are transferred from the "moneybag" address.
    expectedStringCallerBalance = initStringCallerBalance - 300 + 100
    expectedFailOrStoreBalance  = initFailOrStoreBalance  + 300 - 5
    expectedConstAddrBalance    = 5

  getStorage failOrStoreAndTransferHandle @@== str
  getBalance failOrStoreAndTransferHandle @@== expectedFailOrStoreBalance
  getBalance stringCallerHandle           @@== expectedStringCallerBalance
  getBalance [ta|tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU|]
    @@== expectedConstAddrBalance
```

Here we check that the storage of `failOrStoreAndTransfer` was updated
and balances of the addresses match the values we expect using `getStorage` and `getBalance` respectively.

Notice `ta` is a quasi-quote that lets us parse an address literal at compile-time.

We can also test for expected failures. Let's try to do the same transfer again.
It should fail because `failOrStoreAndTransfer` fails when its balance is greater than 1000:

```haskell
  -- Now let's transfer 100 mutez to stringCaller again.
  -- This time execution should fail, because failOrStoreAndTransfer should fail
  -- because its balance is greater than 1300.
  expectTransferFailure
    (addressIs failOrStoreAndTransferHandle)
    transferToStringCaller
```

In this case, we expect that `failOrStoreAndTransfer` will reach the `[FAILED]` state.

We're using `addressIs` failure predicate to assert it's specifically `failOrStoreAndTransfer` that fails and not any other contract in the call chain (e.g. `stringCaller`)

Now let's advance the current timestamp by 500 seconds.
After that `stringCaller` should fail because the current timestamp is greater than 500.

```haskell
  -- Now let's advance time by 500 seconds and watch stringCaller fail
  advanceTime $ sec 500
  expectTransferFailure
    (addressIs stringCallerHandle)
    transferToStringCaller
```

This is the end of this test, but in principle, we can continue performing operations and validating their effects.

As usual, you can run this test using `stack StringCallerSpec.hs`.

Note that the example-based test here can be technically run on a network. However, there it will fail, since we're hard-coding the timestamp of `500` into the contract, which corresponds to roughly 8 minutes past midnight Jan 01, 1970, GMT.

Another thing to note, which we've glossed over in the description above, is that the emulator starts with `NOW` returning the timestamp pretty close to the beginning of the Unix Epoch, i.e. sometime Jan 01 1970. This may be subject to change, and it's not a particularly useful default anyway. If your contracts depend on the current time, you may want to run those via `withInitialNow`, see the next section.

### Manipulating emulator state

It can be useful to run tests on the emulator, imitating different states
of a network. The contract can access a very limited subset of a complete network state, but nonetheless, this subset is non-empty.

We'll use the following contract. Let's call it `environment.tz`:

```
# This contract's behavior heavily depends on the environment in which it's executed.
# 1. It fails if its balance is greater than 1000.
# 2. It fails if NOW is less than 100500.
# 3. It fails if the address passed to it is a contract with parameter `address`.
# 4. It fails if the amount transferred to it is less than 15.
parameter address;
storage unit;
code {
       # Check balance and possibly fail
       PUSH mutez 1000;
       BALANCE;
       IFCMPGT { BALANCE; FAILWITH; } { };

       # Check NOW and possibly fail
       PUSH timestamp 100500;
       NOW;
       IFCMPLT { NOW; FAILWITH; } { };

       # Check address passed as parameter
       CAR;
       CONTRACT address;
       IF_SOME { ADDRESS; FAILWITH; } { };

       # Check amount transferred to this contract
       PUSH mutez 15;
       AMOUNT;
       IFCMPLT { AMOUNT; FAILWITH; } { };

       # Finish
       UNIT;
       NIL operation;
       PAIR; };
```

Its behavior depends on:
1. Its balance.
2. Current timestamp.
3. Whether the address passed to it as a parameter is originated with the parameter type `address`.
4. The amount transferred to it.

Now let's look at the test contained in `EnvironmentSpec.hs`.
Again let's omit the initial boilerplate.
`main` is defined again as `clevelandMain $ testProperty test hprop_environment`.
Then we define a data type we call `Fixture`:

```haskell
data Fixture = Fixture
  { fNow :: !Timestamp
  , fPassOriginatedAddress :: !Bool
  , fBalance :: !Mutez
  , fAmount :: !Mutez
  } deriving (Show)
```

This data type contains the data that our contract's execution depends on.
`fPassOriginatedAddress` determines whether we pass an originated address with parameter `address` to this contract as a parameter.
In our case, we will originate `environment.tz` and pass its address to itself if `fPassOriginatedAddress` is `True`.
This data will be generated by `hedgehog`.
We define an `genFixture` function which specifies how exactly this data will be generated:

```haskell
genFixture :: MonadGen m => m Fixture
genFixture = do
  fNow <- timestampFromSeconds <$> (Gen.integral $ Range.linear 100000 111111)
  fPassOriginatedAddress <- Gen.bool
  fBalance <- genMutez' $ Range.linear 1 1234
  fAmount <- genMutez' $ Range.linear 1 42
  return Fixture {..}
```

For most values, we use a Hedgehog's `linear` function which picks an arbitrary value in some range.
For boolean `fPassOriginatedAddress` we just use `bool` which will generate `True` or `False`.
In principle, we can define generators to be as complex as we want.
Then we define a function that represents the contract's behavior:

```haskell
shouldExpectFailed :: Fixture -> Bool
shouldExpectFailed fixture =
  or
    [ fBalance fixture + fAmount fixture > 1000
    , fNow fixture < timestampFromSeconds 100500
    , fPassOriginatedAddress fixture
    , fAmount fixture < 15
    ]
```

`shouldExpectFailed` returns whether the contract should fail.

We now define a Hedgehog `Property`
```haskell
hprop_environment :: Property
hprop_environment = property $ do
    fixture@Fixture{..} <- forAll genFixture
    testScenarioProps $ withInitialNow fNow $ myScenario fixture
```

Here we're using a new function, `withInitialNow`: it accepts 2 arguments, the
initial timestamp, and the scenario. Here we're passing the `fixture` to the scenario itself as well.

The logic is defined in `myScenario` again, so let's see it:

```haskell
myScenario :: Monad m => Fixture -> Scenario m
myScenario fixture@Fixture{..} = scenario $ do
  contract <- importContract @Address "contracts/environment.tz"

  -- Then let's originate the 'environment.tz' contract
  environmentAddress <- originate OriginateData
    { odContract = contract
    , odName = "environment"
    , odStorage = ()
    , odBalance = fBalance
    }

  -- And transfer mutez to it
  param <-
    if fPassOriginatedAddress
    then pure $ toAddress environmentAddress
    else newFreshAddress auto
  let
    txData = TransferData
      { tdTo = environmentAddress
      , tdParameter = param
      , tdAmount = fAmount
      , tdEntrypoint = DefEpName
      }
  if (shouldExpectFailed fixture)
  then do
    expectTransferFailure (addressIs environmentAddress)
      $ transfer txData
  else do
    transfer txData
```

Essentially we do the following:
1. Originate our contract with the initial balance.
2. Then we create transaction data based on the fixture.
If `fPassOriginatedAddress` is `True` we pass the address of `environment.tz`, otherwise, we generate some new `tz` address and pass that.
3. Then we construct a validator that expects failure if `shouldExpectFailed fixture` is `True` and expects success otherwise.

Now we can do `stack EnvironmentSpec.hs` to run this test.

## Managing Multiple Tests

When you have more tests for more contracts, it will be inconvenient to maintain them manually as a bunch of Haskell scripts.
To overcome this inconvenience, you can create a complete Stack project and put all your tests there.

The [folder with these examples](./examples) contains a Stack project with all example tests.
All we need to do is to create a simple `package.yaml` file (see [`package.yaml`](./examples/package.yaml)), create `Main.hs` with the content

```haskell
import Test.Cleveland.Tasty

import Tree (tests)

main :: IO ()
main = tests >>= clevelandMain
```

(note you can also use `clevelandMainWithIngredients` to customize how tests are run), add `Tree.hs` with the content

```haskell
{-# OPTIONS_GHC -F -pgmF tasty-discover -optF --tree-display -optF --generated-module -optF Tree #-}
```

`tasty-discover` will then automatically collect all exports starting with `test_`, `hprop_`, etc (see [tasty-discover's documentation](https://github.com/haskell-works/tasty-discover/#write-tests) for a complete list), and construct a `TestTree` from those.

Now we can run all tests using `stack test`.

## Summary

In this document, we have demonstrated how one can write tests for their Michelson contracts in Haskell.

We started with a simple example-based test, then demonstrated a slightly more complex property-based test, and finally two more complex tests.

* Both property-based and example-based tests use monadic scenarios, generally of the type `ClevelandT m ()`. These scenarios can run on both a real network and the emulator.
* We use `scenario` to wrap a `ClevelandT m ()` in a `Scenario`.
* We construct Tasty `TestTree`s from scenarios by using `testScenario`, for tests to be run on both a real network and the emulator, which takes a `Scenario` as an argument.
* In test-based properties we use Hedgehog generators to construct input data.
* We then use `testScenarioProps` to run a `Scenario` with the generated input data.
* Inside scenarios, we can import contracts with `importContract`, originate contracts with `originateSimple` or `originate`, make transfers with `transferMoney`, `call` and `transfer`, etc.
* We can also test for equality using `@==`, `@/=`, `@@==`, `@@/=`, or for that matter, check for arbitrary predicates with `assert`, or for comparison predicates using `checkCompares`.

An interested reader can find more examples in our test suite that we use to test Cleveland itself (located in [`TestSuite.Cleveland.*`](https://gitlab.com/morley-framework/morley/-/tree/master/code/cleveland/test/TestSuite/Cleveland)),
or indeed the Morley interpreter (located in [`Test.Interpreter.*`](https://gitlab.com/morley-framework/morley/-/tree/master/code/cleveland/morley-test/Test/Interpreter)).
