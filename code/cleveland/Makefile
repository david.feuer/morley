# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

.PHONY: cleveland \
				test \
				morley-test morley-test-dumb-term morley-test-hide-successes \
				lorentz-test lorentz-test-dumb-term lorentz-test-hide-successes \
				morley-bench \
				haddock haddock-no-deps clean all

# Build target from the common utility Makefile
MAKEU = $(MAKE) -f ../make/Makefile

MAKE_PACKAGE = $(MAKEU) PACKAGE=cleveland

cleveland:
	$(MAKE_PACKAGE) dev

test:
	$(MAKE_PACKAGE) TEST_PACKAGE=test:cleveland-test test
test-dumb-term:
	$(MAKE_PACKAGE) TEST_PACKAGE=test:cleveland-test test-dumb-term
test-hide-successes:
	$(MAKE_PACKAGE) TEST_PACKAGE=test:cleveland-test test-hide-successes

morley-test:
	$(MAKE_PACKAGE) TEST_PACKAGE=test:morley-test test
morley-test-dumb-term:
	$(MAKE_PACKAGE) TEST_PACKAGE=test:morley-test test-dumb-term
morley-test-hide-successes:
	$(MAKE_PACKAGE) TEST_PACKAGE=test:morley-test test-hide-successes

lorentz-test:
	$(MAKE_PACKAGE) TEST_PACKAGE=test:lorentz-test test
lorentz-test-dumb-term:
	$(MAKE_PACKAGE) TEST_PACKAGE=test:lorentz-test test-dumb-term
lorentz-test-hide-successes:
	$(MAKE_PACKAGE) TEST_PACKAGE=test:lorentz-test test-hide-successes

morley-bench:
	$(MAKE_PACKAGE) bench

haddock:
	$(MAKE_PACKAGE) haddock
haddock-no-deps:
	$(MAKE_PACKAGE) haddock-no-deps
clean:
	$(MAKE_PACKAGE) clean

all:
	$(MAKE) cleveland
