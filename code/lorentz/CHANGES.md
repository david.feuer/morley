<!-- Unreleased: append new entries here -->

* [!841](https://gitlab.com/morley-framework/morley/-/merge_requests/841)
  Make entrypoints derivation work with void-like entrypoint arguments.

0.13.3
======
* [!1100](https://gitlab.com/morley-framework/morley/-/merge_requests/1100)
  Support `map` instruction over `Maybe` (a.k.a. `option`)
  + Add `instance MapOpHs (Maybe e)`
* [!1084](https://gitlab.com/morley-framework/morley/-/merge_requests/1084)
  Switch to the new mutez subtraction operation
* [!1101](https://gitlab.com/morley-framework/morley/-/merge_requests/1101)
  Export `sopSetField` and `sopGetField` from `Lorentz.StoreClass`
* [!1089](https://gitlab.com/morley-framework/morley/-/merge_requests/1089)
  Enhance and rename mutez quasiquote
  + Export `tz` quasiquoter from `Lorentz.Value` and `Lorentz`.
* [!1071](https://gitlab.com/morley-framework/morley/-/merge_requests/1071)
  Add `HasRPCRepr` typeclass
  + Move `AsRPC` type instances from the `Lorentz.AsRPC` module
    to the modules of their respective types.
* [!901](https://gitlab.com/morley-framework/morley/-/merge_requests/901)
  Weaken constraints of `getField`
  + `getField` and similar methods now require `Dupable` on the copied field and
    `HasDupableGetters` (defaults to `Dupable`) on the datatype.
* [!1060](https://gitlab.com/morley-framework/morley/-/merge_requests/1060)
  Add `Lorentz.AsRPC` module containing instances for `Morley.AsRPC` type family
* [!978](https://gitlab.com/morley-framework/morley/-/merge_requests/978)
  Make it difficult to misuse 'Show'
  + Use `Buildable` and `pretty` preferrentially.
  + Avoid using `(+||)` and `(||+)`.
  + Use `Debug.show` when defining `Show` instances.
* [!1062](https://gitlab.com/morley-framework/morley/-/merge_requests/1062)
  Move `senderIsNotAdmin` Lorenz error to `morley-ledgers` repo
* [!997](https://gitlab.com/morley-framework/morley/-/merge_requests/997)
  Check necessity in initial `cast` automatically in Lorentz contracts
* [!1043](https://gitlab.com/morley-framework/morley/-/merge_requests/1043)
  Extract actual + expected pattern into a datatype and show diff in type mismatch
* [!1037](https://gitlab.com/morley-framework/morley/-/merge_requests/1037)
  Add synonyms for `SomeConstrainedValue`
  + Reimplement `SomeConstant`, `SomeValue`, `SomeStorage` via `SomeConstrainedValue`.
  + Add `SomePackedVal = SomeConstrainedValue PackedValScope`.
* [!1049](https://gitlab.com/morley-framework/morley/-/merge_requests/1049)
  Delete unnecessary `HasStorage` constraints
  + `Lorentz.Contract` no longer requires `ParameterDeclaresEntrypoints` or `HasAnnotation st`.
  + Added `NiceStorageFull` constraint for when `HasAnnotation st` is actually needed.

0.13.2
======
* [!1041](https://gitlab.com/morley-framework/morley/-/merge_requests/1041)
  `createContract` now returns `TAddress`, not `Address`

0.13.1
======
* [!1016](https://gitlab.com/morley-framework/morley/-/merge_requests/1016)
  + Add support for on-chain views.
  + `Contract`, `TAddress` and similar types has gained a new type argument
    designating the views set exposed by the contract. By default, set it to `()`.
  + The old A1 (aka TZIP-4) views - `View` type and related methods - were renamed
    via adding a `_` suffix: `View_`, `mkView_`, `wrapView_`, e.t.c.
* [!1010](https://gitlab.com/morley-framework/morley/-/merge_requests/1010)
  Add timelock puzzle support.
  + Add Lorentz instruction `openChest`.
  + Add `OpenChest` type, isomorphic to `Either ByteString Bool`
  + Add Lorentz types `ChestT`, `OpenChestT`, abstracting working with any
  type representable as `ByteString` (defined in `Lorentz.Bytes`).
  + Add `openChestT` Lorentz instruction for working with `ChestT` (defined in
  `Lorentz.Bytes`)

0.13.0
======
* [!915](https://gitlab.com/morley-framework/morley/-/merge_requests/915)
  + Add Fixedpoint arithmetics to lorentz
  + Add additional `NFixed` datatype for natural fixedpoint values
  + Add typeclasses for rounding and casting fixedpoint values
  + Change behavior of lorentz arithmetic operations -
    now type of the result is specified by user.
  + Add `ArithOpHs` instance for `EDiv` instruction
* [!989](https://gitlab.com/morley-framework/morley/-/merge_requests/989)
  Switch from interpolate to home-grown string interpolation module
  + Note this changes the syntax of slices in `md` quasiquoter. Before
  we could use expressions `#{expression}`, now we can only use variable
  names `#{name}`.
* [!909](https://gitlab.com/morley-framework/morley/-/merge_requests/909)
  + Update all instances of `Named` functions with patterns from `Morley.Util.Named`
* [!1000](https://gitlab.com/morley-framework/morley/-/merge_requests/1000)
  Modify the `cloneX` macro to only use `dup`s.
* [!937](https://gitlab.com/morley-framework/morley/-/merge_requests/937)
  Attach source code location to interpreter error messages
  + Changed morley interpreter error type from `MichelsonFailed` to `MichelsonFailureWithStack`
* [!945](https://gitlab.com/morley-framework/morley/-/merge_requests/945)
  + Bump Stackage LTS version from 17.9 to 18.10.
* [!835](https://gitlab.com/morley-framework/morley/-/merge_requests/835)
  + Added `Lorentz.Tickets` module with tickets arithmetics and common helpers.
  + Added `Impossible` error.
  + Added `transferTokensE` and `pairE`.
  + Moved `NonZero` to `Lorentz.Macro`.
  + Updated `Wrappable`:
    + Make separate `Wrappable` and `Unwrappable` typeclasses.
    + Old `Unwrappable` type family renamed to `Unwrappabled`.
    + Weaked permissions to perform some actions for some types.
      May now require `unsafeCoerceWrap` instead of `coerceWrap`.
* [!922](https://gitlab.com/morley-framework/morley/-/merge_requests/922)
  Replace lSignEd22519 with generic lSign
* [!881](https://gitlab.com/morley-framework/morley/-/merge_requests/881)
  + Rename `PrintedValScope` to `UntypedValScope` with related types and functions.

0.12.1
======
* [!879](https://gitlab.com/morley-framework/morley/-/merge_requests/879)
  + Add `Buildable` and `RenderDoc` instances for `(:->)` operator
  + Instances are based on transforming operator to `Instr` type and using its instances

0.12.0
======
* [!854](https://gitlab.com/morley-framework/morley/-/merge_requests/854)
  + `StoreHasField` instance definition is no more necessary for simple ADT storage types.
* [!846](https://gitlab.com/morley-framework/morley/-/merge_requests/846)
  + Reorganized `Contract` type and related stuff.
  + Added methods for reading `Contract` from file.

  Migration guide (sufficient unless you worked on framework internals):
  + In case you needed to use non-default contract compilation options, use `mkContractWith` now.

  For more details, see documentation of `Lorentz.Run`.
* [!832](https://gitlab.com/morley-framework/morley/-/merge_requests/832)
  + Add tickets feature.
  + Add dupable restriction to `dup`-like instructions and some high-level helpers.
    See `dup`'s documentation for notes on how to live in this brand new world.
  + Make `parseNoEnv`, `readValue` and other similar functions accept `MichelsonSource` instead of `FilePath`.
* [!838](https://gitlab.com/morley-framework/morley/-/merge_requests/838)
  + All unsafe functions and data constructors now contain "unsafe" word
    at prefix position. E.g `UnsafeMText`, `unsafeMkAnnotation`.
* [!794](https://gitlab.com/morley-framework/morley/-/merge_requests/794)
  [!833](https://gitlab.com/morley-framework/morley/-/merge_requests/833)
  + Added `Lorentz.Expr` module with primitives for convenient expressions evaluation.
    * Added `listE` to construct an expression list from a list of expressions.
  + `if ... then ... else` now is polymorphic in the first argument.
    * Boolean expressions now can appear as condition for `if`.

0.11.0
======
* [!814](https://gitlab.com/morley-framework/morley/-/merge_requests/814)
  + Added the usage of `Prelude.Type` instead of `Data.Kind.Type`,
    as a result `Kind.Type` was replaced with just `Type`.
* [!745](https://gitlab.com/morley-framework/morley/-/merge_requests/745)
  + Generalize `st*` methods for working with storage.
    Now it is possible to access nested fields and submaps like `stToField (#a :-| #b)`.
    This should not require any changes unless you keep you own generic helpers over those methods.
  + Existing `StoreHasSubmap (Big?)Map anyName k v` instance is replaced with two:
    * `StoreHasSubmap (Big?)Map Self k v`
    * `StoreHasSubmap store name k v` that delegates to `StoreHasField`.

    Migration guide:
    * If you have `StoreHasSubmap` instances for specific storage and name implemented via
      `storeSubmapOpsDeeper`, remove them and make sure you have a similar `StoreHasField`
      instance implemented via `storeFieldOpsADT`.
    * If you have storage which is directly a map, use `this` instead of label to access the map.
    * If you have an instance for a complex storage which is not represented as simple ADT,
      turn this instance into overlappable one. Make sure that `name` type parameter is of
      concrete type to avoid "overlapping instances" error.
    * The pattern when a datatype transparently provides access to its inner fields via an
      overlapping instance - may not work smoothly and produce "overlapping instances" error.
      To mitigate this do one of the following:
      * Specify the kind of `name` type parameter in your overlapping instance to be concrete
        (i.e. `Symbol`).
      * Use the new functionality to access the nested field by a fully qualified name.

* [!807](https://gitlab.com/morley-framework/morley/-/merge_requests/807)
  + Add some instances to `BigMap` and `TAddress` types.
* [!787](https://gitlab.com/morley-framework/morley/-/merge_requests/787)
  + Added new Edo macros: `CAR k` and `CDR k` as `carN` and `cdrN`.
* [!798](https://gitlab.com/morley-framework/morley/-/merge_requests/798)
  + Added back the `UnaryArithOpHs Abs Integer` instance that was removed by accident.
* [!802](https://gitlab.com/morley-framework/morley/-/merge_requests/802)
  + Add `NiceParameterFull` and `NiceStorage` constraints to Lorentz `Contract` constructor.
  + Moved `coDisableInitialCast` to `CompilationOptions` datatype.


0.10.0
======
* [!774](https://gitlab.com/morley-framework/morley/-/merge_requests/774)
  + Added support for `BLS12-381` crypto primitives (types, instructions, arithmetics).
  + Now it is possible to use `int` instruction for newtypes over `Natural`.
* [!786](https://gitlab.com/morley-framework/morley/-/merge_requests/786)
* [!791](https://gitlab.com/morley-framework/morley/-/merge_requests/791)
  + Add new Edo instructions: `pairGet` and `pairUpdate`.
* [!755](https://gitlab.com/morley-framework/morley/-/merge_requests/755)
  Restricted `FAILWITH` only to packable values, except `CONTRACT`s
* [!759](https://gitlab.com/morley-framework/morley/-/merge_requests/759)
  + Add `UnitErrorArg` and `NoErrorArg` to be used with `ErrorArg` instance.
    + `UnitErrorArg` is preferred to `()`
    + `NoErrorArg` allows failing with just a string instead of a `pair string unit`.
* [!764](https://gitlab.com/morley-framework/morley/-/merge_requests/764)
  + Added `Never` type, deprecated `Empty` type.

0.9.1
=====
* [!781](https://gitlab.com/morley-framework/morley/-/merge_requests/781)
  + Replaced mixins and dependency on `base` with `base-noprelude`.
* [!751](https://gitlab.com/morley-framework/morley/-/merge_requests/751)
  + Add new Edo instruction: `LEVEL`.
* [!741](https://gitlab.com/morley-framework/morley/-/merge_requests/741)
  + Add new Edo instructions: `SHA3` and `KECCAK`.

0.9.0
=====
* [!731](https://gitlab.com/morley-framework/morley/-/merge_requests/731)
  Added `GenericStrategy`-ies for compatibility with LIGO.
* [!712](https://gitlab.com/morley-framework/morley/-/merge_requests/712)
  Add lenses for Lorentz contract config.
* [!684](https://gitlab.com/morley-framework/morley/-/merge_requests/684)
  + Simplify working with autodoc in contracts (breaking change).
  + `finalizeParamCallingDoc` is now applied automatically when lorentz' `Contract`
    is used, no need to call it manually.

0.8.0
=====
* [!671](https://gitlab.com/morley-framework/morley/-/merge_requests/671)
  + Add `TypeHasDoc` instances for `TAddress` and `FutureContract`.
  + Add a generic `instance DocItem (DEntrypoint ep)`.
    In case of getting overlapping instances, you have to implement `EntrypointKindHasDoc` instead.
  + Some error messages in doc tests are improved.
  + Some bugfixes for anchors.
  + Cases with contract logic residing outside of entrypoints are now better handled.
* [!665](https://gitlab.com/morley-framework/morley/-/merge_requests/665)
  Improve `StorageContains` to work for polymorphic case (when field is marked with `Identity`).
* [!679](https://gitlab.com/morley-framework/morley/-/merge_requests/679)
  Add `dupLNamed`.
* [!672](https://gitlab.com/morley-framework/morley/-/merge_requests/672)
  + Add `Packed`, `TSignature` and `Hash` types.
  + `pack` and `unpack` now work with `Packed` instead of `ByteString` (for the sake of better type-safety).
    Either use `Packed` type, or `packRaw`/`unpackRaw` instructions.
  + `checkSignature` now works with `Packed` and `TSignature`.
  + `blake2b` and other hashing instructions now work with `Hash`.
  + If necessary, coersions can be used to switch between the mentioned types and raw `ByteString`.
* [!667](https://gitlab.com/morley-framework/morley/-/merge_requests/667)
  + Modify representation of extended lambdas
    (arbitrary code used as lambdas is now packed into right-hand combs, not left-hand ones).
  + Add `applicate` macro.
* [!630](https://gitlab.com/morley-framework/morley/-/merge_requests/630)
  Use `:` in all library instructions instead of `&`.
* [!652](https://gitlab.com/morley-framework/morley/-/merge_requests/652)
  Update the `ContractRegistry` to be able to not require contract name for
  all its commands when there is only one contract.
* [!640](https://gitlab.com/morley-framework/morley/-/merge_requests/640)
  Change `StoreSubmapOps` to contain no optional methods.

0.7.1
=====
* [!653](https://gitlab.com/morley-framework/morley/-/merge_requests/653)
  Added primitives for evaluating `OpSize` in Lorentz.
* [!651](https://gitlab.com/morley-framework/morley/-/merge_requests/651)
  Add `dupL`.

0.7.0
=====
* [!629](https://gitlab.com/morley-framework/morley/-/merge_requests/629)
  All `UStore` modules has been moved to the [morley-upgradeable](https://gitlab.com/morley-framework/morley-upgradeable/) repository.
  Now you have to include that repository to your build and import `Lorentz.UStore`.
* [!610](https://gitlab.com/morley-framework/morley/-/merge_requests/610)
  Remove `Lorentz.TestScenario`.
* [!585](https://gitlab.com/morley-framework/morley/-/merge_requests/585)
  Add `HasAnnotation` instance for `ChainId`.

0.6.2
=====
* [!589](https://gitlab.com/morley-framework/morley/-/merge_requests/589)
  Add `annOptions` method in `HasAnnotation` which can be used to customize
  the generated annotations of a type.
  + Add helper functions to use with `annOptions`.
* [!591](https://gitlab.com/morley-framework/morley/-/merge_requests/591)
  Add `dupTop2`.

0.6.1
=====
* [!533](https://gitlab.com/morley-framework/morley/-/merge_requests/533)
  Add `entrypointDoc`, `typeDoc`, and `errorDoc` quasiquotes which help
  generating typeclass instances.

0.6.0
=====
* [!558](https://gitlab.com/morley-framework/morley/-/merge_requests/558)
  Added a new `wrapOne` to wrap a value in a constructor with a single field,
  because it has the advantage of having an input stack that does not depend on
  a type family.
* [!528](https://gitlab.com/morley-framework/morley/-/merge_requests/528)
  The generated documentation now contains a sample value of each entrypoint.
  + Allow modification of sample value via `mkDEntrypointExample`.
* [!493](https://gitlab.com/morley-framework/morley/-/merge_requests/493)
  Add support for root entrypoint.
  + A contract can now specify whether or not the root annotation should be generated
    and which name it should use.
  + `self` and `contract` instructions are able to call the root entrypoint.

0.5.0
=====
* [!530](https://gitlab.com/morley-framework/morley/-/merge_requests/530)
  Field annotation are now generated for sum-type in `parameter` and `storage`.
  + It also ensures that it will not generate field annotations that would introduce
  extra entrypoints.
* [!371](https://gitlab.com/morley-framework/morley/-/merge_requests/371)
  Make `view_` argument accept 2 arguments rather than a pair.
* [!486](https://gitlab.com/morley-framework/morley/-/merge_requests/486)
  Add `customGeneric` to derive `Generic` instances with a custom tree structure
  and, consequently, `IsoValue` instances with such a structure.
* [!483](https://gitlab.com/morley-framework/morley/-/merge_requests/483)
  Add a way to generate field annotations for entrypoint parameter.
* [!470](https://gitlab.com/morley-framework/morley/-/merge_requests/470)
  Move `Lorentz.Test` and  Hedgehog generators to a new package: `cleveland`.
* [!357](https://gitlab.com/morley-framework/morley/-/merge_requests/357)
  Remove deprecated `Lorentz.Store` module and deprecated custom error functionality.

0.4.0
=====
* [!449](https://gitlab.com/morley-framework/morley/-/merge_requests/449)
  + Improve `Lorentz.Test.Integrational` interface.
  + Remove `IntegrationalValidator`, `SuccessValidator`, `validate`,
    `composeValidators`, `composeValidatorsList`, `expectAnySuccess`.
  + Add `unexpectedInterpreterError`, `attempt`, `catchExpectedError`.
  + Change type of `expect*`/`tExpect*`/`lExpect*` functions.
    They now return `IntegrationalScenario` instead of `SuccessValidator` / `Bool`.
  + Rename `ValidationError` to `TestError`
  + Replace `TestError`'s `UnexpectedExecutorError` constructor with
    `InterpreterError` and `UnexpectedInterpreterError`.
* [!432](https://gitlab.com/morley-framework/morley/-/merge_requests/432)
  Add `whenNone` to Lorentz.
* [!398](https://gitlab.com/morley-framework/morley/-/merge_requests/398)
  Add `StoreHasEntrypoint` class to conveniently use entrypoints as lambdas
  stored into `big_map`(s) to make use of lazy evaluation.
* [!409](https://gitlab.com/morley-framework/morley/-/merge_requests/409)
  + Add hedgehog support
* [!303](https://gitlab.com/morley-framework/morley/merge_requests/303)
  + Functions for working with `UStore` from Haskell world (e.g. `fillUStore`) now have a different constraint.
  + `UStore` templates are now documented and thus may require instance of the respective typeclass.

0.3.0
=====
* [!417](https://gitlab.com/morley-framework/morley/-/merge_requests/417)
Change the license to MIT.
* [!360](https://gitlab.com/morley-framework/morley/-/merge_requests/360)
  + Add `compileLorentzWithOptions` function that accepts optional Michelson
  optimizer configuration and string transformer function. Changed `compileLorentz` to call the
  optimizer with the default configuration.
  + Add `Lorentz.Contract` data type as a wrapper for `Lorentz.ContractCode` and
    `Lorentz.CompilationOptions`. Require that all contracts are wrapped in this data type.
* [!354](https://gitlab.com/morley-framework/morley/-/merge_requests/354)
  + Add `constructStack` function that constructs a datatype from fields on the stack.
  + Add `deconstruct` function that deconstructs a datatype to its fields on the stack.
* [!358](https://gitlab.com/morley-framework/morley/-/merge_requests/358)
  Removed deprecated `STEPS_TO_QUOTA` instruction.
* [!337](https://gitlab.com/morley-framework/morley/-/merge_requests/337)
  Added `comment`, `commentAroundFun` and `commentAroundStmt` to generate comments in Michelson code.

0.2.0
=====
* [!346](https://gitlab.com/morley-framework/morley/-/merge_requests/346)
  Added `docStorage` and `contractGeneralDefault`.
* [!306](https://gitlab.com/morley-framework/morley/-/merge_requests/306)
  Deprecated `mapMigrationCode` in favor of `MapLorentzInstr`.
* [!326](https://gitlab.com/morley-framework/morley/-/merge_requests/326)
Updated contract registry (`Lorentz.ContractRegistry`):
  + Now it can print initial storage apart from contract and documentation.
  + Some extra fields were added to `ContractInfo`.
  + Logic is captured in the `runContractRegistry` function.
  + If you don't specify output file, we will use a file with name constructed from contract name. Pass `-` if you want `stdout`.
* [!245](https://gitlab.com/morley-framework/morley/-/merge_requests/245) Added `HasTypeAnn` instance for `FutureContract arg`.
* [!294](https://gitlab.com/morley-framework/morley/-/merge_requests/294)
  + Added `Paths_*` modules to `autogen-modules` in cabal files.  Removed `-O0`
  + from default GHC options. Please set `ghc-options` in your `stack.yaml` or
  `cabal.project.local`.
* [!271](https://gitlab.com/morley-framework/morley/merge_requests/271) Renamed
  'Contract' to 'ContractCode', and appended "Code" to the names of two functions:
  'convertContract' and 'printTypedContract'
* [!267](https://gitlab.com/morley-framework/morley/-/merge_requests/267)
  + Retain type annotations in entrypoints derivation.
  + Remove overlappable `HasTypeAnn` instance defined for nearly each type.
    Add `default` `getTypeAnn` method definition instead and manually define `HasTypeAnn` instance for each type instead (trivially).
    When you create a new data type with `IsoValue` instance, you usually have to derive `HasTypeAnn` as well.

0.1.0
=====

Initial release extracted from the [morley](https://hackage.haskell.org/package/morley) package.
