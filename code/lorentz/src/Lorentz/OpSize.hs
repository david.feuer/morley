-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Operation size evaluation.
module Lorentz.OpSize
  ( OpSize (..)
  , T.opSizeHardLimit
  , T.smallTransferOpSize

  , contractOpSize
  , valueOpSize
  ) where

import Data.Constraint ((\\))

import Lorentz.Constraints
import Lorentz.Run
import Lorentz.Value
import Morley.Michelson.Typed (OpSize)
import Morley.Michelson.Typed qualified as T

-- | Estimate code operation size.
contractOpSize :: Contract cp st vd -> OpSize
contractOpSize =
  T.contractOpSize . toMichelsonContract

{- We do not provide a method for plain lorentz code because it can be
   compiled differently (e.g. with optimizations or not).
-}

-- | Estimate value operation size.
valueOpSize :: forall a. (NiceUntypedValue a) => a -> OpSize
valueOpSize =
  T.valueOpSize . toVal
  \\ niceUntypedValueEvi @a
