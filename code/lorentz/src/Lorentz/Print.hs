-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-redundant-constraints #-}

-- | Printing lorentz contracts.
module Lorentz.Print
  ( printLorentzValue
  , printLorentzContract
  ) where

import Lorentz.Constraints
import Lorentz.Run
import Morley.Michelson.Printer (printTypedContract, printTypedValue)
import Morley.Michelson.Typed (toVal)

-- | Pretty-print a Haskell value as Michelson one.
printLorentzValue
  :: forall v.
    (NiceUntypedValue v)
  => Bool -> v -> LText
printLorentzValue forceSingleLine =
  withDict (niceUntypedValueEvi @v) $
    printTypedValue forceSingleLine . toVal

-- | Pretty-print a Lorentz contract into Michelson code.
printLorentzContract
  :: Bool -> Contract cp st vd -> LText
printLorentzContract forceSingleLine =
  printTypedContract forceSingleLine . toMichelsonContract
