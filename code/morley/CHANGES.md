<!-- Unreleased: append new entries here -->

* [!1115](https://gitlab.com/morley-framework/morley/-/merge_requests/1115)
  Add better error messages when typechecking sapling values.

1.16.3
======
* [!1113](https://gitlab.com/morley-framework/morley/-/merge_requests/1113)
  Remove uses of `Parser LetEnv` due to redundant constraint.
* [!1092](https://gitlab.com/morley-framework/morley/-/merge_requests/1092)
  Add quasiquoter for `Notes t`
* [!1094](https://gitlab.com/morley-framework/morley/-/merge_requests/1094)
  Deprecate morley language extensions
  + Morley language extensions now require `--deprecated-morley-extensions` flag to parse.
  + AST constructors corresponding to the Morley language extensions are deprecated
  + Functions for parsing Michelson with Morley extensions are added and marked as deprecated:  `parseContractExt`, `parseExpandContractExt`, `readAndParseContractExt`, `prepareContractExt`.
* [!1100](https://gitlab.com/morley-framework/morley/-/merge_requests/1100)
  Support `MAP` instruction over option type
  + Add `instance SingI1 'TOption`
  + Add `instance MapOp ('TOption e)`
* [!1084](https://gitlab.com/morley-framework/morley/-/merge_requests/1084)
  Switch to the new mutez subtraction operation
* [!1107](https://gitlab.com/morley-framework/morley/-/merge_requests/1107)
  Fix redundant `SDecide Nat` constraint warning
* [!1099](https://gitlab.com/morley-framework/morley/-/merge_requests/1099)
  Update utils for `Instr` type further.
  + `CtorEffectsApp` is now applied to the leaves in the `Instr` tree.
* [!1083](https://gitlab.com/morley-framework/morley/-/merge_requests/1083)
  Add typechecking and better errors for sapling
* [!965](https://gitlab.com/morley-framework/morley/-/merge_requests/965)
  Add fixed-size lists to Morley
  + Add `Morley.Util.SizedList` introducing Peano-based fixed-size homogenous lists.
  + Use `SizedList` for `genesisAddressN`.
  + Use `SizedList` to simplify `Morley.Util.Interpolate`.
  + Add instance `ToBigMap (SizedList' n (k, v))`
  + Add `Morley.Michelson.Parser.Helpers.count`, a fixed-size list version of `Text.Megaparsec.count`.
* [!1042](https://gitlab.com/morley-framework/morley/-/merge_requests/1042)
  Amend Morley.Util.Named
  + Re-export `arg` and `argF`
  + Deprecate `N`, `M`, `SomeArg`, `NoArg` patterns due to potential for abuse (use `arg` or `argF` with view patterns instead)
  + Do not re-export `Name` data constructor (use `fromLabel` with a type annotation/application instead)
* [!1089](https://gitlab.com/morley-framework/morley/-/merge_requests/1089)
  Enhance and rename mutez quasiquote
  + Remove `mtzQ` quasiquoter
  + Add `tz` quasiquoter; units are XTZ by default, but can be changed with a suffix; use `[tz|123u|]`, `[tz|123μ]` or `[tz|123 micro|]` for mutez.
* [!1095](https://gitlab.com/morley-framework/morley/-/merge_requests/1095)
  Put run, transfer and originate commands under emulate parent command
* [!1072](https://gitlab.com/morley-framework/morley/-/merge_requests/1072)
  Add `runCode` to Cleveland
  + `Morley.Michelson.Runtime`:
    - Don't assign IDs to bigmaps that already have one.
    - Allow overriding a contract's balance during a `transfer`.
  + Added `MaybeRPC` and `replaceBigMapIds`.
* [!1075](https://gitlab.com/morley-framework/morley/-/merge_requests/1075)
  Handle oddly-sized integral types with the `int-cast` facilities
  + Redefine `Mutez`, `TLTime` as newtype wrappers for `Word63`, `Word62` types respectively.
  + Implement `toTLTime` conversion function using `fromIntegralNoOverflow`.
  + Delegate `mkMutez`, `toMutez`, `mkTLTime` conversion functions to convenient and safe `fromIntegral` and `fromIntegralNoOverflow`.
  + Implement safe `mtzQ` quasiquoter for creating `Mutez` values.
* [!1071](https://gitlab.com/morley-framework/morley/-/merge_requests/1071)
  Add `HasRPCRepr` typeclass
* [!1068](https://gitlab.com/morley-framework/morley/-/merge_requests/1068)
  Update some utils for `Instr` type.
  * `dfsInstr` itself got deprecated in favor of other variations of this function.
  * `CtorEffectsApp` setting in `dfsInstr`-like functions now works differently.
    Instead of specifying complex combining function as the second `ceaApplyEffects` field,
    now you should specify how to transform the monadic action (and the field is now called `ceaPostStep`).
    Please create an issue if this change does not work for you.
  * Added `isMichelsonInstr`.
  * Now exporing `analyzeInstrFailure`.
* [!1076](https://gitlab.com/morley-framework/morley/-/merge_requests/1076)
  Fix escaping in haddock for Morley.Util.Interpolate
* [!1025](https://gitlab.com/morley-framework/morley/-/merge_requests/1025)
  Reduce the number of unsafe functions at the call site
  + Remove the following `unsafeFName` constructors/converters that are used as counter-parts of safe functions and replaced with `unsafe . fName`:
    - `unsafeMkMText`
    - `unsafeMkPos`
    - `unsafeParseEpAddress`
    - `unsafeMkMutez`
    - `unsafeMkAnnotation`
    - `unsafeMkViewName`
    - `unsafeFromMichelsonBytes`
    - `unsafeParseAddressRaw`
    - `unsafeBuildEpName`
    - `unsafeMkSomeParamType`
    - `unsafeParseKeyHash`
    - `unsafeParseEpAddressRaw`
    - `unsafeMkTLTime`
    - `unsafeParseAddress`
    - `unsafeParseContractHash`
    - `unsafeMkChainId`
  + Refactor `mkPos` and `mkChainId` to return `Either` instead of `Maybe`.
* [!1063](https://gitlab.com/morley-framework/morley/-/merge_requests/1063)
  Remove redundant `SingI` constraints
* [!1060](https://gitlab.com/morley-framework/morley/-/merge_requests/1060)
  Add `Morley.AsRPC` module for mapping types to their Tezos RPC representations
* [!978](https://gitlab.com/morley-framework/morley/-/merge_requests/978)
  Make it difficult to misuse 'Show'
  + Use `Buildable` and `pretty` preferrentially.
  + Add `Buildable` instances to that effect for `ArithError`, `StackSize`, `CtorEffectsApp`, `ContractBlock`,
  + Avoid using `(+||)` and `(||+)`.
  + Use `Debug.show` where we can't use `pretty` for some reason.
* [!1057](https://gitlab.com/morley-framework/morley/-/merge_requests/1057)
  Fix `Show`, `RenderDoc`, `Builder` instances of `Annotation t`, `AnnotationSet`, `Notes t`, `InstrAbstract op`
* [!997](https://gitlab.com/morley-framework/morley/-/merge_requests/997)
  Check necessity in initial `cast` automatically in Lorentz contracts
* [!1043](https://gitlab.com/morley-framework/morley/-/merge_requests/1043)
  Extract actual + expected pattern into a datatype and show diff in type mismatch
* [!1055](https://gitlab.com/morley-framework/morley/-/merge_requests/1055)
  Add more optimization rules
  + Rules added: `DUP 1 -> DUP`, `IF x x -> x`, `DIP n { } -> Nop`, `DIG n ; DUG n -> Nop`.
  + `TestEquality` instance added for `Morley.Util.Peano.SingNat`.
  + `Morley.Util.PeanoNatural` exports two new utility functions, `eqPeanoNat` and `singPeanoNat`.
  + `Morley.Util.Type` exports a new utility function, `knownListFromSingI`
* [!1013](https://gitlab.com/morley-framework/morley/-/merge_requests/1013)
  Add `WellTyped` to complex scope constraints
  + Make `WellTyped` constraint a part of complex constraints, namely `ParameterScope`, `StorageScope`, `ConstantScope`, `PackedValScope`, `UnpackedValScope`.
* [!987](https://gitlab.com/morley-framework/morley/-/merge_requests/987)
  Make parser's separators between values and instructions mandatory
  + Add `spaces` parser which doesn't accept zero white spaces
  + Change `ops'`, that parses sequence of operations, to accept absence of `;` separator after code in brackets `a; {b; c; ...} d`
  + Add backtracking with `try` to `symbol1`
* [!1037](https://gitlab.com/morley-framework/morley/-/merge_requests/1037)
  Add synonyms for `SomeConstrainedValue`
  + Reimplement `SomeConstant`, `SomeValue`, `SomeStorage` via `SomeConstrainedValue`.
* [!1035](https://gitlab.com/morley-framework/morley/-/merge_requests/1035)
  Better type errors on invalid int casts

1.16.2
======
* [!1045](https://gitlab.com/morley-framework/morley/-/merge_requests/1045)
  + Add optics:
    - `Ixed` and `At` instances for `Value t` and `BigMap k v`.
    - `isoValue`.
    - prisms for `Expression`.
    - lenses for `MichelinePrimAp`.
  + Add `SomeAnnotatedValue` and `AnnotatedValue`.
  + Add `Lift` instance for `Expression`.
* [!1040](https://gitlab.com/morley-framework/morley/-/merge_requests/1040)
  Add missing `FromExpression` conversions
  + `Chest` and `ChestKey` cases were missing in `FromExpression` conversions.

1.16.1
======
* [!1016](https://gitlab.com/morley-framework/morley/-/merge_requests/1016)
  + Add support for on-chain views.
  + The old `VIEW` macro for A1 (aka TZIP-4) views support was renamed to `VIEW_` (A1/TZIP-4).
* [!1010](https://gitlab.com/morley-framework/morley/-/merge_requests/1010)
  Add timelock puzzle support.
  + Binary encoding primitives moved to `Morley.Micheline.Binary.Internal`
  and exposed from there.
  + Added binary encoding primitives for nonnegative integers `buildNatural`
  and `getNatural`.
  + Added timelock puzzle algorithms to `Morley.Tezos.Crypto.Timelock`
  + Added support for `chest` and `chest_key` types. Those are represented
  by `TChest` and `TChestKey` in Haskell.
  + Added support for `OPEN_CHEST` instruction
  + Added `create_chest` command to Morley CLI to create a timelocked chest
  from user-supplied parameters. This isn't necessarily cryptographically safe
  (it was neither written nor audited by security experts), and is primarily
  intended for testing purposes.

1.16.0
======
* [!1011](https://gitlab.com/morley-framework/morley/-/merge_requests/1011)
  Support working with delegates in the morley interpreter
  + `SET_DELEGATE` previously was a no-op. Now it emits an operation,
  which is then interpreted to set a field in the contract state.
* [!989](https://gitlab.com/morley-framework/morley/-/merge_requests/989)
  Switch from interpolate to home-grown string interpolation module
  + Adds `Morley.Util.StringInterpolation` module to `morley`.
  + Note this changes the syntax of slices in `md` quasiquoter. Before
  we could use expressions `#{expression}`, now we can only use variable
  names `#{name}`.
* [!909](https://gitlab.com/morley-framework/morley/-/merge_requests/909)
  + Add new patterns to `Morley.Util.Named` allowing to pattern-match more easily on named variables
  + Replaced import of `Named` package with `Morley.Util.Named`
* [!995](https://gitlab.com/morley-framework/morley/-/merge_requests/995)
  Allow users to save meta instructions in the typed representation.
  + New typed instruction: `Meta`, which takes some existential value that can be matched and tested with `cast`.
* [!851](https://gitlab.com/morley-framework/morley/-/merge_requests/851)
  + Make `source` argument optional in `readValue`, `readContract` and similar functions.
  + Rename `ImportContractError` and `ImportValueError` types to `ContractReadError` and `ValueReadError` respectively. Their constructors (starting from `ICE` and `VCE` are updated accordingly).
* [!971](https://gitlab.com/morley-framework/morley/-/merge_requests/971)
  Fix exponential backtracking when parsing Michelson tuple values
  + New helpers in "Morley.Michelson.Parser.Helpers": `sepBy1` and `some'`, which are versions of Megaparsec's `sepBy1` and `some` returning a `NonEmpty` instead of `[]`.
  + Removed `mparens` from "Morley.Michelson.Parser.Helpers": it happens to be unused now, and can be pretty dangerous.
  + Removed `typeWithParen` from "Morley.Michelson.Parser.Type", since it was completely equivalent to just `type_`, but its performance was slightly worse.
* [!941](https://gitlab.com/morley-framework/morley/-/merge_requests/941)
  Use pretty-printer in PRINT instruction
  + New `RenderDoc` instance for typed `Value`s without operations
  + Some places that were using `Buildable Value` instance changed to use `buildRenderDoc` to preserve `HasNoOp` constraint (where it makes sense, i.e. outside of other `Buildable` instances)
  + New `Buildable` instance for typed `Value`s, closely mirroring the behaviour of the old one, but able to print opreations via `Buildable Operation` instance.
  + Use `Fmt.pretty` instead of `show` in `PRINT` implementation
  + Use `Buildable Operation` pretty-printer for Michelson failure reports (i.e. in `Buildable MichelsonFailed` instance, used for `FAILWITH`)
* [!943](https://gitlab.com/morley-framework/morley/-/merge_requests/943)
  + Place modules below a top-level `Morley` name.
* [!918](https://gitlab.com/morley-framework/morley/-/merge_requests/918)
  + Add `pairMisc` rule which handles `PAIR :# CDR` and `PAIR :# CAR` to Optimizer
  + Add `justDoubleDrops` rule optimizing `i :# DROP` where `i :: a : b : s -> c : s` to Optimizer
  + Make `optimize` function several-staged: flatten the sequence to right-combed firstly, then apply default rules and finally perform `adjacentDrops`
  + Update documentation of Optimizer
* [!937](https://gitlab.com/morley-framework/morley/-/merge_requests/937)
  Attach source code location to interpreter error messages
  + New `Buildable InstrCallStack` instance
  + New `ceInstrCallStack :: InstrCallStack` field in `ContractEnv`
  + New `MichelsonFailureWithStack` type which is a product of `MichelsonFailed` and `InstrCallStack`, with a `Buidlable` instance
  + Changed interpreter error type from `MichelsonFailed` to `MichelsonFailureWithStack`
* [!945](https://gitlab.com/morley-framework/morley/-/merge_requests/945)
  + Bump Stackage LTS version from 17.9 to 18.10.
* [!908](https://gitlab.com/morley-framework/morley/-/merge_requests/916)
  + Remove `NFData` instance for `Rec` and require `vinyl` ≥ 0.13.1.
* [!881](https://gitlab.com/morley-framework/morley/-/merge_requests/881)
  + Rename `PrintedValScope` to `UntypedValScope` with related types and functions.

1.15.1
======
* [!908](https://gitlab.com/morley-framework/morley/-/merge_requests/908)
  + Remove `NFData` constrains from `Instr` constructors
* [!879](https://gitlab.com/morley-framework/morley/-/merge_requests/879)
  + Add Buildable and RenderDoc instances for `Instr inp out` type.
  + Instances are based on transforming `Instr inp out` to list of `ExpandedOp` and using its instance
* [!900](https://gitlab.com/morley-framework/morley/-/merge_requests/900)
  + Added clarification that "OCaml reference client" is `tezos-client`.

1.15.0
======
* [!896](https://gitlab.com/morley-framework/morley/-/merge_requests/896)
  + Move `MorleyLogs` in interpreter monad to a dedicated `WriterT` layer.
* [!878](https://gitlab.com/morley-framework/morley/-/merge_requests/878)
  + Fixed behavior of `packValue` to match the `tezos-client hash data`
    behavior.
  + Pair conversion in `instance ToExpression Untyped.Value` always produces
    `ExpressionPrim` instead `ExpressionSeq`. Optimization of right-combed pairs
    moved to `Michelson.Typed.Convert` and depends on `UntypingOptions`.
* [!880](https://gitlab.com/morley-framework/morley/-/merge_requests/880)
  + Fixed loss of type annotations in serialization of right-combed pairs.
* [!795](https://gitlab.com/morley-framework/morley/-/merge_requests/795)
  + Changed typecheck exception messages.
  + Changed Buildable instances for several datatypes to be based on
     RenderDoc instances.
  + Changed the regexp in scripts/regenerate-gold-files-for-verbose-typechecking.sh
    so it does not ommit extra space in .gold files on MacOS.
* [!863](https://gitlab.com/morley-framework/morley/-/merge_requests/863)
  + Removed conversion from variable anotations to field anontations in
    `PAIRN` instruction.
* [!856](https://gitlab.com/morley-framework/morley/-/merge_requests/856)
  + Removed `Typeable` constrains from singletons generated by `T`.
  + Added the `Util.Sing` module with functions that allow equality comparison
    using `SDecide`.
* [!858](https://gitlab.com/morley-framework/morley/-/merge_requests/858)
  + Added `SomeStorage` type.
  + Modified `runContract` to return `SomeStorage`.
* [!833](https://gitlab.com/morley-framework/morley/-/merge_requests/833)
  + Fixed `ContainsBigMap` and `ContainsNestedBigMaps` instances for `TContract`.
  + Added `Data` instance to `BigMap` and `BigMapId`.
  + Added `requireEqT`, `castM` and `gcastM` to `Util.Typeable`.
* [!859](https://gitlab.com/morley-framework/morley/-/merge_requests/859)
  + Added `BigMapId` type.
  + Added ID to `VBigMap` and `BigMap`.
  + Updated interpreter to assign a unique big_map ID to each big_map, and
    keep track of how many big_maps have been created with a `BigMapCounter`.
  + Deleted `dfsValue`.
  + Renamed `dfsModifyValue` and `dfsFoldValue` to `dfsMapValue` and `dfsFoldMapValue`.
  + Added `dfsTraverseValue` and `dfsFoldMapValueM`.
* [!840](https://gitlab.com/morley-framework/morley/-/merge_requests/840)
  + Changed behaviour of deserializer: it fails if the expression contains
    more annotations than expected instead of ignoring them.
  + Changed behaviour of serializer: it uses list notation for all pair
    values, not just for right-combed ones.
* [!816](https://gitlab.com/morley-framework/morley/-/merge_requests/816)
  + Removed the `SingI` and `KnownPeano` constraints by introducting the
    `PeanoNatural` data type.
  + The similar updates for `SingT`. Removed the `SingI` constraints in the
    constructors of `SingT`
* [!855](https://gitlab.com/morley-framework/morley/-/merge_requests/855)
  + Fix custom error arguments comparison in nettest by converting them
    to `Value t` before checking for equality.
* [!830](https://gitlab.com/morley-framework/morley/-/merge_requests/830)
  + Added the contract's address to some of the `ExecutorError'`
    constructors: `EEIllTypedParameter` and `EEUnexpectedParameterType`.
* [!820](https://gitlab.com/morley-framework/morley/-/merge_requests/820)
  + Moved functions for contract importing (like `importContract`) from cleveland.
  + Added functions for values importing (`readValue`, `importValue` and others).
* [!832](https://gitlab.com/morley-framework/morley/-/merge_requests/832)
  + Add tickets feature.
  + Add dupable restriction to `DUP`-like instructions.
* [!838](https://gitlab.com/morley-framework/morley/-/merge_requests/838)
  + All unsafe functions and data constructors now contain "unsafe" word
    at prefix position. E.g `UnsafeUParam`, `unsafeParseKeyHash`.
* [!828](https://gitlab.com/morley-framework/morley/-/merge_requests/828)
  + Fix bug in serialization of instructions with variable annotations:
    `EMPTY_SET`, `EMPTY_MAP`, `PUSH`, `LAMBDA`, `CAST`, `CONTRACT`.
* [!770](https://gitlab.com/morley-framework/morley/-/merge_requests/770)
  + Added `Fn` meta-instruction to support Morley's typed stack function in
    typed form.
  + Added `STACKTYPE` as a typed `ExtInstr`.
* [!822](https://gitlab.com/morley-framework/morley/-/merge_requests/822)
  + Add new `edo` instruction: `UNPAIR`.
  + Rename `UNPAIR` macro to `UNPAPAIR`.
  + Expand `UNPAPAIR` macros to `UNPAIR` instructions.
  + The `Seq` constructor can now be used as a right-associative operator.
  + Optimize `DUP; CAR; DIP { CDR }` to `UNPAIR`
  + Optimize `DUP; CDR; DIP { CAR }` to `UNPAIR; SWAP`
* [!825](https://gitlab.com/morley-framework/morley/-/merge_requests/825)
  + Fix bug in morley executable's `repl` command: consume the entire input
    when parsing an instruction.
  + Fix bug in the handling of special annotations for `CAR`/`CDR`.
  + Fix bug in parsing of `PAIR` instructions with exactly 1 field ann.
  + Preserve `CAR`/`CDR`'s original annotations when converting them back to
    michelson/binary/json.

1.14.0
======
* [!799](https://gitlab.com/morley-framework/morley/-/merge_requests/799)
  + Fix product type instances of 1-nary constructors and empty types
    (they could produce compilation error before).
* [!814](https://gitlab.com/morley-framework/morley/-/merge_requests/814)
  + Renamed `Type` from `Michelson.Untyped.Type` with `Ty`.
  + Added the usage of `Prelude.Type` instead of `Data.Kind.Type`,
    as a result `Kind.Type` was replaced with just `Type`.
* [!733](https://gitlab.com/morley-framework/morley/-/merge_requests/733)
  + Added special annotations handling for `LEFT` and `RIGHT` instructions.
* [!787](https://gitlab.com/morley-framework/morley/-/merge_requests/787)
  + Added support for `CAR k` and `CDR k` macros.
* [!747](https://gitlab.com/morley-framework/morley/-/merge_requests/747)
  + Fix `ligoLayout` not working for alphabetically unordered sum types.
* [!798](https://gitlab.com/morley-framework/morley/-/merge_requests/798)
  + Added helper functions for recent Peano utility types.
* [!815](https://gitlab.com/morley-framework/morley/-/merge_requests/815)
  + Added `IsList` and `Buildable` instances to `BigMap`.

1.13.0
======
* [!796](https://gitlab.com/morley-framework/morley/-/merge_requests/796)
  + Generalized `InstrWithNotes` to handle instructions that put
    more than one value at the top of the stack.
  + Added support for `GET_AND_UPDATE` instructions.
* [!774](https://gitlab.com/morley-framework/morley/-/merge_requests/774)
  + Added support for `BLS12-381` crypto primitives.
* [!776](https://gitlab.com/morley-framework/morley/-/merge_requests/776)
  + Added support for `DUP n` instruction.
* [!755](https://gitlab.com/morley-framework/morley/-/merge_requests/755)
  Restricted `FAILWITH` only to packable values, except `CONTRACT`s
* [!781](https://gitlab.com/morley-framework/morley/-/merge_requests/781)
  + Replaced mixins and dependency on `base` with `base-noprelude`.
  + Added doctest examples and enabled `doctest` tests in `morley:lib`.
* [!764](https://gitlab.com/morley-framework/morley/-/merge_requests/764)
  + Added support for `never` type.
* [!750](https://gitlab.com/morley-framework/morley/-/merge_requests/750)
  [!769](https://gitlab.com/morley-framework/morley/-/merge_requests/769)
  [!786](https://gitlab.com/morley-framework/morley/-/merge_requests/786)
  [!791](https://gitlab.com/morley-framework/morley/-/merge_requests/791)
  + Added support for `PAIR n`, `UNPAIR n`, `GET n` and `UPDATE n` instructions.
* [!778](https://gitlab.com/morley-framework/morley/-/merge_requests/778)
  + Added support for `VOTING_POWER` and `TOTAL_VOTING_POWER` instructions.
* [!767](https://gitlab.com/morley-framework/morley/-/merge_requests/767)
  Made `unit`, `key`, `signature`, `chain_id`, `option`, `or` types
  comparable in preparation for `edo` switch.

1.12.0
======
* [!751](https://gitlab.com/morley-framework/morley/-/merge_requests/751)
  + Added support for `LEVEL` instruction.
  + Added --level parameter to `morley` executable
* [!753](https://gitlab.com/morley-framework/morley/-/merge_requests/753)
  [!754](https://gitlab.com/morley-framework/morley/-/merge_requests/754)
  + `edo` changes:
    + Ability to parse right-combed types (e.g. `pair int nat string`)
      from all 3 formats (binary, micheline, michelson)
    + Ability to parse right-combed values (e.g. `Pair 1 2 "a"`)
      from all 3 formats (binary, micheline, michelson)
* [!742](https://gitlab.com/morley-framework/morley/-/merge_requests/742)
  Allowed parsing single field annotations for `LEFT` and `RIGHT` instructions.
* [!744](https://gitlab.com/morley-framework/morley/-/merge_requests/744)
  + Added `reifyDataType` and `deriveFullType` to `Util.CustomGeneric`.
  + Added `lookupTypeNameOrFail` to `Util.TH`.
* [!741](https://gitlab.com/morley-framework/morley/-/merge_requests/741)
  + Added support for `SHA3` and `KECCAK` instructions.

1.11.1
======
* [!740](https://gitlab.com/morley-framework/morley/-/merge_requests/740)
  Fixed `morley print` to correctly render `PUSH` instructions that
  previously caused contracts to be misaligned and rejected by `tezos-client`.

1.11.0
======
* [!731](https://gitlab.com/morley-framework/morley/-/merge_requests/731)
  + Added opportunity to reorder fields in `GenericStrategy`.
  + Added `GenericStrategy`-ies for compatibility with LIGO.
* [!724](https://gitlab.com/morley-framework/morley/-/merge_requests/724)
  Fixed `UNPACK` to accept pairs of comparable types.
* [!712](https://gitlab.com/morley-framework/morley/-/merge_requests/712)
  + In optimizer, by default lambdas are now also handled.
  + Field names in optimizer config are changed (prefix added).
* [!726](https://gitlab.com/morley-framework/morley/-/merge_requests/726)
  Added `Data` and `Plated` instances to `Morley.Micheline.Expression`.
* [!723](https://gitlab.com/morley-framework/morley/-/merge_requests/723)
  * Splitted `class ContainsDoc` into `ContainsDoc` and `ContainsUpdateableDoc`;
  * Allow avoiding explicit `DName` call (now `docGroup "Title"` works).
* [!684](https://gitlab.com/morley-framework/morley/-/merge_requests/684)
  Simplify working with autodoc in contracts.
  (follow the deprecation warnings in case this hits you).

1.10.0
======
* [!692](https://gitlab.com/morley-framework/morley/-/merge_requests/692)
  The `ToJSON` instance for Micheline `Expression` now produces more compact JSON values,
  by omitting the `"annots"` and `"args"` fields when these lists are empty.
* [!673](https://gitlab.com/morley-framework/morley/-/merge_requests/673)
  + Removed `TextException`.
  Migration guide: use `StringException` or `throwString` from `safe-exceptions`.
  + Removed `displayUncaughtException`.
  Migration guide: use `uncaught-exception` library.
  + Added `Util.Main` module, consider using it in your `Main.hs`.
* [!657](https://gitlab.com/morley-framework/morley/-/merge_requests/657)
  Make `namedParser` handle complex-worded options neatly.
* [!678](https://gitlab.com/morley-framework/morley/-/merge_requests/678)
  Added `FromExpression` instances for `Michelson.Untyped.Type`, `Michelson.Typed.T`,
  and `Michelson.Typed.Instr`.
* [!607](https://gitlab.com/morley-framework/morley/-/merge_requests/607)
  Removed `parse` from the executable.
* [!659](https://gitlab.com/morley-framework/morley/-/merge_requests/659)
  Remove `Michelson.Interpret.MichelsonAmbigousEpRef` exception constructor.
* [!664](https://gitlab.com/morley-framework/morley/-/merge_requests/664)
  Added `Tezos.Crypto.Sign`.
* [!638](https://gitlab.com/morley-framework/morley/-/merge_requests/638)
  Added `Morley.Micheline.Json.TezosMutez`.
* [!638](https://gitlab.com/morley-framework/morley/-/merge_requests/638)
  Added `Tezos.Core.prettyTez`.

1.9
=====
* [!653](https://gitlab.com/morley-framework/morley/-/merge_requests/653)
  + `Michelson.OpSize` module is moved to `Michelson.Untyped.OpSize`.
  + Added primitives for evaluating `OpSize` in typed Michelson.
  + Added `instance Buildable OpSize`.
* [!633](https://gitlab.com/morley-framework/morley/-/merge_requests/633)
  Added `Tezos.Core.timestampToUTCTime`.

1.8.1
=====
* [!613](https://gitlab.com/morley-framework/morley/-/merge_requests/613)
  Added `HasCLReader` instances for `Word16` and `String`.

1.8.0
=====

* [!610](https://gitlab.com/morley-framework/morley/-/merge_requests/610)
  Remove `Arbitrary` instances and everything else that depends on `QuickCheck`.
* [!616](https://gitlab.com/morley-framework/morley/-/merge_requests/616)
  Make `RootAnn` a mere type alias of `FieldAnn`.
* [!615](https://gitlab.com/morley-framework/morley/-/merge_requests/615)
  Make `mkEntrypointsMap` accept `ParameterType` instead of `Type`.
* [!567](https://gitlab.com/morley-framework/morley/-/merge_requests/567)
  Add `DConversionInfo` that describes Haskell <-> Michelson conversion
  mechanism.
* [!585](https://gitlab.com/morley-framework/morley/-/merge_requests/585)
  Add `Exception` instance for `ParseChainIdError`.
* [!598](https://gitlab.com/morley-framework/morley/-/merge_requests/598)
  Fix bug: correct processing of `EDIV` of negative `Integer` and `Natural`.
* [!574](https://gitlab.com/morley-framework/morley/-/merge_requests/574)
  Implement a verbose switch for `morley typecheck`. It allows to
  print a stack type after every well-typed instruction, even if a
  contract as a whole is ill-typed.

1.7.1
=====
* [!549](https://gitlab.com/morley-framework/morley/-/merge_requests/549)
  Add `instance Buildable TezosInt64`.

1.7.0
=====
* [!565](https://gitlab.com/morley-framework/morley/-/merge_requests/565)
  Remove useless error entities from `Michelson.Interpret`.
* [!563](https://gitlab.com/morley-framework/morley/-/merge_requests/563)
  Fix handling of Natural numbers.
* [!554](https://gitlab.com/morley-framework/morley/-/merge_requests/554)
  Fix 'SELF' instruction packing.
* [!548](https://gitlab.com/morley-framework/morley/-/merge_requests/548)
  + The interpreter now takes a typed contract and storage value for origination operations.
  + Use binary serialization to compute operation hashes and addresses.
  + Add `typeCheckContractAndStorage` to `Michelson.TypeCheck`
  + Remove `withGlobalOperation` from `Michelson.Runtime`
  + Remove `EEIllTypedContract` and `EEIllTypedStorage` constructors from `ExecutorError'`

1.6.0
=====
* [!323](https://gitlab.com/morley-framework/morley/-/merge_requests/323)
  Add `parseSecretKey` which allows parsing all types of `SecretKey`.
* [!537](https://gitlab.com/morley-framework/morley/-/merge_requests/537)
  Permit `SELF %default` instruction.
* [!522](https://gitlab.com/morley-framework/morley/-/merge_requests/522)
  Allow calling the interpreter with a typed transfer parameter and
  avoid unnecessary typechecking.
* [!495](https://gitlab.com/morley-framework/morley/-/merge_requests/495)
  Add source location to typed `Instr` AST.
* [!521](https://gitlab.com/morley-framework/morley/-/merge_requests/521)
  Document generater can now generate table of contents.

1.5.0
=====
* [!509](https://gitlab.com/morley-framework/morley/-/merge_requests/509)
  Implement missing parts of `secp256k1` and `P256`.
  + `checkSignature` now works correctly with all signature formats.
* [!511](https://gitlab.com/morley-framework/morley/-/merge_requests/511)
  Add `zeroMutez` and `oneMutez` for safe initial values.
* [!500](https://gitlab.com/morley-framework/morley/-/merge_requests/500)
  Add ability to parse optimized formats of `signature`, `contract`, `key`, and `key_hash`.
* [!486](https://gitlab.com/morley-framework/morley/-/merge_requests/486)
  Add `Util.CustomGeneric` to derive `Generic` instances with a custom tree structure.
* [!466](https://gitlab.com/morley-framework/morley/-/merge_requests/466)
  Add sub-command 'repl' to Morley executable which starts an REPL that
  can execute Morley instructions.
* [!492](https://gitlab.com/morley-framework/morley/-/merge_requests/492)
  + `TypeHasDoc` instance for `newtype` now has its field remove by default
  without having to use `haskellRepNoFields`.
  + Add `haskellAddNewtypeField` function to add field name for `newtype`
  if desired.
* [!487](https://gitlab.com/morley-framework/morley/-/merge_requests/487)
  Fix typechecking error message pointing to the whole `map unit nat` as not comprable.
  + It now only points to `unit` type as not comparable.
* [!457](https://gitlab.com/morley-framework/morley/-/merge_requests/457)
  + Allow using the same position when defining `DocItem`.
  + When two `DocItem`s have the same position, they will be sorted alphabetically by their names.
* [!480](https://gitlab.com/morley-framework/morley/-/merge_requests/480)
  Arithmetic instructions (ADD, MUL, EDIV, LSL, ...) now take annotations into account.
    + Results from the instructions are also properly annotated.
* [!470](https://gitlab.com/morley-framework/morley/-/merge_requests/470)
  Move `Michelson.Doc.Test`, `Michelson.Test` and  Hedgehog generators to
  a new package: `cleveland`.
* [!453](https://gitlab.com/morley-framework/morley/-/merge_requests/453)
  Order of top-level entries in contract are now preserved.
  + `PACK`/`UNPACK` now serialize contracts according their top-level entries order.
  + Pretty printer now display contracts according to their given top-level entries order.
* [!445](https://gitlab.com/morley-framework/morley/merge_requests/445)
  Remove unnecessary `TcOriginatedContracts` usages and split `typeCheckTopLevelValue`
  to `typeCheckParameter` (the only case where `TcOriginatedContracts` is useful)
  and `typeCheckStorage`.

1.4.0
=====
* [!446](https://gitlab.com/morley-framework/morley/-/merge_requests/446)
  Include source locations and let-binding stacks into typecheck errors.
* [!449](https://gitlab.com/morley-framework/morley/-/merge_requests/449)
  + Improve `Michelson.Test.Integrational` interface.
  + Remove `IntegrationalValidator`, `SuccessValidator`, `validate`,
    `composeValidators`, `composeValidatorsList`, `expectAnySuccess`.
  + Add `unexpectedInterpreterError`, `attempt`, `catchExpectedError`.
  + Change type of `expect*`/`tExpect*` functions.
    They now return `IntegrationalScenario` instead of `SuccessValidator` / `Bool`.
  + Rename `ValidationError` to `TestError`
  + Replace `TestError`'s `UnexpectedExecutorError` constructor with
    `InterpreterError` and `UnexpectedInterpreterError`.
* [!441](https://gitlab.com/morley-framework/morley/merge_requests/441)
  Remove `tezos-bake-monitor-lib dependency`, and define value to json conversion
  using binary round trip locally instead.
* [!448](https://gitlab.com/morley-framework/morley/merge_requests/448)
  Remove `base-noprelude` dependency and use mixins to hide `Prelude` instead.
* [!367](https://gitlab.com/morley-framework/morley/merge_requests/367)
  Values and instructions now carry some extra constraints in order to avoid
  construction of illegally typed values from within Haskell.
  `WellTyped` now also requires `Typeable` and `SingI` constraints.
* [!409](https://gitlab.com/morley-framework/morley/-/merge_requests/409)
  + Add hedgehog support
* [!438](https://gitlab.com/morley-framework/morley/-/merge_requests/438)
  + Fixed parsing bug: support semicolons after block comments.

* [!424](https://gitlab.com/morley-framework/morley/-/merge_requests/424) Changed
  contract address computation logic in the interpreter to match the
  reference implementation a bit more closely by including a global counter and
  an origination index (nonce) to contract's hash. Specifically, now the same
  contract may be originated several times, leading to different addresses,
  just like in Tezos.

1.3.0
=====
* [!417](https://gitlab.com/morley-framework/morley/-/merge_requests/417)
Change the license to MIT.
* [!386](https://gitlab.com/morley-framework/morley/-/merge_requests/386)
  + Add a way to describe constructors and fields of ADTs in Markdown documentation.
  + Require that all contracts include documentation for the storage type.
* [!396](https://gitlab.com/morley-framework/morley/merge_requests/396) Fixed
  typed `Notes` to permit pair annotations for container keys.
* [!359](https://gitlab.com/morley-framework/morley/merge_requests/359) Renamed
  'FullContract' to 'Contract', removed the prefix `f` from its record fields
  and removed "Full" from the names of the functions:
  `convertFullContract`, `printTypedFullContract` and `mapFullContractCode`.
* [!354](https://gitlab.com/morley-framework/morley/-/merge_requests/354)
  + Fix bug: add `ChainId` to `IsPrimitiveValue` type family.
  + Add `GInstrDeconstruct` type class and related functions
    which serve to deconstruct datatype into its fields on the stack.
  + Add `gInstrConstructStack` to `GInstrConstruct` type class that serves
    to construct a datatype from its fields on the stack.
  + Add `GFieldNames` type family returning list of `Symbol`s:
    the names of direct fields of datatype.

1.2.0
=====
* [!343](https://gitlab.com/morley-framework/morley/-/merge_requests/343)
  + Make pairs comparable and available to use as map keys and in sets.
  + CT type has been integrated into T.
  + A `WellTyped` type class has been added to ensure that a type
    represented by  T is valid as per Michelson's rules.
* [!358](https://gitlab.com/morley-framework/morley/-/merge_requests/358)
  Remove deprecated `STEPS_TO_QUOTA` instruction.
* [!368](https://gitlab.com/morley-framework/morley/-/merge_requests/368)
  + Update JSON encoding of types.
  + Rename fields of the untyped `Contract'`.
* [!286](https://gitlab.com/morley-framework/morley/-/merge_requests/286)
  Morley supports field annotations for parameter root now.
* [!344](https://gitlab.com/morley-framework/morley/-/merge_requests/344)
  Fixed a bug in `Michelson.Analyzer` and added `linearizeLeftDeep`.

1.1.0
=====
* [!337](https://gitlab.com/morley-framework/morley/-/merge_requests/337)
  Added an extension for Michelson comment to typed and untyped contract representation.
* [!306](https://gitlab.com/morley-framework/morley/-/merge_requests/306)
  Added PAIR/UNPAIR rule to optimizer.
* [!314](https://gitlab.com/morley-framework/morley/-/merge_requests/314)
  Fixed a bug in the implementation of `MAP` operation: it did not preserve modifications to
  stack.
* [!261](https://gitlab.com/morley-framework/morley/merge_requests/261)
  Slightly modified interpreter API.
* [!313](https://gitlab.com/morley-framework/morley/-/merge_requests/313)
  Made `typeCheckValue` polymorphic in desired type, instead of using
  existential wrappers.
* [!310](https://gitlab.com/morley-framework/morley/merge_requests/232)
  + Add `DGeneralInfoSection` documentation section with git revision and
    potentially other info.
  + Add `buildLorentzDocWithGitRev` and `buildInstrDocWithGitRev` to
    API to add a git revision to contract docs from the executable.
* [!121](https://gitlab.com/morley-framework/morley/merge_requests/302)
  `BALANCE` instruction now returns the balance with funds from incoming
  transaction.
* [!294](https://gitlab.com/morley-framework/morley/-/merge_requests/294)
  + Added `Paths_*` modules to `autogen-modules` in cabal files.  Removed `-O0`
  + from default GHC options. Please set `ghc-options` in your `stack.yaml` or
  `cabal.project.local`.
* [!271](https://gitlab.com/morley-framework/morley/merge_requests/271) Renamed
  'Contract' to 'ContractCode', and appended "Code" to the names of two functions:
  'convertContract' and 'printTypedContract'
* [!278](https://gitlab.com/morley-framework/morley/merge_requests/278)
  Added some utilities for command line option parsing, see `Util.CLI` and `Morley.CLI` modules.
* [!268](https://gitlab.com/morley-framework/morley/merge_requests/268)
  Test functions which import typed contract now return `FullContract` instead
  of `Contract`, thus preserving parameter and storage annotations. In case you
  don't need this behaviour, use `fcCode` for conversion.
  Test functions which import Lorentz contracts have been removed because they
  cannot be implemented sanely, and Lorentz is assumed to be used to generate
  code, do not use it for work with textual Michelson contracts.
* [!212](https://gitlab.com/morley-framework/morley/merge_requests/212)
  + Fix `AND` instruction return type.
  + Add `DUP n` macro support.
  + Fix `LAMBDA` instruction printer.
* [!265](https://gitlab.com/morley-framework/morley/merge_requests/265)
  The semicolons between instructions are now optional.

1.0.0
=====

* [!215](https://gitlab.com/morley-framework/morley/merge_requests/215)
  Major change: all Lorentz functionality was moved into `lorentz` package.
  A small portion of testing library code was moved around (from `Lorentz.*` to `Michelson.*` or vice versa).

0.7.0
=====

* [!237](https://gitlab.com/morley-framework/morley/merge_requests/237)
  The `option (nat %foo)` is now accepted syntax. The `%foo` is ignored.
* [!241](https://gitlab.com/morley-framework/morley/merge_requests/241)
  Derive `Show` instance for `Void_`.
* [!238](https://gitlab.com/morley-framework/morley/merge_requests/238)
  + `NoExplicitDefaultEntrypoint` is now GHC-understandable constraint,
    for previous behaviour with nice error message use
    `ForbidExplicitDefaultEntrypoint`.
  + `CanHaveEntrypoints` is made stricter, now it returns true only for
     Michelson sum types. If that's a problem for you, consider using
     `ShouldHaveEntrypoints` wrapper.
  + Added pseudo `Empty` type.
* [!219](https://gitlab.com/morley-framework/morley/merge_requests/219)
  Various entrypoints-related updates.
  + Integrational tests now carry `TAddress` as a reference to contract.
    Make sure that parameters of the originated contracts are clear;
    e.g. origination of `consumer` contract may require explicit type
    annotation to specify parameter type.
    If you passed a reference to the originated contract to `View` constructor,
    consider using `mkView` instead or applying `callingDefTAddress` to the
    reference.
  + Generally it is encouraged to use `TAddress` instead of `Address`
    where it is used to refer to a contract.
  + `fromContractAddr` renamed to `fromContractRef`.
  + `Default` instances for `{Some,}EntrypointCall` are removed, use
     one of explicit versions: `epcPrimitive`, `epcCallRootUnsafe`.
  + Reworked `FutureContract` type.

0.6.0
=====

* [!127](https://gitlab.com/morley-framework/morley/merge_requests/127)
  Limit annotations' allowed characters
* [!184](https://gitlab.com/morley-framework/morley/merge_requests/184)
  Fix `LAMBDA` instruction type converter bug
* [!173](https://gitlab.com/morley-framework/morley/merge_requests/173)
  [!197](https://gitlab.com/morley-framework/morley/merge_requests/197)
  Add support for lightweight entrypoints in Lorentz, as well as in
  the interpreter runner and morley executable.
  Support entrypoint references in `SELF` instruction.
  Minor refactorings related to entrypoints.
* [!201](https://gitlab.com/morley-framework/morley/merge_requests/201)
  + Add entrypoint argument to methods in testing eDSL. Preserve the existing type of `lCall` and deprecate it.
  + Deprecate `Lorentz.TestScenario`.
* [!208](https://gitlab.com/morley-framework/morley/merge_requests/208)
  Resolve an ambiguity within integrational and unit interpreters
  by renaming interpreter in `Michelson.Runtime` to `executor`.
* [!180](https://gitlab.com/morley-framework/morley/merge_requests/180)
  Remove `coerce_` in favor of more safe variants.
  Migration guide: pick one of the functions from 'Lorentz.Coercions' module.
  In most cases you need one of the following ones:
    * `checkedCoerce_`
    * `coerceWrap` / `coerceUnwrap`
    * `forcedCoerce_` (aka old `coerce_` - this variant provides the least safety).
* [!180](https://gitlab.com/morley-framework/morley/merge_requests/180)
  Type arguments of `coerceWrap` and `coerceUnwrap` are flipped.
  This change concerns you if you used type application on these functions.

0.5.0
=====

* Require `morley-prelude` to be ≥ 0.3.0 to make Hackage happy.
* [!156](https://gitlab.com/morley-framework/morley/merge_requests/156)
  Consider annotations in PACK and UNPACK.

0.4.0
=====

* Implemented most of Babylon changes: new instructions, elimination of `big_map` restructions, new restrictions for the `contract` type, partial support for entrypoints. Some instructions have been removed/deprecated.
* Many updates of helper data types for upgradeable contracts: `UParam` and `UStore`.
* Michelson printer can produce pretty output, not just one line.
* Added utilities for contract processing and analysis: optimizer, string transformer, errors transformer, analyzer.
* Added `tasty` helpers to the testing engine.
* Added annotations to typed representation and Lorentz.
* Added automatic documentation generator.
* Modified standard errors in Lorentz: now they are based on `CustomError`.
* Added unpacking from readable representation.
* Removed `parameter` and `storage` aliases, they were not very useful and complicated the code.
* Extended cryptographic types and `Address` to work with other curves (tz2 and tz3 addresses).
* Made it nearly impossible to generate dead code (which is illegal in Michelson) using Lorentz.
* Various bug fixes.

0.3.0.1
=======

* Update maintainer.

0.3.0
=====

* [TM-68](https://issues.serokell.io/issue/TM-68) Lorentz DSL which allows one to write contracts directly in Haskell.
May be moved to a separate package later.
* [TM-132](https://issues.serokell.io/issue/TM-132) Names for contracts in integrational tests.
* [TM-35](https://issues.serokell.io/issue/TM-35) `PACK` and `UNPACK` instructions.
* [TM-27](https://issues.serokell.io/issue/TM-27) Proper handling of `FAILWITH`.
* [TM-44](https://issues.serokell.io/issue/TM-44) [TM-124](https://issues.serokell.io/issue/TM-124) Reorganization of modules.
* Bug fixes.

0.2.0.1
=======

* Update documentation and metadata.

0.2.0
=====

Initial release.

* Typechecker and interpreter for Michelson.
* Morley extensions:
  - syntax sugar
  - let-blocks
  - inline assertions
* EDSL for unit testing and integrational testing
