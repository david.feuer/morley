-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Util.Exception
  ( -- * General simple helpers
    throwLeft
  ) where

-- | If monadic action returns a 'Left' value, it will be
-- thrown. Otherwise the returned value will be returned as is.
throwLeft :: (MonadThrow m, Exception e) => m (Either e a) -> m a
throwLeft =
  (>>= \case
      Left e -> throwM e
      Right x -> return x)
