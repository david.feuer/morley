-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Util.Text
  ( headToLower
  , surround
  , dquotes
  ) where

import Data.Char (toLower)
import Data.Text qualified as T

-- | Leads first character of text to lower case.
--
-- For empty text this will throw an error.
headToLower :: HasCallStack => Text -> Text
headToLower txt = case T.uncons txt of
  Nothing -> error "Empty text"
  Just (c, cs) -> T.cons (toLower c) cs

surround :: Semigroup a => a -> a -> a -> a
surround pre post content = pre <> content <> post

dquotes :: (Semigroup a, IsString a) => a -> a
dquotes = surround "\"" "\""
