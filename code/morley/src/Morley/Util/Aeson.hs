-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Utilities related to the @aeson@ package.

module Morley.Util.Aeson
  ( morleyAesonOptions
  ) where

import Data.Aeson.Casing (aesonPrefix, camelCase)
import Data.Aeson.TH (Options)

-- | Options that we use in @morley@-based packages (including
-- @morley@) by default.
morleyAesonOptions :: Options
morleyAesonOptions = aesonPrefix camelCase
