-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Michelson.Parser.Common
  ( viewName_
  ) where

import Text.Megaparsec (anySingle, customFailure, manyTill)
import Text.Megaparsec.Char (string)

import Morley.Michelson.Parser.Error
import Morley.Michelson.Parser.Lexer
import Morley.Michelson.Parser.Types
import Morley.Michelson.Untyped

viewName_ :: Parser le ViewName
viewName_ = lexeme do
  string "\""
  str <- manyTill anySingle (string "\"")
  mkViewName (toText str)
    & either (customFailure . ViewNameException) pure
