-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE QuantifiedConstraints #-}

module Morley.Michelson.Typed.Existential
  ( -- * SomeConstrainedValue and derivatives
    SomeConstrainedValue (.., SomeValue, SomeConstant, SomeStorage, SomePackedVal)
  , SomeConstant
  , SomeValue
  , SomeStorage
  , SomePackedVal
    -- * Other existentials
  , SomeContract (..)
  , SomeContractAndStorage (..)
  , SomeIsoValue (..)
  ) where

import Fmt (Buildable(..))

import Morley.Michelson.Printer.Util (RenderDoc(..))
import Morley.Michelson.Typed.Aliases
import Morley.Michelson.Typed.Convert ()
import Morley.Michelson.Typed.Haskell.Value (KnownIsoT)
import Morley.Michelson.Typed.Scope
import Morley.Michelson.Typed.T (T(..))
import Morley.Util.Sing (eqParamSing)

----------------------------------------------------------------------------
-- SomeConstrainedValue
----------------------------------------------------------------------------

data SomeConstrainedValue (c :: T -> Constraint) where
  SomeConstrainedValue :: forall t c. (c t) => Value t -> SomeConstrainedValue c

deriving stock instance Show (SomeConstrainedValue c)

instance Buildable (SomeConstrainedValue c) where
  build (SomeConstrainedValue v) = build v

instance (forall t. cs t => SingI t) => Eq (SomeConstrainedValue cs) where
  SomeConstrainedValue v1 == SomeConstrainedValue v2 = v1 `eqParamSing` v2

instance (forall t. cs t => HasNoOp t) => RenderDoc (SomeConstrainedValue cs) where
  renderDoc pn (SomeConstrainedValue v) = renderDoc pn v

----------------------------------------------------------------------------
-- SomeConstrainedValue synonyms
----------------------------------------------------------------------------

type SomeValue = SomeConstrainedValue SingI
pattern SomeValue :: () => SingI t => Value t -> SomeValue
pattern SomeValue x = SomeConstrainedValue x
{-# COMPLETE SomeValue #-}

type SomeConstant = SomeConstrainedValue ConstantScope
pattern SomeConstant :: () => ConstantScope t => Value t -> SomeConstant
pattern SomeConstant x = SomeConstrainedValue x
{-# COMPLETE SomeConstant #-}

type SomeStorage = SomeConstrainedValue StorageScope
pattern SomeStorage :: () => StorageScope t => Value t -> SomeStorage
pattern SomeStorage x = SomeConstrainedValue x
{-# COMPLETE SomeStorage #-}

type SomePackedVal = SomeConstrainedValue PackedValScope
pattern SomePackedVal :: () => PackedValScope t => Value t -> SomePackedVal
pattern SomePackedVal x = SomeConstrainedValue x
{-# COMPLETE SomePackedVal #-}

-- other synonyms should be easy to add by analogy, if needed.

----------------------------------------------------------------------------
-- Other existentials
----------------------------------------------------------------------------

-- | Hides some Haskell value put in line with Michelson 'Value'.
data SomeIsoValue where
  SomeIsoValue :: KnownIsoT a => a -> SomeIsoValue

data SomeContract where
  SomeContract :: Contract cp st -> SomeContract

instance NFData SomeContract where
  rnf (SomeContract c) = rnf c

deriving stock instance Show SomeContract

-- | Represents a typed contract & a storage value of the type expected by the contract.
data SomeContractAndStorage where
  SomeContractAndStorage
    :: forall cp st.
       (StorageScope st, ParameterScope cp)
       => Contract cp st
       -> Value st
       -> SomeContractAndStorage

deriving stock instance Show SomeContractAndStorage
