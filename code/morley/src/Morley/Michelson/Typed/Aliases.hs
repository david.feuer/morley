-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Michelson.Typed.Aliases
  ( Value
  , Operation
  , ContractCode
  , Contract
  , ViewCode
  , View
  , SomeView
  , ViewsSet
  , SomeViewsSet
  ) where

import Morley.Michelson.Typed.Contract
import Morley.Michelson.Typed.Instr
import Morley.Michelson.Typed.Value
import Morley.Michelson.Typed.View

type Value = Value' Instr
type Operation = Operation' Instr
type ContractCode cp st = ContractCode' Instr cp st
type Contract = Contract' Instr
type ViewCode arg st ret = ViewCode' Instr arg st ret
type View = View' Instr
type SomeView = SomeView' Instr
type ViewsSet = ViewsSet' Instr
type SomeViewsSet = SomeViewsSet' Instr
