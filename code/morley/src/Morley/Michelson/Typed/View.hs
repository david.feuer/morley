-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- 'newtype Container' deriving produced some fake warnings
{-# OPTIONS_GHC -Wno-redundant-constraints #-}

-- | Module, containing on-chain views declarations.
module Morley.Michelson.Typed.View
  ( -- * View
    ViewName (..)
  , mkViewName
  , viewNameToMText
  , ViewCode'
  , View' (..)
  , SomeView' (..)
  , someViewName

    -- * Views set
  , ViewsSet' (.., ViewsSet, ViewsList)
  , ViewsSetError (..)
  , mkViewsSet
  , emptyViewsSet
  , addViewToSet
  , lookupView
  , viewsSetNames
  , SomeViewsSet' (..)
  ) where

import Control.Monad.Except (throwError)
import Data.Default (Default(..))
import Data.Sequence qualified as Seq
import Fmt (Buildable(..), (+|), (|+))
import GHC.Exts (fromList)

import Morley.Michelson.Typed.Annotation
import Morley.Michelson.Typed.Scope
import Morley.Michelson.Typed.T (T(..))
import Morley.Michelson.Untyped.View (ViewName(..), mkViewName, viewNameToMText)
import Morley.Util.Sing


type ViewCode' instr arg st ret = instr '[ 'TPair arg st] '[ret]

-- | Contract view.
data View' instr arg st ret = (ViewableScope arg, SingI st, ViewableScope ret) => View
  { vName :: ViewName
    -- ^ Name of the view.
  , vArgument :: Notes arg
    -- ^ Argument type annotations.
  , vReturn :: Notes ret
    -- ^ Return type annotations.
  , vCode :: ViewCode' instr arg st ret
    -- ^ View code.
  }

deriving stock instance Show (ViewCode' instr arg st ret) => Show (View' instr arg st ret)
deriving stock instance Eq (ViewCode' instr arg st ret) => Eq (View' instr arg st ret)
instance NFData (ViewCode' instr arg st ret) => NFData (View' instr arg st ret) where
  rnf (View a b c d) = rnf (a, b, c, d)

data SomeView' instr st where
  SomeView :: View' instr arg st ret -> SomeView' instr st

deriving stock instance
  (forall arg ret. Show (ViewCode' instr arg st ret)) =>
  Show (SomeView' instr st)
instance
  (forall arg ret. Eq (ViewCode' instr arg st ret)) =>
  Eq (SomeView' instr st) where
    SomeView v1@View{} == SomeView v2@View{} = v1 `eqParamSing3` v2
instance
  (forall arg ret. NFData (ViewCode' instr arg st ret)) =>
  NFData (SomeView' instr st) where
    rnf (SomeView v) = rnf v

-- | Obtain the name of the view.
someViewName :: SomeView' instr st -> ViewName
someViewName (SomeView v) = vName v

-- View sets
----------------------------------------------------------------------------

-- | Views that belong to one contract.
--
-- Invariant: all view names are unique.
--
-- Implementation note: lookups still take linear time.
-- We use 'Seq' for simplicity, as in either case we need to preserve the order
-- of views (so that decoding/encoding roundtrip).
newtype ViewsSet' instr st = UnsafeViewsSet { unViewsSet :: Seq $ SomeView' instr st }
  deriving newtype (Default, Container)

deriving stock instance
  (forall i o. Show (instr i o)) =>
  Show (ViewsSet' instr st)
deriving stock instance
  (forall i o. Eq (instr i o)) =>
  Eq (ViewsSet' instr st)
instance
  (forall i o. NFData (instr i o)) =>
  NFData (ViewsSet' instr st) where
    rnf (ViewsSet vs) = rnf vs

pattern ViewsSet :: Seq (SomeView' instr st) -> ViewsSet' instr st
pattern ViewsSet views <- UnsafeViewsSet views
{-# COMPLETE ViewsSet #-}

pattern ViewsList :: [SomeView' instr st] -> ViewsSet' instr st
pattern ViewsList views <- ViewsSet (toList -> views)
{-# COMPLETE ViewsList #-}

-- | Errors possible when constructing 'ViewsSet\''.
data ViewsSetError
  = DuplicatedViewName ViewName
  deriving stock (Show, Eq)

instance Buildable ViewsSetError where
  build = \case
    DuplicatedViewName name -> "Duplicated view name '" +| name |+ "'"

-- | Construct views set.
mkViewsSet :: [SomeView' instr st] -> Either ViewsSetError (ViewsSet' instr st)
mkViewsSet views = do
  forM_ (group . sort $ map someViewName views) $ \case
    name : _ : _ -> throwError $ DuplicatedViewName name
    _ : _ -> pass
    [] -> error "impossible"
  pure (UnsafeViewsSet $ fromList views)

-- | No views.
emptyViewsSet :: ViewsSet' instr st
emptyViewsSet = UnsafeViewsSet mempty

-- | Add a view to set.
addViewToSet
  :: View' instr arg st ret
  -> ViewsSet' instr st
  -> Either ViewsSetError (ViewsSet' instr st)
addViewToSet v views = do
  when (vName v `elem` viewsSetNames views) $
    throwError $ DuplicatedViewName (vName v)
  return (UnsafeViewsSet $ unViewsSet views Seq.|> SomeView v)

-- | Find a view in the set.
lookupView :: ViewName -> ViewsSet' instr st -> Maybe (SomeView' instr st)
lookupView name (ViewsSet views) =
  find (\(SomeView View{..}) -> vName == name) views

-- | Get all taken names in views set.
viewsSetNames :: ViewsSet' instr st -> [ViewName]
viewsSetNames = map someViewName . toList

data SomeViewsSet' instr where
  SomeViewsSet :: SingI st => ViewsSet' instr st -> SomeViewsSet' instr

deriving stock instance
  (forall i o. Show (instr i o)) =>
  Show (SomeViewsSet' instr)
instance
  (forall i o. Eq (instr i o)) =>
  Eq (SomeViewsSet' instr) where
    SomeViewsSet vs1 == SomeViewsSet vs2 = eqParamSing vs1 vs2
instance
  (forall i o. NFData (instr i o)) =>
  NFData (SomeViewsSet' instr) where
    rnf (SomeViewsSet vs) = rnf vs
