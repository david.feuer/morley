-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Michelson.Let
  ( LetType (..)
  , LetValue (..)
  ) where

import Data.Aeson.TH (deriveJSON)
import Data.Text qualified as T

import Morley.Michelson.Macro (ParsedOp)
import Morley.Michelson.Untyped (Ty, Value')
import Morley.Util.Aeson

-- | A programmer-defined constant
data LetValue = LetValue
  { lvName :: T.Text
  , lvSig :: Ty
  , lvVal :: (Value' ParsedOp)
  } deriving stock (Eq, Show)

{-# DEPRECATED LetValue, LetType "Let macros are deprecated" #-}

-- | A programmer-defined type-synonym
data LetType = LetType
  { ltName :: T.Text
  , ltSig :: Ty
  } deriving stock (Eq, Show)

deriveJSON morleyAesonOptions ''LetValue
deriveJSON morleyAesonOptions ''LetType
