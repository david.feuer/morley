-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Some simple aliases for Michelson types.

module Morley.Michelson.Untyped.Aliases
  ( Contract
  , View
  , Value
  , ExpandedExtInstr
  ) where

import Morley.Michelson.Untyped.Contract qualified as Untyped
import Morley.Michelson.Untyped.Ext qualified as Untyped
import Morley.Michelson.Untyped.Instr qualified as Untyped
import Morley.Michelson.Untyped.Value qualified as Untyped

type Value = Untyped.Value' Untyped.ExpandedOp
type View = Untyped.View' Untyped.ExpandedOp
type Contract = Untyped.Contract' Untyped.ExpandedOp
type ExpandedExtInstr = Untyped.ExtInstrAbstract Untyped.ExpandedOp
