-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | This module provides a data type for representing a partially typed
-- sequence of instructions.
--
-- It is needed to represent the fact that there can only be one well-typed node
-- in a sequence and it is the first one. Also, it serves its role to remove
-- @TCError@ usage from @TypeCheckedOp@.
module Morley.Michelson.TypeCheck.TypeCheckedSeq
  ( TypeCheckedInstr
  , TypeCheckedOp(..)
  , IllTypedInstr(..)
  , TypeCheckedSeq(..)
  , tcsEither
  , seqToOps
  , someInstrToOp
  , someViewToOp
  ) where

import Morley.Michelson.TypeCheck.Error (TCError)
import Morley.Michelson.TypeCheck.TypeCheckedOp
  (IllTypedInstr(..), TypeCheckedInstr, TypeCheckedOp(..), someInstrToOp, someViewToOp)
import Morley.Michelson.TypeCheck.Types (SomeInstr(..))

-- | Represents a partiall typed sequence of instructions.
data TypeCheckedSeq inp
  -- | A fully well-typed sequence.
  = WellTypedSeq (SomeInstr inp)
  -- | A well-typed prefix followed by some error and semi-typed instructions.
  | MixedSeq (SomeInstr inp) TCError [IllTypedInstr]
  -- | There is no well-typed prefix, only an error and semi-typed instructions.
  | IllTypedSeq TCError [IllTypedInstr]

seqToOps :: TypeCheckedSeq inp -> [TypeCheckedOp]
seqToOps = \case
  WellTypedSeq instr -> [someInstrToOp instr]
  MixedSeq instr _ tail' -> someInstrToOp instr : map IllTypedOp tail'
  IllTypedSeq _ tail' -> map IllTypedOp tail'

-- | Case analysis for @TypeCheckedSeq@.
tcsEither
  :: ([TypeCheckedOp] -> TCError -> a)
     -- ^ On error, with all already typechecked operations
  -> (SomeInstr inp -> a) -- ^ On well-typed instruction
  -> TypeCheckedSeq inp -- ^ The sequence to dispatch on
  -> a
tcsEither onErr onInstr v = case v of
  WellTypedSeq instr -> onInstr instr
  MixedSeq _ err _ -> onErr (seqToOps v) err
  IllTypedSeq err _ -> onErr (seqToOps v) err
