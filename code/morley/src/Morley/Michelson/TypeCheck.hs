-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Michelson.TypeCheck
  ( typeCheckContract
  , typeCheckContractAndStorage
  , typeCheckExt
  , typeCheckInstr
  , typeCheckList
  , typeCheckListNoExcept
  , typeCheckParameter
  , typeCheckStorage
  , typeCheckTopLevelType
  , typeCheckValue
  , typeVerifyContract
  , typeVerifyParameter
  , typeVerifyStorage
  , typeVerifyTopLevelType
  , typeVerifyView
  , module E
  , module M
  , module T
  , eqType
  , matchTypes
  ) where

import Morley.Michelson.TypeCheck.Error as E
import Morley.Michelson.TypeCheck.Ext
import Morley.Michelson.TypeCheck.Instr
import Morley.Michelson.TypeCheck.TypeCheck as T
import Morley.Michelson.TypeCheck.Types as M

import Morley.Michelson.TypeCheck.Helpers (eqType, matchTypes)
