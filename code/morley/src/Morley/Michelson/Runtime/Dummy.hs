-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Dummy data to be used in tests or demos where it's not essential.

module Morley.Michelson.Runtime.Dummy
  ( dummyNow
  , dummyLevel
  , dummyMaxSteps
  , dummyVotingPowers
  , dummyBigMapCounter
  , dummyContractEnv
  , dummyGlobalCounter
  , dummyOrigination
  ) where

import Data.Default (def)

import Morley.Michelson.Interpret (ContractEnv(..), RemainingSteps)
import Morley.Michelson.Runtime.GState (BigMapCounter, dummyVotingPowers, genesisAddress)
import Morley.Michelson.Typed (ParameterScope, StorageScope)
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.Operation (OriginationOperation(..))
import Morley.Tezos.Address (GlobalCounter(..))
import Morley.Tezos.Core (Timestamp(..), dummyChainId, tz)

-- | Dummy timestamp, can be used to specify current @NOW@ value or
-- maybe something else.
dummyNow :: Timestamp
dummyNow = Timestamp 100

dummyLevel :: Natural
dummyLevel = 4000

-- | Dummy value for maximal number of steps a contract can
-- make. Intentionally quite large, because most likely if you use
-- dummy value you don't want the interpreter to stop due to gas
-- exhaustion. On the other hand, it probably still prevents the
-- interpreter from working for eternity.
dummyMaxSteps :: RemainingSteps
dummyMaxSteps = 100500

dummyBigMapCounter :: BigMapCounter
dummyBigMapCounter = 0

dummyGlobalCounter :: GlobalCounter
dummyGlobalCounter = 0

-- | Dummy 'ContractEnv' with some reasonable hardcoded values. You
-- can override values you are interested in using record update
-- syntax.
dummyContractEnv :: ContractEnv
dummyContractEnv = ContractEnv
  { ceNow = dummyNow
  , ceMaxSteps = dummyMaxSteps
  , ceBalance = [tz|100u|]
  , ceContracts = mempty
  , ceSelf = genesisAddress
  , ceSource = genesisAddress
  , ceSender = genesisAddress
  , ceAmount = [tz|100u|]
  , ceVotingPowers = dummyVotingPowers
  , ceChainId = dummyChainId
  , ceOperationHash = Nothing
  , ceLevel = dummyLevel
  , ceInstrCallStack = def
  }

-- | 'OriginationOperation' with most data hardcoded to some
-- reasonable values. Contract and initial values must be passed
-- explicitly, because otherwise it hardly makes sense.
dummyOrigination
  :: (ParameterScope cp, StorageScope st)
  => T.Value st
  -> T.Contract cp st
  -> GlobalCounter
  -> OriginationOperation
dummyOrigination storage contract counter = OriginationOperation
  { ooOriginator = genesisAddress
  , ooDelegate = Nothing
  , ooBalance = [tz|100u|]
  , ooStorage = storage
  , ooContract = contract
  , ooCounter = counter
  }
