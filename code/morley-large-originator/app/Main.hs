-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- TODO [#712]: Remove this next major release
{-# OPTIONS_GHC -Wno-deprecations #-}

module Main
  ( main
  ) where

import Data.Default (def)
import Data.Text.Lazy.IO (writeFile)
import Fmt (padLeftF, pretty, (+|), (|+))
import Options.Applicative qualified as Opt
import Options.Applicative.Help.Pretty (Doc, linebreak)
import System.Directory (createDirectoryIfMissing)
import System.FilePath ((</>))

import Lorentz.Print (printLorentzValue)
import Morley.CLI
import Morley.Client
import Morley.Client.Action.Origination.Large
import Morley.Client.Parser
import Morley.Michelson.Printer (printTypedContract, printTypedValue)
import Morley.Michelson.Runtime (prepareContract, prepareContractExt)
import Morley.Michelson.TypeCheck (typeCheckContract, typeCheckingWith, typeVerifyStorage)
import Morley.Michelson.Typed
import Morley.Michelson.Untyped qualified as U
import Morley.Tezos.Address (Address, formatAddress)
import Morley.Tezos.Core
import Morley.Util.CLI (mkCommandParser)
import Morley.Util.Exception (throwLeft)
import Morley.Util.Main (wrapMain)
import Morley.Util.Named

main :: IO ()
main = wrapMain $ do
  disableAlphanetWarning
  (exts, args) <- Opt.execParser morleylargeOriginatorInfo
  case args of
    -- here we use morley client to actually perform the multi-step origination
    OriginateLarge parsedConfig OriginateArgs{..} -> do
      env <- mkMorleyClientEnv parsedConfig
      runMorleyClientM env $ do
        untypedContract <- liftIO $ prepareContract' exts oaMbContractFile

        putTextLn $ "Deploying contract...\nThis may take a while, please wait."
        (operationHash, contractAddr) <-
          originateLargeUntypedContract True oaContractName oaOriginateFrom
            oaInitialBalance untypedContract oaInitialStorage oaMbFee

        putTextLn "Contract was successfully deployed."
        putTextLn $ "Operation hash: " <> pretty operationHash
        putTextLn $ "Contract address: " <> formatAddress contractAddr

    -- here we don't perform the multi-step origination, but rather save to file
    -- all the steps to do it in some other way
    MakeOriginationSteps msa@MakeStepsArgs{..} -> do
      untypedContract <- prepareContract' exts maMbContractFile
      someContract <- throwLeft $ pure $ typeCheckingWith def $ typeCheckContract untypedContract
      case someContract of
        SomeContract (contract@Contract{} :: Contract cp st) -> do
          storage <- throwLeft $ pure $ typeCheckingWith def $ typeVerifyStorage @st maInitialStorage
          case mkSomeLargeContractOriginator storage contract maInitialBalance of
            SomeLargeContractOriginator bigMapsTVal origContract origLambda -> do
              putTextLn "Saving large origination steps data."
              createDirectoryIfMissing True maDestinationDir

              -- originator contract and storage
              writeFile (maDestinationDir </> "00_originator.tz") $
                printTypedContract False origContract
              writeFile (maDestinationDir </> "00_storage.tz") $
                printTypedValue True $ mkLargeOriginatorStore bigMapsTVal maOriginateFrom

              -- chunks of the packed lambda
              let numberedChunks = zip [1..] . reverse $ divideValueInChunks origLambda
              forM_ numberedChunks $ \(n, chunk) -> do
                -- here we dangerously assume the contract origination is going
                -- to fit in less than 100 packed lambda chunks
                let numPrefix = pretty $ padLeftF @Int 2 '0' n
                let chunkPath = maDestinationDir </> (numPrefix <> "_packed_lambda.tz")
                writeFile chunkPath $ printLorentzValue True chunk
              -- finally we print what to do with the saved files
              putTextLn $ stepsMessage msa
  where
    -- TODO [#712]: Remove this next major release
    prepareContract' exts
      | exts = prepareContractExt
      | otherwise = prepareContract

stepsMessage:: MakeStepsArgs -> Text
stepsMessage MakeStepsArgs{..} = unlines
  [ "All steps' data has been saved to file, under " +| maDestinationDir |+ ""
  , ""
  , "To perform the large origination follow these steps with your client of choice:"
  , ""
  , "First: use the \"00_*\" files to originate a contract with the 'originator'"
  , "code and the provided initial 'storage', with no balance,"
  , "using the " +| maOriginateFrom |+ " address. Take note of its address."
  , ""
  , "Then, for each \"xx_packed_lambda\" file, and in ascending order: "
  , "make a 'transfer' to the originator contract's \"load_lambda\" entrypoint,"
  , "using the content of the file as argument."
  , "Each without a balance, again from the " +| maOriginateFrom |+ " address."
  , ""
  , "Lastly, make a 'transfer' to the originator contract's \"run_lambda\""
  , "entrypoint, with a balance of " +| maInitialBalance |+ ", a 'Unit' argument"
  , "and once again from the " +| maOriginateFrom |+ " address."
  , ""
  , "Done. Your large contract has been created at this point."
  , "Its address can be seen in the originator's storage."
  , "Moreover, any following transfer to the originator will fail with such address."
  ]

--------------------------------------------------------------------------------
-- Options and parsing
--------------------------------------------------------------------------------

data LargeOriginatorArgs
  = OriginateLarge MorleyClientConfig OriginateArgs
  | MakeOriginationSteps MakeStepsArgs

data MakeStepsArgs = MakeStepsArgs
  { maMbContractFile :: Maybe FilePath
  , maInitialBalance :: Mutez
  , maInitialStorage :: U.Value
  , maOriginateFrom  :: Address
  , maDestinationDir :: FilePath
  }

morleylargeOriginatorInfo :: Opt.ParserInfo (Bool, LargeOriginatorArgs)
morleylargeOriginatorInfo =
  parserInfo
    (#usage :! usageDoc)
    (#description :! "Morley Large Originator: utility to originate large tezos contracts.")
    (#header :! "Morley Large Originator")
    (#parser :! largeOriginatorParser)

largeOriginatorParser :: Opt.Parser (Bool, LargeOriginatorArgs)
largeOriginatorParser = ((,) <$> enableExtsOption <*>) . Opt.subparser $
  originateLargeCmd <> somethingElseCmd
  where
    enableExtsOption = Opt.switch $
      Opt.long "deprecated-morley-extensions" <>
      Opt.help "Enable parsing deprecated Morley extensions"

originateLargeCmd :: Opt.Mod Opt.CommandFields LargeOriginatorArgs
originateLargeCmd = mkCommandParser "originate"
  (OriginateLarge <$> clientConfigParser (pure Nothing) <*> originateArgsOption)
  "Originate the given large contract on the network"

somethingElseCmd :: Opt.Mod Opt.CommandFields LargeOriginatorArgs
somethingElseCmd = mkCommandParser "steps"
  (MakeOriginationSteps <$> makeStepsArgsOption)
  "Save the data for the steps to originate a large contract externally"

makeStepsArgsOption :: Opt.Parser MakeStepsArgs
makeStepsArgsOption = do
  maMbContractFile <- mbContractFileOption
  maInitialBalance <-
    mutezOption
      (Just zeroMutez)
      (#name :! "initial-balance")
      (#help :! "Inital balance of the contract")
  maInitialStorage <-
    valueOption
      Nothing
      (#name :! "initial-storage")
      (#help :! "Initial contract storage value")
  maOriginateFrom <-
    addressOption
      Nothing
      (#name :! "from")
      (#help :! "Address from which origination is performed")
  maDestinationDir <- Opt.strOption $ mconcat
      [ Opt.long "destination", Opt.metavar "FILEPATH"
      , Opt.help "Path to destination directory"
      ]
  pure $ MakeStepsArgs {..}

usageDoc :: Doc
usageDoc = mconcat
  [ "You can use help for specific COMMAND", linebreak
  , "EXAMPLE:", linebreak
  , "morley-large-originator originate --help"
  , linebreak
  , "USAGE EXAMPLE:", linebreak
  , "morley-large-originator originate \\", linebreak
  , "  -E florence.testnet.tezos.serokell.team:8732 \\", linebreak
  , "  --from tz1akcPmG1Kyz2jXpS4RvVJ8uWr7tsiT9i6A \\", linebreak
  , "  --contract ../contracts/tezos_examples/attic/add1.tz \\", linebreak
  , "  --initial-balance 1 --initial-storage 0", linebreak
  , linebreak
  , "  This command will originate a large contract with the code stored in add1.tz", linebreak
  , "  on a real network, by performing a multi-step origination process, ", linebreak
  , "  with initial balance 1 and initial storage set to 0.", linebreak
  , "  It returns info about the operation: its hash and the originated contract address", linebreak
  , linebreak
  , "morley-large-originator steps \\", linebreak
  , "  --from tz1akcPmG1Kyz2jXpS4RvVJ8uWr7tsiT9i6A \\", linebreak
  , "  --contract ../contracts/tezos_examples/attic/add1.tz \\", linebreak
  , "  --initial-balance 1 --initial-storage 0 --destination out", linebreak
  , linebreak
  , "  This command will save the data required to perform all the steps for", linebreak
  , "  the origination of a large smart contract."
  , "  It will save as many files as it needs into the 'out' directory", linebreak
  , "  and print an explanation on the steps to execute with any", linebreak
  , "  client you might prefer."
  ]
