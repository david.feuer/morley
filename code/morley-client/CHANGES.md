<!-- Unreleased: append new entries here -->


0.1.1
=====
* [!1094](https://gitlab.com/morley-framework/morley/-/merge_requests/1094)
  Deprecate morley language extensions
  + Morley language extensions now require `--deprecated-morley-extensions` flag to parse.
* [!1077](https://gitlab.com/morley-framework/morley/-/merge_requests/1077)
  Ithaca changes: Use `head~2` block in the `branch` field of RPC operations.
* [!1034](https://gitlab.com/morley-framework/morley/-/merge_requests/1034)
  Add key revealing that uses only RPC.
* [!965](https://gitlab.com/morley-framework/morley/-/merge_requests/965)
  Use Morley's fixed-size lists
  + Use `SizedList` for `feeOutputParser`
* [!1072](https://gitlab.com/morley-framework/morley/-/merge_requests/1072)
  Add `runCode` to Cleveland
  + `runContract` now supports parameter/storage values in their RPC
    representation (i.e. with bigmap IDs).
* [!1070](https://gitlab.com/morley-framework/morley/-/merge_requests/1070)
  Simplify cleveland's internals & public api
  + Removed `runContractSimple`, added `runContractParameters` and lenses.
* [!1060](https://gitlab.com/morley-framework/morley/-/merge_requests/1060)
  Move `AsRPC` type family to `morley`
* [!978](https://gitlab.com/morley-framework/morley/-/merge_requests/978)
  Make it difficult to misuse 'Show'
  + Use `Buildable` and `pretty` preferrentially.
  + Add `Buildable` instances to that effect for `FeeParserException`, `SecretKeyEncryptionParserException`.
  + Use `displayException` instead of `show` where appropriate.

0.1.0
=====
Initial release.
A client to interact with the Tezos blockchain, by use of the `tezos-node` RPC
and/or of the `tezos-client` binary.
