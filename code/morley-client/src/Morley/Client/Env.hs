-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Runtime environment for @morley-client@.

module Morley.Client.Env
  ( MorleyClientEnv' (..)

  -- * Lens
  , mceTezosClientL
  , mceLogActionL
  , mceSecretKeyL
  , mceClientEnvL
  ) where

import Control.Lens (lens)
import Morley.Util.Lens (makeLensesWith, postfixLFields)
import Servant.Client (ClientEnv)

import Morley.Tezos.Crypto.Ed25519 qualified as Ed25519

import Morley.Client.Logging (ClientLogAction)
import Morley.Client.TezosClient.Types

-- | Runtime environment for morley client.
data MorleyClientEnv' m = MorleyClientEnv
  { mceTezosClient :: TezosClientEnv
  -- ^ Environment for @tezos-client@.
  , mceLogAction :: ClientLogAction m
  -- ^ Action used to log messages.
  , mceSecretKey :: Maybe Ed25519.SecretKey
  -- ^ Pass if you want to sign operations manually or leave it
  -- to tezos-client
  , mceClientEnv :: ClientEnv
  -- ^ Environment necessary to make HTTP calls.
  }

makeLensesWith postfixLFields ''MorleyClientEnv'

instance HasTezosClientEnv (MorleyClientEnv' m) where
  tezosClientEnvL =
    lens mceTezosClient (\mce tce -> mce { mceTezosClient = tce })
