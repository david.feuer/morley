-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA
-- | Functions to submit transactions via @tezos-client@ and node RPC.

module Morley.Client.Action.Transaction
  ( runTransactions
  , lRunTransactions

  -- * Transfer
  , transfer
  , lTransfer

  -- Datatypes for batch transactions
  , TD (..)
  , LTransactionData (..)
  , TransactionData (..)
  ) where

import Lorentz.Constraints
import Morley.Client.Action.Common
import Morley.Client.Action.Operation
import Morley.Client.Logging
import Morley.Client.RPC.Class
import Morley.Client.RPC.Error
import Morley.Client.RPC.Types
import Morley.Client.TezosClient.Class
import Morley.Client.TezosClient.Types
import Morley.Client.Types
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.Scope
import Morley.Michelson.Untyped.Entrypoints
import Morley.Tezos.Address
import Morley.Tezos.Core (Mutez)

-- | Perform sequence of transactions to the contract, each transactions should be
-- defined via @EntrypointParam t@. Returns operation hash and block in which
-- this operation appears or @Nothing@ in case empty list was provided.
runTransactions
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => Address -> [TransactionData]
  -> m (Maybe OperationHash)
runTransactions sender transactions = do
  (opHash, _) <- runOperations (AddressResolved sender) (OpTransfer <$> transactions)
  return opHash

-- | Lorentz version of 'TransactionData'.
data LTransactionData where
  LTransactionData ::
    forall (t :: Type). NiceParameter t =>
    TD t -> LTransactionData

-- | Lorentz version of 'runTransactions'
lRunTransactions
  :: forall m env.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    )
  => Address
  -> [LTransactionData]
  -> m (Maybe OperationHash)
lRunTransactions sender transactions =
  runTransactions sender $ map convertLTransactionData transactions
  where
    convertLTransactionData :: LTransactionData -> TransactionData
    convertLTransactionData (LTransactionData (TD {..} :: TD t))=
      withDict (niceParameterEvi @t) $
      TransactionData TD
      { tdReceiver = tdReceiver
      , tdEpName = tdEpName
      , tdAmount = tdAmount
      , tdParam = T.toVal tdParam
      , tdMbFee = tdMbFee
      }

transfer
  :: forall m t env.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    , ParameterScope t
    )
  => Address
  -> Address
  -> Mutez
  -> EpName
  -> T.Value t
  -> Maybe Mutez
  -> m OperationHash
transfer from to amount epName param mbFee = do
  res <- runTransactions from $
    [TransactionData TD
      { tdReceiver = to
      , tdAmount = amount
      , tdEpName = epName
      , tdParam = param
      , tdMbFee = mbFee
      }
    ]
  case res of
    Just hash -> return hash
    _ -> throwM RpcNoOperationsRun

lTransfer
  :: forall m t env.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    , NiceParameter t
    )
  => Address
  -> Address
  -> Mutez
  -> EpName
  -> t
  -> Maybe Mutez
  -> m OperationHash
lTransfer from to amount epName param mbFee =
  withDict (niceParameterEvi @t) $
    transfer from to amount epName (T.toVal param) mbFee
