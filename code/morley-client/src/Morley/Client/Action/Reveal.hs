-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Functions to reveal keys via node RPC.
module Morley.Client.Action.Reveal
  ( RevealData (..)
  , revealKey
  , revealKeyUnlessRevealed
  ) where

import Fmt ((|+))

import Morley.Client.Action.Common hiding (revealKeyUnlessRevealed)
import Morley.Client.Action.Operation
import Morley.Client.Logging
import Morley.Client.RPC.Class
import Morley.Client.RPC.Error
import Morley.Client.RPC.Getters
import Morley.Client.RPC.Types
import Morley.Client.TezosClient (AddressOrAlias(..), HasTezosClient)
import Morley.Client.Types
import Morley.Tezos.Address

-- | Reveal given key.
--
-- This is a variation of key revealing method that tries to use solely RPC.
revealKey
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => Address
  -> RevealData
  -> m OperationHash
revealKey sender revealing = do
  (mOpHash, _) <- runOperations (AddressResolved sender) [OpReveal revealing]
  case mOpHash of
    Just hash -> return hash
    _ -> throwM RpcNoOperationsRun

-- | Reveal given key.
revealKeyUnlessRevealed
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => Address
  -> RevealData
  -> m ()
revealKeyUnlessRevealed sender param = do
  -- An optimization for the average case, but we can't rely on it in
  -- distributed environment
  mManagerKey <- getManagerKey sender
  case mManagerKey of
    Just _  -> logDebug $ sender |+ " address has already revealed key"
    Nothing -> ignoreAlreadyRevealedError $ void (revealKey sender param)
  where
    ignoreAlreadyRevealedError action =
      action `catch` \case
        RunCodeErrors [PreviouslyRevealedKey _] -> pass
        e -> throwM e
