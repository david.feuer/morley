-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Types used for interaction with @tezos-client@.

module Morley.Client.TezosClient.Types
  ( CmdArg (..)
  , Alias
  , AliasHint
  , AliasOrAliasHint (..)
  , AddressOrAlias (..)
  , addressResolved
  , CalcOriginationFeeData (..)
  , CalcTransferFeeData (..)
  , TezosClientConfig (..)
  , TezosClientEnv (..)
  , HasTezosClientEnv (..)
  , SecretKeyEncryption (..)

  -- * Unsafe coercions
  , unsafeCoerceAliasHintToAlias
  , unsafeCoerceAliasToAliasHint
  , unsafeGetAliasText
  , unsafeGetAliasHintText
  , mkAlias
  , mkAliasHint

  -- * Lens
  , tceAliasPrefixL
  , tceEndpointUrlL
  , tceTezosClientPathL
  , tceMbTezosClientDataDirL
  ) where

import Data.Aeson (FromJSON(..), KeyValue(..), ToJSON(..), object, withObject, (.:))
import Data.ByteArray (ScrubbedBytes)
import Data.Coerce (coerce)
import Data.Fixed (E6, Fixed(..))
import Fmt (Buildable(..), pretty)
import Morley.Util.Lens (makeLensesWith, postfixLFields)
import Options.Applicative qualified as Opt
import Servant.Client (BaseUrl(..), showBaseUrl)
import Text.Hex (encodeHex)

import Lorentz (ToAddress, toAddress)
import Morley.Client.RPC.Types (OperationHash)
import Morley.Client.Util
import Morley.Micheline
import Morley.Michelson.Printer
import Morley.Michelson.Typed (Contract, EpName, Value)
import Morley.Michelson.Typed qualified as T
import Morley.Tezos.Address (Address, parseAddress)
import Morley.Tezos.Core
import Morley.Tezos.Crypto
import Morley.Util.CLI (HasCLReader(..))

-- | An object that can be put as argument to a tezos-client command-line call.
class CmdArg a where
  -- | Render an object as a command-line argument.
  toCmdArg :: a -> String
  default toCmdArg :: Buildable a => a -> String
  toCmdArg = pretty

instance CmdArg Text where

instance CmdArg LText where

instance CmdArg Word16 where

instance CmdArg SecretKey where
  toCmdArg = toCmdArg . formatSecretKey

instance CmdArg Address where

instance CmdArg ByteString where
  toCmdArg = toCmdArg . ("0x" <>) . encodeHex

instance CmdArg EpName where
  toCmdArg = toCmdArg . epNameToTezosEp

instance CmdArg Mutez where
  toCmdArg m = show . MkFixed @_ @E6 $ fromIntegral (unMutez m)

instance T.ProperUntypedValBetterErrors t => CmdArg (Value t) where
  toCmdArg = toCmdArg . printTypedValue True

instance CmdArg (Contract cp st) where
  toCmdArg = toString . printTypedContract True

instance CmdArg BaseUrl where
  toCmdArg = showBaseUrl

instance CmdArg OperationHash

-- | @tezos-client@ can associate addresses with textual aliases.
-- This type denotes such an alias.
newtype Alias = Alias
  { unsafeGetAliasText :: Text
    -- ^ Unsafely extract 'Text' from 'Alias'. Do NOT use the result with
    -- 'mkAliasHint'.
  }
  deriving stock (Show, Eq, Ord)
  deriving newtype (Buildable, CmdArg)

-- | A hint for constructing an alias when generating an address or
-- remembering a contract.
--
-- Resulting 'Alias' most likely will differ from this as we tend to prefix
-- aliases, but a user should be able to recognize your alias visually.
-- For instance, passing @"alice"@ as a hint may result into @"myTest.alice"@
-- alias being created.
newtype AliasHint = AliasHint
  { unsafeGetAliasHintText :: Text
    -- ^ Unsafely extract 'Text' from 'AliasHint'. Do NOT use the result with
    -- 'mkAlias'.
  }
  deriving stock (Show)
  deriving newtype (IsString, Buildable, Semigroup, Monoid)

-- | Coerce 'Alias' to 'AliasHint'. Unless you know for a fact that prefix is empty,
-- this is unsafe.
unsafeCoerceAliasToAliasHint :: Alias -> AliasHint
unsafeCoerceAliasToAliasHint = coerce

-- | Coerce 'AliasHint' to 'Alias'. Unless you know for a fact that prefix is empty,
-- this is unsafe.
unsafeCoerceAliasHintToAlias :: AliasHint -> Alias
unsafeCoerceAliasHintToAlias = coerce

-- | Make 'Alias' from 'Text'
mkAlias :: Text -> Alias
mkAlias = Alias

-- | Make 'AliasHint' from 'Text'
mkAliasHint :: Text -> AliasHint
mkAliasHint = AliasHint

-- | Either an 'Alias', or an 'AliasHint'. The difference is that 'AliasHint' needs to be
-- prefixed (if alias prefix is non-empty), while 'Alias' doesn't.
data AliasOrAliasHint
  = AnAlias Alias
  | AnAliasHint AliasHint
  deriving stock (Show)

-- | Representation of an address that @tezos-client@ uses. It can be
-- an address itself or a textual alias.
data AddressOrAlias
  = AddressResolved Address
  -- ^ Address itself, can be used as is.
  | AddressAlias Alias
  -- ^ Address alias, should be resolved by @tezos-client@.
  deriving stock (Show, Eq, Ord)

instance CmdArg AddressOrAlias where

instance HasCLReader AddressOrAlias where
  getReader =
    Opt.str <&> \addrOrAlias ->
      case parseAddress addrOrAlias of
        Right addr -> AddressResolved addr
        Left _ -> AddressAlias (Alias addrOrAlias)
  getMetavar = "ADDRESS OR ALIAS"

-- | Creates an 'AddressOrAlias' with the given address.
addressResolved :: ToAddress addr => addr -> AddressOrAlias
addressResolved = AddressResolved . toAddress

instance Buildable AddressOrAlias where
  build = \case
    AddressResolved addr -> build addr
    AddressAlias alias -> build alias

-- | Representation of address secret key encryption type
data SecretKeyEncryption
  = UnencryptedKey
  | EncryptedKey
  | LedgerKey
  deriving stock (Eq, Show)

-- | Configuration maintained by @tezos-client@, see its @config@ subcommands
-- (e. g. @tezos-client config show@).
-- Only the field we are interested in is present here.
newtype TezosClientConfig = TezosClientConfig { tcEndpointUrl :: BaseUrl }
  deriving stock Show

-- | For reading tezos-client config.
instance FromJSON TezosClientConfig where
  parseJSON = withObject "node info" $ \o -> TezosClientConfig <$> o .: "endpoint"

-- | Runtime environment for @tezos-client@ bindings.
data TezosClientEnv = TezosClientEnv
  { tceAliasPrefix :: Maybe Text
  -- ^ Optional prefix for aliases that will be passed to @tezos-client@.
  -- If you call some function and pass @foo@ 'Alias' to it when the prefix
  -- is provided, it will be prepened to @foo@. So @prefix.foo@ will be passed
  -- to @tezos-client@. Note that the prefix will be only applied in functions
  -- such as 'Morley.Client.TezosClient.Class.genKey' and
  -- 'Morley.Client.TezosClient.Class.rememberContract' that work directly
  -- with @tezos-client@ contract cache and add addresses to it.
  , tceEndpointUrl :: BaseUrl
  -- ^ URL of tezos node on which operations are performed.
  , tceTezosClientPath :: FilePath
  -- ^ Path to tezos client binary through which operations are
  -- performed.
  , tceMbTezosClientDataDir :: Maybe FilePath
  -- ^ Path to tezos client data directory.
  }

makeLensesWith postfixLFields ''TezosClientEnv

-- | Using this type class one can require 'MonadReader' constraint
-- that holds any type with 'TezosClientEnv' inside.
class HasTezosClientEnv env where
  tezosClientEnvL :: Lens' env TezosClientEnv

-- | Data required for calculating fee for transfer operation.
data CalcTransferFeeData = forall t. T.UntypedValScope t => CalcTransferFeeData
  { ctfdTo :: AddressOrAlias
  , ctfdParam :: Value t
  , ctfdEp :: EpName
  , ctfdAmount :: TezosMutez
  }

instance ToJSON CalcTransferFeeData where
  toJSON CalcTransferFeeData{..} = object
    [ "destination" .= pretty @_ @Text ctfdTo
    , "amount" .= (fromString @Text $ toCmdArg $ unTezosMutez ctfdAmount)
    , "arg" .= (fromString @Text $ toCmdArg ctfdParam)
    , "entrypoint" .= (fromString @Text $ toCmdArg ctfdEp)
    ]

-- | Data required for calculating fee for origination operation.
data CalcOriginationFeeData cp st = CalcOriginationFeeData
  { cofdFrom :: AddressOrAlias
  , cofdBalance :: TezosMutez
  , cofdMbFromPassword :: Maybe ScrubbedBytes
  , cofdContract :: Contract cp st
  , cofdStorage :: Value st
  , cofdBurnCap :: TezosInt64
  }
