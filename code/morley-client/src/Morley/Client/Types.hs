-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Client.Types
  ( OperationInfoDescriptor (..)
  , OperationInfo (..)
  , _OpTransfer
  , _OpOriginate
  , _OpReveal
  ) where

import Control.Lens (makePrisms)

class OperationInfoDescriptor (i :: Type) where
  type family TransferInfo i :: Type
  type family OriginationInfo i :: Type
  type family RevealInfo i :: Type

data OperationInfo i
  = OpTransfer (TransferInfo i)
  | OpOriginate (OriginationInfo i)
  | OpReveal (RevealInfo i)

makePrisms ''OperationInfo
