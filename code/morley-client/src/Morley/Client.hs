-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Morley client that connects with real Tezos network through RPC and tezos-client binary.
-- For more information please refer to README.
module Morley.Client
  ( -- * Command line parser
    parserInfo
  , clientConfigParser

  -- * Full client monad and environment
  , MorleyClientM
  , MorleyClientConfig (..)
  , MorleyClientEnv' (..)
  , MorleyClientEnv
  , runMorleyClientM
  , mkMorleyClientEnv
  -- ** Lens
  , mceTezosClientL
  , mceLogActionL
  , mceSecretKeyL
  , mceClientEnvL

  -- * Only-RPC client monad and environment
  , MorleyOnlyRpcM (..)
  , MorleyOnlyRpcEnv (..)
  , mkMorleyOnlyRpcEnv
  , runMorleyOnlyRpcM

  -- * High-level actions
  , module Morley.Client.Action

  -- * RPC
  , BlockId (..)
  , HasTezosRpc (..)
  , OperationInfo (..)
  , OperationInfoDescriptor (..)
  , RPCInput
  , getContract
  , getImplicitContractCounter
  , getContractStorage
  , getBigMapValue
  , getHeadBlock
  , getCounter
  , getProtocolParameters
  , runOperation
  , preApplyOperations
  , forgeOperation
  , getContractScript
  , getContractBigMap
  , getBalance
  , getDelegate
  , runCode
  , getManagerKey

  -- ** Errors
  , ClientRpcError (..)
  , UnexpectedErrors (..)
  , IncorrectRpcResponse (..)
  , RunError (..)
  -- ** Getters
  , ValueDecodeFailure (..)
  , ValueNotFound (..)
  , readAllBigMapValues
  , readAllBigMapValuesMaybe
  , readContractBigMapValue
  , readBigMapValueMaybe
  , readBigMapValue

  -- * @tezos-client@
  , Alias
  , mkAlias
  , AliasHint
  , mkAliasHint
  , AliasOrAliasHint (..)
  , AddressOrAlias (..)
  , addressResolved
  , HasTezosClient (..)
  , resolveAddress
  , TezosClientError (..)

  -- * Util
  , disableAlphanetWarning

  -- * Reexports
  , Opt.ParserInfo -- Needed for tests
  ) where

import Options.Applicative qualified as Opt

import Morley.Client.Action
import Morley.Client.Env
import Morley.Client.Full
import Morley.Client.Init
import Morley.Client.OnlyRPC
import Morley.Client.Parser
import Morley.Client.RPC
import Morley.Client.TezosClient
import Morley.Client.Types
import Morley.Client.Util
