-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Main
  ( main
  ) where

import Test.Tasty (defaultMainWithIngredients)

import Ingredients (ourIngredients)
import Tree (tests)

main :: IO ()
main = tests >>= defaultMainWithIngredients ourIngredients
