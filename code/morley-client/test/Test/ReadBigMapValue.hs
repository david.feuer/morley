-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.ReadBigMapValue
  ( test_ReadBigMapValueUnit
  , test_ReadBigMapValueMaybeUnit
  ) where

import Data.Map (fromList)
import Test.HUnit (Assertion, assertFailure)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, (@?=))

import Morley.Client.RPC.Getters
import Morley.Michelson.Runtime.GState (genesisAddress1)
import Morley.Michelson.Typed (BigMapId(..))
import Test.Util
import TestM

bigMapGetHandlers :: Handlers TestM
bigMapGetHandlers = defaultHandlers
  { hGetBigMapValue = handleGetBigMapValue
  }

fakeStateWithBigMapContract
  :: FakeState
fakeStateWithBigMapContract = defaultFakeState
  { fsContracts = fromList $
    [ (genesisAddress1, dumbContractState
         { csContractData = (csContractData dumbContractState) & \case
             ContractData os _ -> ContractData os $ Just $
               mapToContractStateBigMap @Integer @Integer validBigMapId $ fromList [(2, 3), (3, 5)]
             implicitData -> implicitData
         }
      )
    ]
  }

validBigMapId :: BigMapId Integer Integer
validBigMapId = BigMapId 123

invalidBigMapId :: BigMapId Integer Integer
invalidBigMapId = BigMapId 456

resultShouldBe :: (HasCallStack, Eq a, Show a) => a -> Either SomeException a -> Assertion
resultShouldBe expected = \case
  Left e -> assertFailure $ displayException e
  Right actual -> actual @?= expected

test_ReadBigMapValueUnit :: TestTree
test_ReadBigMapValueUnit =
  testGroup "readBigMapValue"
    [ testCase "Retrieves existing value" $
        resultShouldBe 5 $
          runFakeTest bigMapGetHandlers fakeStateWithBigMapContract $
            readBigMapValue validBigMapId (3 :: Integer)
    ]

test_ReadBigMapValueMaybeUnit :: TestTree
test_ReadBigMapValueMaybeUnit =
  testGroup "readBigMapValueMaybe"
    [ testCase "Retrieves existing value" $
        resultShouldBe (Just 5) $
          runFakeTest bigMapGetHandlers fakeStateWithBigMapContract $
            readBigMapValueMaybe validBigMapId (3 :: Integer)
    , testCase "Returns Nothing when contract does not exist" $
        resultShouldBe Nothing $
          runFakeTest bigMapGetHandlers fakeStateWithBigMapContract $
            readBigMapValueMaybe invalidBigMapId (3 :: Integer)
    , testCase "Returns Nothing when key does not exist" $
        resultShouldBe Nothing $
          runFakeTest bigMapGetHandlers fakeStateWithBigMapContract $
            readBigMapValueMaybe validBigMapId (9 :: Integer)
    ]
