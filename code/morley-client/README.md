# Morley-client

This package implements a client to interact with the Tezos blockchain.
It is intended to be used both as a library and as a tool.

`morley-client` uses the [Tezos RPC API](https://tezos.gitlab.io/developer/rpc.html)
to interact with a remote (or local) `tezos-node` and uses the `tezos-client`
binary for some operations, such as signing and local contract manipulations.

IMPORTANT: `morley-client` is still actively developed and unstable.
Its interface(s) may change abruptly and it shouldn't be used for anything but testing.

## Command line interface

At the moment, the `morley-client` CLI can be used to perform a handful of operations.

You can use:
```sh
morley-client --help
```
to get a list of available commands and info about common options.

You can also use:
```sh
morley-client COMMAND --help
```
to get more information about each of the available `COMMAND` and its options.
